﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace DialogDesigner
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            string filename = null;
            if (args.Length == 1)
                filename = args[0];

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Main( filename ));
            /*try
            {
                Application.Run(new Main());
            }
            catch (Exception e)
            {
                const int MAX_TRACE_LINES = 15;
                int end = -1;
                int count = 0;
                while (count < MAX_TRACE_LINES && end < e.StackTrace.Length)
                {
                    end = e.StackTrace.IndexOf('\n', end + 1);
                    if (end < 0)
                        end = e.StackTrace.Length;
                    else
                        count++;
                }
                string trace = e.StackTrace.Substring(0, end);

                ErrorBox.Show(e.Message + "\n\n" + e.Source + ":\n" + trace);
            }*/
        }
    }
}
