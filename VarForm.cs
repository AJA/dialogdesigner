﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace DialogDesigner
{
    public partial class VarForm : Form
    {
        private Dialog dlg_;
        string beforeEdit_;

        public VarForm(Dialog dlg)
        {
            InitializeComponent();
            dlg_ = dlg;
            //LoadVariables();

            IndexColumn.ValueType = Type.GetType("int");
            VarColumn.ValueType = Type.GetType("string");
            ValueColumn.ValueType = Type.GetType("int");
            DescriptionColumn.ValueType = Type.GetType("string");
            TypeColumn.DataSource = Enum.GetNames(typeof(Variable.VarType));
        }

        public void LoadVariables(Dialog dlg)
        {
            dlg_ = dlg;
            if (dlg_ == null)
                return;

            // Tyhjennetään taulu
            gridVars.Rows.Clear();
            
            foreach (KeyValuePair<int, Variable> v in dlg_.Variables)
            {
                int row = gridVars.Rows.Add();
                gridVars.Rows[row].Cells[0].Value = v.Key;
                gridVars.Rows[row].Cells[0].ValueType = typeof(int);
                gridVars.Rows[row].Cells[1].Value = v.Value.Name;
                gridVars.Rows[row].Cells[2].Value = Enum.GetName(typeof(Variable.VarType), v.Value.Type);
                gridVars.Rows[row].Cells[3].Value = v.Value.Value;
                //gridVars.Rows[row].Cells[3].ValueType = typeof(int);
                gridVars.Rows[row].Cells[4].Value = v.Value.Description;
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            // Haetaan ensimmäinen vapaa indeksi
            int index = 0;
            while (dlg_.Variables.ContainsKey(index))
                index++;
                        
            dlg_.Variables.Add(index, new Variable("newVariable", Variable.VarType.Integer, 0, ""));
            int row = gridVars.Rows.Add();
            gridVars.Rows[row].Cells[0].Value = index;
            gridVars.Rows[row].Cells[0].ValueType = typeof(int);
            gridVars.Rows[row].Cells[1].Value = "newVariable";
            gridVars.Rows[row].Cells[2].Value = "Integer";
            gridVars.Rows[row].Cells[3].Value = 0;
            //gridVars.Rows[row].Cells[3].ValueType = typeof(int);
            gridVars.Rows[row].Cells[4].Value = "";
                        
            gridVars.CurrentCell = gridVars[1, row];
            gridVars.BeginEdit(true);
            //((Main)Tag).txtCond_TextChanged(this, e);
            dlg_.Modified = true;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in gridVars.SelectedRows)
            {
                int index = Convert.ToInt32(row.Cells[0].Value);
                dlg_.Variables.Remove(index);
                gridVars.Rows.Remove(row);
            }
            //((Main)Tag).txtCond_TextChanged(this, e);
            dlg_.Modified = true;
        }

        private void gridVars_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            bool nameMod = false;

            if (e.ColumnIndex == ValueColumn.Index)
            {
                try
                {
                    if (gridVars[e.ColumnIndex, e.RowIndex].Value == null)
                        gridVars[e.ColumnIndex, e.RowIndex].Value = "0";
                    int i = Convert.ToInt32(gridVars[e.ColumnIndex, e.RowIndex].Value);
                }
                catch (Exception)
                {
                    // Väärä tyyppi!
                    ErrorBox.Show("Values must be integers!\n\n" +
                                  "For boolean variables, use zero for false and any other value for true.");
                    // Palautetaan aiempi arvo
                    gridVars.CancelEdit();
                    
                    //gridVars[e.ColumnIndex, e.RowIndex].Value = beforeEdit_;
                    
                    gridVars.CurrentCell = gridVars[e.ColumnIndex, e.RowIndex];
                    gridVars.BeginEdit(true);
                }
            }
            else if (e.ColumnIndex == VarColumn.Index)
            {
                // Ei saa sisältää numeroita alussa eikä merkkejä eikä välejä
                const string REGEX = "(^[^A-Za-z_]|[^0-9A-Za-z_\\.\\(\\)\\[\\]]+)";
                string text = "";

                if (gridVars[e.ColumnIndex, e.RowIndex].Value != null)
                {
                    text = gridVars[e.ColumnIndex, e.RowIndex].Value.ToString();
                    Match m = Regex.Match(text, REGEX); //"(\\W+|\\s+|[^A-Za-z]+[0-9]+)");
                    while (m.Success)
                    {
                        string alku = text.Substring(0, m.Index);
                        string loppu = text.Substring(m.Index + m.Length);
                        text = alku + loppu;
                        nameMod = true;
                        m = Regex.Match(text, REGEX);
                    }
                }
                if (text.Length == 0)
                {
                    // Karsiutui kokonaan
                    text = "newVariable";
                }
                gridVars[e.ColumnIndex, e.RowIndex].Value = text;
            }

            if (gridVars[e.ColumnIndex, e.RowIndex].Value == null ||
                gridVars[e.ColumnIndex, e.RowIndex].Value.ToString().Equals(beforeEdit_))
            {
                // Arvo ei muuttunut
                return;
            }

            // Tallennetaan data
            int index = Convert.ToInt32(gridVars[IndexColumn.Index, e.RowIndex].Value);
            object value = gridVars[e.ColumnIndex, e.RowIndex].Value;

            if (e.ColumnIndex == VarColumn.Index)
                dlg_.Variables[index].Name = value.ToString();
            else if (e.ColumnIndex == TypeColumn.Index)
            {
                dlg_.Variables[index].Type = (Variable.VarType)Enum.Parse(typeof(Variable.VarType),
                    gridVars[e.ColumnIndex, e.RowIndex].Value.ToString());
            }
            else if (e.ColumnIndex == ValueColumn.Index)
            {
                dlg_.Variables[index].Value = Convert.ToInt32(value);
                // Tämän kentän arvon muuttaminen ei vaikuta tiedoston sisältöön
                return;
            }
            else if (e.ColumnIndex == DescriptionColumn.Index)
                dlg_.Variables[index].Description = value.ToString();

            dlg_.Modified = true;
            
            //if (e.ColumnIndex < gridVars.ColumnCount - 1)
            //    gridVars.CurrentCell = gridVars[e.ColumnIndex + 1, e.RowIndex];

            //((Main)Tag).txtCond_TextChanged(this, e);

            if (nameMod)
            {
                WarningBox.Show("Illegal variable name was modified.");
            }
        }

        private void gridVars_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (gridVars[e.ColumnIndex, e.RowIndex].Value != null)
                beforeEdit_ = gridVars[e.ColumnIndex, e.RowIndex].Value.ToString();
            else
                beforeEdit_ = null;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (dlg_ == null)
                return;

            // Asetetaan arvot oletusarvoiksi
            foreach (KeyValuePair<int, Variable> v in dlg_.Variables)
            {
                v.Value.DefaultValue = v.Value.Value;
            }
            dlg_.Modified = true;
        }

        private void VarForm_Deactivate(object sender, EventArgs e)
        {
            if (gridVars.IsCurrentCellInEditMode)
            {
                gridVars.EndEdit();
            }
            ((Main)Tag).txtCond_TextChanged(this, e);
        }

        private void gridVars_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex > 0 && e.ColumnIndex > 1)
                gridVars.Rows[e.RowIndex].Selected = true;
        }

        private void VarForm_Load(object sender, EventArgs e)
        {
            // Asetetaan tallennetut ikkunan asetukset
            this.Size = Properties.Settings.Default.VarSize;
            this.Location = Properties.Settings.Default.VarLoc;
            this.WindowState = Properties.Settings.Default.VarState;
            gridVars.Columns[1].Width = Properties.Settings.Default.VarColSize_Name;
            gridVars.Columns[2].Width = Properties.Settings.Default.VarColSize_Type;
            gridVars.Columns[3].Width = Properties.Settings.Default.VarColSize_Value;
            gridVars.Columns[4].Width = Properties.Settings.Default.VarColSize_Desc;
        }

        private void VarForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            // Tallennetaan ikkunan sijaintitieto
            Properties.Settings.Default.VarState = this.WindowState;
            if (this.WindowState == FormWindowState.Normal)
            {
                Properties.Settings.Default.VarSize = this.Size;
                Properties.Settings.Default.VarLoc = this.Location;
            }
            else
            {
                Properties.Settings.Default.VarSize = this.RestoreBounds.Size;
                Properties.Settings.Default.VarLoc = this.RestoreBounds.Location;
            }

            // Ja taulukon sarakeleveydet
            Properties.Settings.Default.VarColSize_Name = gridVars.Columns[1].Width;
            Properties.Settings.Default.VarColSize_Type = gridVars.Columns[2].Width;
            Properties.Settings.Default.VarColSize_Value = gridVars.Columns[3].Width;            
            Properties.Settings.Default.VarColSize_Desc = gridVars.Columns[4].Width;

            Properties.Settings.Default.Save();
        }

        private void VarForm_Activated(object sender, EventArgs e)
        {    
            LoadVariables(dlg_);
        }

        private void gridVars_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            gridVars.CurrentCell = gridVars[e.ColumnIndex, e.RowIndex];
            gridVars.BeginEdit(true);
        }

        private void gridVars_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F2)
            {
                gridVars.BeginEdit(true);
            }
        }

        private void menuVars_Opening(object sender, CancelEventArgs e)
        {
            if (dlg_ == null)
            {
                e.Cancel = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (dlg_ == null)
                return;

            // Asetetaan arvoihin oletusarvot
            foreach (KeyValuePair<int, Variable> v in dlg_.Variables)
            {
                v.Value.Value = v.Value.DefaultValue;
            }
            LoadVariables(dlg_);
        }

    }
}
