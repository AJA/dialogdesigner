﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.ComponentModel;
using System.IO;

namespace DialogDesigner
{
    public class OptionData : IComparable<OptionData>
    {
        private int index_;
        private List<string> script_;
        private Dialog dlg_;
        private string text_;
        private List<OptionData> children_;
        private OptionData parent_;

        private const string PATTERN_REFERENCE = "__REF\\{\\[\\(([1-9][0-9]*)\\)\\]\\}__";
        private const string PATTERN_COMMENT = "//.*$";

        public enum OptionState
        {
            On,
            Off,
            OffForever
        };

        /// <summary>
        /// Rakentaja
        /// </summary>
        /// <param name="dlg"></param>
        /// <param name="text"></param>
        public OptionData(Dialog dlg, string text)
        {
            dlg_ = dlg;
            text_ = text;
            Show = true;
            Condition = null;

            index_ = 0;
            children_ = new List<OptionData>();
            parent_ = null;

            script_ = new List<string>();
            script_.Add("");
        }

        /// <summary>
        /// Poistaa vaihtoehdon dialogista
        /// </summary>
        public void Remove(bool removeChildren, bool keepHierarchy)
        {
            if (!keepHierarchy)
            {
                Parent = null;
            }

            if (removeChildren)
            {
                for (int i = 0; i < Children.Count; i++)
                {
                    Children[i].Remove(true, keepHierarchy);
                    if (!keepHierarchy)
                        i--;
                }                
            }            

            dlg_.RemoveOption(index_);
        }

        // Index-property
        public int Index
        {
            get { return index_; }
            set { index_ = value; }
        }

        // Text-property
        public string Text
        {
            get { return text_; }
            set { text_ = value; }
        }

        // Show-property
        public bool Show { get; set; }

        // Say-property
        public bool Say { get; set; }

        // Script-property
        public string[] Script
        {
            get { return script_.ToArray(); }
            set
            {
                List<string> strList = new List<string>(value.Length);
                strList.AddRange(value);
                script_ = strList;
            }
        }

        // Children-property
        public List<OptionData> Children
        {
            get { return children_; }
        }

        // ChildCount-property
        public int ChildCount
        {
            get
            {
                return countChildren(this);
            }
        }

        private int countChildren(OptionData parent)
        {
            int lkm = parent.Children.Count;
            foreach (OptionData child in parent.Children)
            {
                lkm += countChildren(child);
            }
            return lkm;
        }

        /// <summary>
        /// Vertailu toiseen vaihtoehtoon indeksin mukaan
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public int CompareTo(OptionData other)
        {
            return Index - other.Index;
        }

        // Parent-property
        public OptionData Parent
        {
            get { return parent_; }
            set
            {
                // Poistetaan nykyisen parentin lapsista
                if (parent_ != null)
                {
                    parent_.Children.Remove(this);
                }

                parent_ = value;

                // Lisätään uuden parentin lapsiin
                if (parent_ != null)
                {
                    for (int i = 0; i < parent_.Children.Count; i++)
                    {
                        OptionData sibling = parent_.Children[i];
                        if (sibling.Index > Index)
                        {
                            parent_.Children.Insert(i, this);
                            return;
                        }
                    }
                    // Ei löytynyt isompi-indeksistä, joten lisätään perään
                    parent_.Children.Add(this);                    
                }
            }
        }

        // Node-property
        public TreeNode Node { get; set; }

        // Condition-property
        public string Condition { get; set; }

        // Visible-property
        public OptionState Visible { get; set; }

        // Dialog-property
        public Dialog Dialog { get { return dlg_; } }

        /// <summary>
        /// Valintateksti
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return Text;
        }

        private string stripReference(Match re)
        {
            string[] text = re.ToString().Split(new char[2] { '(', ')' });
            if (text.Length != 3)
            {
                throw new Exception("StripReference: invalid reference!");
            }
            return text[1];
        }

        public void BeginFixingReferences()
        {
            for (int i = 0; i < script_.Count; i++)
            {
                // Jos rivillä viitataan johonkin toiseen valintaan, merkitään se
                script_[i] = ScriptCommand.MarkReferences(script_[i], "__REF{[(", ")]}__");                
            }

            if (Condition == null)
                return;

            // Condition
            const string refables = "is(On|Off)\\((?<ref>[1-9][0-9]*)\\)";
            int diff = 0;
            MatchCollection matches = Regex.Matches(Condition, refables);

            foreach (Match m in matches)
            {
                string text = "__REF{[(" + m.Groups["ref"].Value + ")]}__";
                int len = Condition.Length;
                Condition = Condition.Remove(m.Groups["ref"].Index - diff, m.Groups["ref"].Length);
                Condition = Condition.Insert(m.Groups["ref"].Index - diff, text);
                diff += len - Condition.Length;
            }
        }

        public void EndFixingReferences()
        {
            for (int i = 0; i < script_.Count; i++)
            {
                // Jos rivillä on merkitty viittaus, poistetaan merkintä
                script_[i] = Regex.Replace(script_[i], PATTERN_REFERENCE,
                    new MatchEvaluator(stripReference));
            }

            if (Condition == null)
                return;

            // Condition
            Condition = Regex.Replace(Condition, PATTERN_REFERENCE,
                new MatchEvaluator(stripReference));
        }

        public void FixReference(int oldValue, int newValue)
        {
            // Etsitään rivit, joilla arvo(ja) on viitattuna ja korvataan uudella
            for (int i = 0; i < script_.Count; i++)
            {
                MatchCollection matches = Regex.Matches(script_[i], PATTERN_REFERENCE);
                int deleted = 0; // poistettuja merkkejä
                foreach (Match m in matches)
                {
                    // Rivillä on jokin viite
                    int reference = Convert.ToInt32( m.Groups[1].ToString() );

                    if (oldValue == reference)
                    {
                        if (newValue > 0)
                        {
                            // Korjataan viite
                            int len = script_[i].Length;
                            string start = script_[i].Substring(0, m.Index - deleted);
                            string end = script_[i].Substring(m.Index + m.Length - deleted);
                            script_[i] = start + newValue + end;
                            deleted = len - script_[i].Length;
                        }
                        else
                        {
                            // Valinta poistettiin, lisätään vain huomautus
                            script_[i] += "// *** Referenced option " + oldValue + " was deleted!";
                        }
                    }
                }
            }

            if (Condition == null)
                return;

            // Condition
            MatchCollection ma = Regex.Matches(Condition, PATTERN_REFERENCE);
            int del = 0; // poistettuja merkkejä
            foreach (Match m in ma)
            {
                // Rivillä on jokin viite
                int reference = Convert.ToInt32(m.Groups[1].ToString());

                if (oldValue == reference)
                {
                    if (newValue > 0)
                    {
                        // Korjataan viite
                        int len = Condition.Length;
                        string start = Condition.Substring(0, m.Index - del);
                        string end = Condition.Substring(m.Index + m.Length - del);
                        Condition = start + newValue + end;
                        del = len - Condition.Length;
                    }
                    else
                    {
                        // Valinta poistettiin, lisätään vain huomautus
                        Condition += "// *** Referenced option " + oldValue + " was deleted!";
                    }
                }
            }
        }

        public ScriptEffects GetScriptEffects(bool noThrow, bool readOnly, List<string> warnings)
        {
            ScriptEffects se = new ScriptEffects();
            se.Ons = new List<int>();
            se.Offs = new List<int>();
            se.OffForevers = new List<int>();
            se.ConditionalOns = new List<int>();

            bool leaveOn = false;
            bool dontShowChildren = false;
            bool showConditionals = true;

            // Käydään skripti läpi ja etsitään avattavat ja suljettavat
            for (int i = 0; i < script_.Count; i++)
            {
                ScriptCommand.Effects? e = null;

                try
                {
                    e = ScriptCommand.GetEffects(this, script_[i], readOnly);
                }
                catch (ScriptWarningException ex)
                {
                    if (warnings != null)
                        warnings.Add("Line " + (i + 1) + ": " + ex.Message);
                    continue;
                }
                catch (Exception ex)
                {
                    if (!noThrow)
                    {
                        throw new Exception("Option " + this.Index + ", line " + (i + 1) + ":\n"
                            + ex.Message);
                    }
                }
                
                if (e == null)
                    continue;

                if (!readOnly)
                {
                    // Suoritetaan tulokset
                    foreach (int j in e.Value.Ons)
                        if (dlg_.Options[j].Visible != OptionData.OptionState.OffForever)
                            dlg_.Options[j].Visible = OptionData.OptionState.On;
                    foreach (int j in e.Value.ConditionalOns)
                        if (dlg_.Options[j].Visible != OptionData.OptionState.OffForever)
                            dlg_.Options[j].Visible = OptionData.OptionState.On;
                    foreach (int j in e.Value.Offs)
                        if (dlg_.Options[j].Visible != OptionData.OptionState.OffForever)
                            dlg_.Options[j].Visible = OptionData.OptionState.Off;
                    foreach (int j in e.Value.OffForevers)
                        dlg_.Options[j].Visible = OptionData.OptionState.OffForever;
                }

                se.Ons.AddRange(e.Value.Ons);
                se.Offs.AddRange(e.Value.Offs);
                se.OffForevers.AddRange(e.Value.OffForevers);

                if (e.Value.Return || e.Value.Stop)
                    break;
                else if (e.Value.LeaveOn)
                    leaveOn = true;
                else if (e.Value.DontShowChildren)
                {
                    dontShowChildren = true;
                    showConditionals = false;
                }
                else if (e.Value.ShowConditionalsOnly)
                {
                    showConditionals = true;
                    dontShowChildren = true;
                }
            }

            doScriptControlStatements(se.Ons, se.Offs, se.ConditionalOns,
                leaveOn, dontShowChildren, showConditionals, noThrow, null, readOnly, warnings);

            return se;
        }

        public void doScriptControlStatements(List<int> ons, List<int> offs,
            List<int> condOns, bool leaveOn, bool dontShowChildren, bool showConditionals,
            bool noThrow, StreamWriter ags, bool readOnly, List<string> warnings)
        {
            int onsCount = 0;
            int offsCount = 0;
            int condOnsCount = 0;

            if (ags == null)
            {
                onsCount = ons.Count;
                offsCount = offs.Count;
                condOnsCount = condOns.Count;
            }

            if (index_ == 0) // START-valinnan lukitus
            {
                dontShowChildren = true;
                showConditionals = true;
            }

            if (!leaveOn && index_ > 0 && ScriptCommand.AutomaticallyTurnOffSelectedOption)
            {
                // Suljetaan tämä itse
                if (ags == null)
                    offs.Add(index_);
                else
                    ags.WriteLine("option-off " + index_);
            }

            if (!dontShowChildren)
            {
                // Näytetään lapset joille määritetty show päälle
                foreach (OptionData child in Children)
                {
                    if (child.Show)
                    {
                        if (ags == null)
                            ons.Add(child.Index);
                        else
                            ags.WriteLine("option-on " + child.Index);
                    }
                }
            }
            if (showConditionals)
            {
                // Näytetään lapset joiden ehto pätee
                foreach (OptionData child in Children)
                {
                    try
                    {
                        if (!child.Show && child.Condition != null)
                        {
                            if (ags == null &&
                                Conditional.Evaluate(child.Condition, dlg_))
                            {
                                condOns.Add(child.Index);
                            }
                            else if (ags != null)
                            {
                                string cond = ScriptCommand.ReplaceIsOnOff(dlg_, child.Condition, true);
                                ags.WriteLine("  if (" + cond + ") {");
                                ags.WriteLine("option-on " + child.Index);
                                ags.WriteLine("  }");
                            }
                        }
                    }
                    catch (ScriptWarningException ex)
                    {
                        if (warnings != null)
                            warnings.Add("Child option " + child.Index + ": " + ex.Message);
                        continue;
                    }
                    catch (Exception ex)
                    {
                        if (!noThrow)
                        {
                            throw new Exception("Option " + this.Index + ", child option " + child.Index + ":\n"
                                + ex.Message);
                        }
                    }
                }
            }

            if (!readOnly)
            {
                for (int i = onsCount; i < ons.Count; i++ )
                    if (dlg_.Options[ons[i]].Visible != OptionData.OptionState.OffForever)
                        dlg_.Options[ons[i]].Visible = OptionData.OptionState.On;
                for (int i = offsCount; i < offs.Count; i++)
                    if (dlg_.Options[offs[i]].Visible != OptionData.OptionState.OffForever)
                        dlg_.Options[offs[i]].Visible = OptionData.OptionState.Off;
                for (int i = condOnsCount; i < condOns.Count; i++)
                    if (dlg_.Options[condOns[i]].Visible != OptionData.OptionState.OffForever)
                        dlg_.Options[condOns[i]].Visible = OptionData.OptionState.On;
            }

        }

        public void ExportToAGS(StreamWriter ags)
        {
            // Otsikkorivi
            string indexStr = index_.ToString();
            if (index_ == 0)
                indexStr = "S";
            ags.WriteLine(string.Format("@{0}  // {1}", indexStr, text_));

            bool returnMissing = true;

            // Skripti
            bool leaveOn = false;
            bool dontShowChildren = false;
            bool showConditionals = true;

            // Käydään skripti läpi ja etsitään muuntamista tarvitsevat komennot
            for (int i = 0; i < script_.Count; i++)
            {
                string line = script_[i];
                ScriptCommand.Name n = ScriptCommand.ToAGS(ags, this, ref line, i);

                if (n == ScriptCommand.Name.Stop || n == ScriptCommand.Name.Return
                    || n == ScriptCommand.Name.GotoPrevious || n == ScriptCommand.Name.GotoDialog)
                {
                    returnMissing = false;
                    // Tähän väliin liitettävä ohjaustoiminnallisuus
                    doScriptControlStatements(null, null, null,
                        leaveOn, dontShowChildren, showConditionals, false, ags, true, null);
                }
                else if (n == ScriptCommand.Name.LeaveOn || n == ScriptCommand.Name.OffForever)
                    leaveOn = true;
                else if (n == ScriptCommand.Name.DontShowChildren)
                {
                    dontShowChildren = true;
                    showConditionals = false;
                }
                else if (n == ScriptCommand.Name.ShowConditionalsOnly)
                {
                    showConditionals = true;
                    dontShowChildren = true;
                }

                ags.Write(line);
            }

            if (returnMissing)
            {
                doScriptControlStatements(null, null, null,
                    leaveOn, dontShowChildren, showConditionals, false, ags, true, null);
                ags.WriteLine("return");
            }
        }
    }

    public struct ScriptEffects
    {
        public List<int> Ons;
        public List<int> Offs;
        public List<int> OffForevers;
        public List<int> ConditionalOns;
    }
}
