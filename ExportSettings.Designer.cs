﻿namespace DialogDesigner
{
    partial class ExportSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnOK = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.linkLabel2 = new System.Windows.Forms.LinkLabel();
            this.btnDelReplace = new System.Windows.Forms.Button();
            this.btnDelSearch = new System.Windows.Forms.Button();
            this.checkCapitalize = new System.Windows.Forms.CheckBox();
            this.boxDialogReplace = new System.Windows.Forms.ComboBox();
            this.boxDialogSearch = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.checkDialog = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textDialogOutput = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textDialogTest = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.checkTurnOffSelectedOptions = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(297, 490);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 11;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.linkLabel2);
            this.groupBox1.Controls.Add(this.btnDelReplace);
            this.groupBox1.Controls.Add(this.btnDelSearch);
            this.groupBox1.Controls.Add(this.checkCapitalize);
            this.groupBox1.Controls.Add(this.boxDialogReplace);
            this.groupBox1.Controls.Add(this.boxDialogSearch);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.linkLabel1);
            this.groupBox1.Controls.Add(this.checkDialog);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Location = new System.Drawing.Point(12, 90);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(441, 390);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Dialog lines";
            // 
            // linkLabel2
            // 
            this.linkLabel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.linkLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel2.ForeColor = System.Drawing.Color.Firebrick;
            this.linkLabel2.LinkArea = new System.Windows.Forms.LinkArea(75, 25);
            this.linkLabel2.Location = new System.Drawing.Point(19, 48);
            this.linkLabel2.Name = "linkLabel2";
            this.linkLabel2.Size = new System.Drawing.Size(403, 46);
            this.linkLabel2.TabIndex = 3;
            this.linkLabel2.TabStop = true;
            this.linkLabel2.Text = "Warning: the program will hang if processing the search pattern results in catast" +
    "rophic backtracking, so make sure you know what you\'re doing!";
            this.linkLabel2.UseCompatibleTextRendering = true;
            this.linkLabel2.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel2_LinkClicked);
            // 
            // btnDelReplace
            // 
            this.btnDelReplace.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelReplace.Location = new System.Drawing.Point(404, 175);
            this.btnDelReplace.Name = "btnDelReplace";
            this.btnDelReplace.Size = new System.Drawing.Size(18, 21);
            this.btnDelReplace.TabIndex = 7;
            this.btnDelReplace.Text = "–";
            this.btnDelReplace.UseVisualStyleBackColor = true;
            this.btnDelReplace.Click += new System.EventHandler(this.btnDelReplace_Click);
            // 
            // btnDelSearch
            // 
            this.btnDelSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelSearch.Location = new System.Drawing.Point(404, 110);
            this.btnDelSearch.Name = "btnDelSearch";
            this.btnDelSearch.Size = new System.Drawing.Size(18, 21);
            this.btnDelSearch.TabIndex = 5;
            this.btnDelSearch.Text = "–";
            this.btnDelSearch.UseVisualStyleBackColor = true;
            this.btnDelSearch.Click += new System.EventHandler(this.btnDelSearch_Click);
            // 
            // checkCapitalize
            // 
            this.checkCapitalize.AutoSize = true;
            this.checkCapitalize.Location = new System.Drawing.Point(19, 220);
            this.checkCapitalize.Name = "checkCapitalize";
            this.checkCapitalize.Size = new System.Drawing.Size(215, 17);
            this.checkCapitalize.TabIndex = 8;
            this.checkCapitalize.Text = "Capitalize the first letter of group \"name\"";
            this.checkCapitalize.UseVisualStyleBackColor = true;
            this.checkCapitalize.CheckedChanged += new System.EventHandler(this.UpdateDialogTest);
            // 
            // boxDialogReplace
            // 
            this.boxDialogReplace.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.boxDialogReplace.FormattingEnabled = true;
            this.boxDialogReplace.Location = new System.Drawing.Point(19, 176);
            this.boxDialogReplace.Name = "boxDialogReplace";
            this.boxDialogReplace.Size = new System.Drawing.Size(379, 21);
            this.boxDialogReplace.TabIndex = 6;
            this.boxDialogReplace.SelectedIndexChanged += new System.EventHandler(this.boxDialogReplace_SelectedIndexChanged);
            this.boxDialogReplace.TextUpdate += new System.EventHandler(this.boxDialogReplace_TextUpdate);
            this.boxDialogReplace.TextChanged += new System.EventHandler(this.UpdateDialogTest);
            // 
            // boxDialogSearch
            // 
            this.boxDialogSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.boxDialogSearch.FormattingEnabled = true;
            this.boxDialogSearch.Location = new System.Drawing.Point(19, 110);
            this.boxDialogSearch.Name = "boxDialogSearch";
            this.boxDialogSearch.Size = new System.Drawing.Size(379, 21);
            this.boxDialogSearch.TabIndex = 4;
            this.boxDialogSearch.SelectedIndexChanged += new System.EventHandler(this.boxDialogSearch_SelectedIndexChanged);
            this.boxDialogSearch.TextUpdate += new System.EventHandler(this.boxDialogSearch_TextUpdate);
            this.boxDialogSearch.TextChanged += new System.EventHandler(this.UpdateDialogTest);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(253, 200);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(169, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "Example: c${name}.Say(\"${line}\");";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(232, 134);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(190, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Example: (?<name>[^:]+):\\s*(?<line>.+)";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // linkLabel1
            // 
            this.linkLabel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(285, 29);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(137, 13);
            this.linkLabel1.TabIndex = 2;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "About regular expressions...";
            this.linkLabel1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // checkDialog
            // 
            this.checkDialog.AutoSize = true;
            this.checkDialog.Location = new System.Drawing.Point(19, 28);
            this.checkDialog.Name = "checkDialog";
            this.checkDialog.Size = new System.Drawing.Size(137, 17);
            this.checkDialog.TabIndex = 1;
            this.checkDialog.Text = "Use custom conversion";
            this.checkDialog.UseVisualStyleBackColor = true;
            this.checkDialog.CheckedChanged += new System.EventHandler(this.checkDialog_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.textDialogOutput);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.textDialogTest);
            this.groupBox2.Location = new System.Drawing.Point(19, 261);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(403, 112);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Try it";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 61);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Output:";
            // 
            // textDialogOutput
            // 
            this.textDialogOutput.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textDialogOutput.Location = new System.Drawing.Point(13, 77);
            this.textDialogOutput.Name = "textDialogOutput";
            this.textDialogOutput.ReadOnly = true;
            this.textDialogOutput.Size = new System.Drawing.Size(376, 20);
            this.textDialogOutput.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Line of dialog:";
            // 
            // textDialogTest
            // 
            this.textDialogTest.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textDialogTest.Location = new System.Drawing.Point(13, 38);
            this.textDialogTest.Name = "textDialogTest";
            this.textDialogTest.Size = new System.Drawing.Size(376, 20);
            this.textDialogTest.TabIndex = 9;
            this.textDialogTest.TextChanged += new System.EventHandler(this.UpdateDialogTest);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 160);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(109, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Replacement pattern:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 94);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Search pattern:";
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(378, 490);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 12;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.checkTurnOffSelectedOptions);
            this.groupBox3.Location = new System.Drawing.Point(12, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(441, 72);
            this.groupBox3.TabIndex = 13;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "General settings";
            // 
            // checkTurnOffSelectedOptions
            // 
            this.checkTurnOffSelectedOptions.AutoSize = true;
            this.checkTurnOffSelectedOptions.Location = new System.Drawing.Point(19, 28);
            this.checkTurnOffSelectedOptions.Name = "checkTurnOffSelectedOptions";
            this.checkTurnOffSelectedOptions.Size = new System.Drawing.Size(403, 17);
            this.checkTurnOffSelectedOptions.TabIndex = 0;
            this.checkTurnOffSelectedOptions.Text = "Automatically turn off selected option. (Override with $leave-on script command.)" +
    "";
            this.checkTurnOffSelectedOptions.UseVisualStyleBackColor = true;
            // 
            // ExportSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(465, 525);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnOK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ExportSettings";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Export to AGS - Settings";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textDialogOutput;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textDialogTest;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.CheckBox checkDialog;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox boxDialogReplace;
        private System.Windows.Forms.ComboBox boxDialogSearch;
        private System.Windows.Forms.CheckBox checkCapitalize;
        private System.Windows.Forms.Button btnDelSearch;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.Button btnDelReplace;
        private System.Windows.Forms.LinkLabel linkLabel2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox checkTurnOffSelectedOptions;
    }
}