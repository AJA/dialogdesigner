﻿namespace DialogDesigner
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.btnFile = new System.Windows.Forms.ToolStripMenuItem();
            this.btnNew = new System.Windows.Forms.ToolStripMenuItem();
            this.btnOpen = new System.Windows.Forms.ToolStripMenuItem();
            this.btnSave = new System.Windows.Forms.ToolStripMenuItem();
            this.btnSaveAs = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnExportAGS = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.variablesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpTopicsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dlgMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.btnAddBefore = new System.Windows.Forms.ToolStripMenuItem();
            this.btnAddAfter = new System.Windows.Forms.ToolStripMenuItem();
            this.btnAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.btnDeleteTree = new System.Windows.Forms.ToolStripMenuItem();
            this.statusline = new System.Windows.Forms.StatusStrip();
            this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.dlgOpen = new System.Windows.Forms.OpenFileDialog();
            this.dlgSave = new System.Windows.Forms.SaveFileDialog();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.panelPreview = new System.Windows.Forms.Panel();
            this.lblSpeaker = new System.Windows.Forms.Label();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnPreview = new System.Windows.Forms.Button();
            this.lblPreview = new System.Windows.Forms.Label();
            this.timerPreview = new System.Windows.Forms.Timer(this.components);
            this.dlgMove = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.btnMoveBefore = new System.Windows.Forms.ToolStripMenuItem();
            this.btnMoveAfter = new System.Windows.Forms.ToolStripMenuItem();
            this.btnMoveChild = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.script = new System.Windows.Forms.RichTextBox();
            this.dlgScript = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.copyFirstLineToTitle = new System.Windows.Forms.ToolStripMenuItem();
            this.lblScript = new System.Windows.Forms.Label();
            this.checkShow = new System.Windows.Forms.CheckBox();
            this.checkCond = new System.Windows.Forms.CheckBox();
            this.txtCond = new System.Windows.Forms.RichTextBox();
            this.lblCondValue = new System.Windows.Forms.Label();
            this.groupScript = new System.Windows.Forms.GroupBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.groupOptions = new System.Windows.Forms.GroupBox();
            this.options = new System.Windows.Forms.TreeView();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.dlgExport = new System.Windows.Forms.SaveFileDialog();
            this.btnRecent = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.menuStrip1.SuspendLayout();
            this.dlgMenu.SuspendLayout();
            this.panelPreview.SuspendLayout();
            this.dlgMove.SuspendLayout();
            this.dlgScript.SuspendLayout();
            this.groupScript.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupOptions.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnFile,
            this.viewToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(767, 24);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // btnFile
            // 
            this.btnFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnNew,
            this.btnOpen,
            this.btnSave,
            this.btnSaveAs,
            this.toolStripSeparator1,
            this.btnExportAGS,
            this.toolStripSeparator5,
            this.btnRecent,
            this.toolStripSeparator6,
            this.closeToolStripMenuItem});
            this.btnFile.Name = "btnFile";
            this.btnFile.Size = new System.Drawing.Size(35, 20);
            this.btnFile.Text = "File";
            this.btnFile.DropDownOpening += new System.EventHandler(this.btnFile_DropDownOpening);
            // 
            // btnNew
            // 
            this.btnNew.Name = "btnNew";
            this.btnNew.ShortcutKeyDisplayString = "Ctrl+N";
            this.btnNew.Size = new System.Drawing.Size(182, 22);
            this.btnNew.Text = "New";
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnOpen
            // 
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.ShortcutKeyDisplayString = "Ctrl+O";
            this.btnOpen.Size = new System.Drawing.Size(182, 22);
            this.btnOpen.Text = "Open...";
            this.btnOpen.Click += new System.EventHandler(this.btnOpen_Click);
            // 
            // btnSave
            // 
            this.btnSave.Name = "btnSave";
            this.btnSave.ShortcutKeyDisplayString = "Ctrl+S";
            this.btnSave.Size = new System.Drawing.Size(182, 22);
            this.btnSave.Text = "Save";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnSaveAs
            // 
            this.btnSaveAs.Name = "btnSaveAs";
            this.btnSaveAs.Size = new System.Drawing.Size(182, 22);
            this.btnSaveAs.Text = "Save As...";
            this.btnSaveAs.Click += new System.EventHandler(this.btnSaveAs_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(179, 6);
            // 
            // btnExportAGS
            // 
            this.btnExportAGS.Name = "btnExportAGS";
            this.btnExportAGS.Size = new System.Drawing.Size(182, 22);
            this.btnExportAGS.Text = "Export AGS Script...";
            this.btnExportAGS.Click += new System.EventHandler(this.btnExportAGS_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(179, 6);
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.closeToolStripMenuItem.Text = "Exit";
            this.closeToolStripMenuItem.Click += new System.EventHandler(this.closeToolStripMenuItem_Click);
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.variablesToolStripMenuItem});
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            this.viewToolStripMenuItem.Size = new System.Drawing.Size(41, 20);
            this.viewToolStripMenuItem.Text = "View";
            // 
            // variablesToolStripMenuItem
            // 
            this.variablesToolStripMenuItem.Name = "variablesToolStripMenuItem";
            this.variablesToolStripMenuItem.Size = new System.Drawing.Size(128, 22);
            this.variablesToolStripMenuItem.Text = "Variables";
            this.variablesToolStripMenuItem.Click += new System.EventHandler(this.variablesToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpTopicsToolStripMenuItem,
            this.toolStripSeparator4,
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // helpTopicsToolStripMenuItem
            // 
            this.helpTopicsToolStripMenuItem.Name = "helpTopicsToolStripMenuItem";
            this.helpTopicsToolStripMenuItem.ShortcutKeyDisplayString = "F1";
            this.helpTopicsToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.helpTopicsToolStripMenuItem.Text = "DialogDesigner Help";
            this.helpTopicsToolStripMenuItem.Click += new System.EventHandler(this.helpTopicsToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(196, 6);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.aboutToolStripMenuItem.Text = "About...";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // dlgMenu
            // 
            this.dlgMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnAddBefore,
            this.btnAddAfter,
            this.btnAdd,
            this.toolStripSeparator2,
            this.btnDelete,
            this.btnDeleteTree});
            this.dlgMenu.Name = "dlgMenu";
            this.dlgMenu.Size = new System.Drawing.Size(157, 120);
            this.dlgMenu.Opening += new System.ComponentModel.CancelEventHandler(this.dlgMenu_Opening);
            // 
            // btnAddBefore
            // 
            this.btnAddBefore.Name = "btnAddBefore";
            this.btnAddBefore.Size = new System.Drawing.Size(156, 22);
            this.btnAddBefore.Text = "Add before";
            this.btnAddBefore.Click += new System.EventHandler(this.btnAddBefore_Click);
            // 
            // btnAddAfter
            // 
            this.btnAddAfter.Name = "btnAddAfter";
            this.btnAddAfter.Size = new System.Drawing.Size(156, 22);
            this.btnAddAfter.Text = "Add after";
            this.btnAddAfter.Click += new System.EventHandler(this.btnAddAfter_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(156, 22);
            this.btnAdd.Text = "Add child";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(153, 6);
            // 
            // btnDelete
            // 
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(156, 22);
            this.btnDelete.Text = "Delete option";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnDeleteTree
            // 
            this.btnDeleteTree.Name = "btnDeleteTree";
            this.btnDeleteTree.Size = new System.Drawing.Size(156, 22);
            this.btnDeleteTree.Text = "Delete subtree";
            this.btnDeleteTree.Click += new System.EventHandler(this.btnDeleteTree_Click);
            // 
            // statusline
            // 
            this.statusline.Location = new System.Drawing.Point(0, 444);
            this.statusline.Name = "statusline";
            this.statusline.Size = new System.Drawing.Size(767, 22);
            this.statusline.TabIndex = 9;
            this.statusline.Text = "status";
            // 
            // lblStatus
            // 
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(38, 17);
            this.lblStatus.Text = "Ready";
            // 
            // dlgOpen
            // 
            this.dlgOpen.DefaultExt = "dlg.xml";
            this.dlgOpen.Filter = "Dialog files (*.dlg.xml)|*.dlg.xml";
            // 
            // dlgSave
            // 
            this.dlgSave.DefaultExt = "dlg.xml";
            this.dlgSave.Filter = "Dialog files (*.dlg.xml)|*.dlg.xml";
            // 
            // timer
            // 
            this.timer.Enabled = true;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // panelPreview
            // 
            this.panelPreview.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panelPreview.BackColor = System.Drawing.SystemColors.ControlText;
            this.panelPreview.Controls.Add(this.lblSpeaker);
            this.panelPreview.Controls.Add(this.btnReset);
            this.panelPreview.Controls.Add(this.btnPreview);
            this.panelPreview.Controls.Add(this.lblPreview);
            this.panelPreview.Location = new System.Drawing.Point(12, 27);
            this.panelPreview.Name = "panelPreview";
            this.panelPreview.Size = new System.Drawing.Size(743, 89);
            this.panelPreview.TabIndex = 14;
            this.panelPreview.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panelPreview_MouseDown);
            // 
            // lblSpeaker
            // 
            this.lblSpeaker.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSpeaker.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSpeaker.ForeColor = System.Drawing.Color.Turquoise;
            this.lblSpeaker.Location = new System.Drawing.Point(62, 12);
            this.lblSpeaker.Name = "lblSpeaker";
            this.lblSpeaker.Size = new System.Drawing.Size(617, 23);
            this.lblSpeaker.TabIndex = 17;
            this.lblSpeaker.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.lblSpeaker.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panelPreview_MouseDown);
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(6, 45);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(53, 23);
            this.btnReset.TabIndex = 16;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            this.btnReset.EnabledChanged += new System.EventHandler(this.btnReset_EnabledChanged);
            // 
            // btnPreview
            // 
            this.btnPreview.Location = new System.Drawing.Point(6, 21);
            this.btnPreview.Name = "btnPreview";
            this.btnPreview.Size = new System.Drawing.Size(53, 23);
            this.btnPreview.TabIndex = 15;
            this.btnPreview.Text = "Preview";
            this.btnPreview.UseVisualStyleBackColor = true;
            this.btnPreview.Click += new System.EventHandler(this.btnPreview_Click);
            // 
            // lblPreview
            // 
            this.lblPreview.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPreview.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPreview.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblPreview.Location = new System.Drawing.Point(62, 35);
            this.lblPreview.Name = "lblPreview";
            this.lblPreview.Size = new System.Drawing.Size(617, 42);
            this.lblPreview.TabIndex = 0;
            this.lblPreview.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblPreview.TextChanged += new System.EventHandler(this.lblPreview_TextChanged);
            this.lblPreview.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panelPreview_MouseDown);
            // 
            // timerPreview
            // 
            this.timerPreview.Interval = 1000;
            this.timerPreview.Tick += new System.EventHandler(this.timerPreview_Tick);
            // 
            // dlgMove
            // 
            this.dlgMove.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnMoveBefore,
            this.btnMoveAfter,
            this.btnMoveChild,
            this.toolStripSeparator3,
            this.toolStripMenuItem2});
            this.dlgMove.Name = "dlgMove";
            this.dlgMove.Size = new System.Drawing.Size(165, 98);
            // 
            // btnMoveBefore
            // 
            this.btnMoveBefore.Name = "btnMoveBefore";
            this.btnMoveBefore.Size = new System.Drawing.Size(164, 22);
            this.btnMoveBefore.Text = "Move before";
            this.btnMoveBefore.Click += new System.EventHandler(this.btnMoveBefore_Click);
            // 
            // btnMoveAfter
            // 
            this.btnMoveAfter.Name = "btnMoveAfter";
            this.btnMoveAfter.Size = new System.Drawing.Size(164, 22);
            this.btnMoveAfter.Text = "Move after";
            this.btnMoveAfter.Click += new System.EventHandler(this.btnMoveAfter_Click);
            // 
            // btnMoveChild
            // 
            this.btnMoveChild.Name = "btnMoveChild";
            this.btnMoveChild.Size = new System.Drawing.Size(164, 22);
            this.btnMoveChild.Text = "Move to children";
            this.btnMoveChild.Click += new System.EventHandler(this.btnMoveChild_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(161, 6);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(164, 22);
            this.toolStripMenuItem2.Text = "Cancel";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // script
            // 
            this.script.AcceptsTab = true;
            this.script.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.script.ContextMenuStrip = this.dlgScript;
            this.script.DetectUrls = false;
            this.script.Font = new System.Drawing.Font("Courier New", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.script.Location = new System.Drawing.Point(6, 59);
            this.script.Name = "script";
            this.script.Size = new System.Drawing.Size(421, 251);
            this.script.TabIndex = 0;
            this.script.Text = "";
            this.script.WordWrap = false;
            this.script.KeyDown += new System.Windows.Forms.KeyEventHandler(this.script_KeyDown);
            this.script.KeyUp += new System.Windows.Forms.KeyEventHandler(this.script_KeyUp);
            this.script.TextChanged += new System.EventHandler(this.script_TextChanged);
            // 
            // dlgScript
            // 
            this.dlgScript.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.copyFirstLineToTitle});
            this.dlgScript.Name = "dlgScript";
            this.dlgScript.Size = new System.Drawing.Size(209, 26);
            // 
            // copyFirstLineToTitle
            // 
            this.copyFirstLineToTitle.Name = "copyFirstLineToTitle";
            this.copyFirstLineToTitle.Size = new System.Drawing.Size(208, 22);
            this.copyFirstLineToTitle.Text = "Replace title with first line";
            this.copyFirstLineToTitle.ToolTipText = "Replaces the option title text with the first line of speech in the script.";
            this.copyFirstLineToTitle.Click += new System.EventHandler(this.copyFirstLineToTitle_Click);
            // 
            // lblScript
            // 
            this.lblScript.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblScript.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblScript.Location = new System.Drawing.Point(22, 20);
            this.lblScript.Name = "lblScript";
            this.lblScript.Size = new System.Drawing.Size(405, 13);
            this.lblScript.TabIndex = 1;
            this.lblScript.Text = "Script name";
            // 
            // checkShow
            // 
            this.checkShow.AutoSize = true;
            this.checkShow.Location = new System.Drawing.Point(25, 36);
            this.checkShow.Name = "checkShow";
            this.checkShow.Size = new System.Drawing.Size(53, 17);
            this.checkShow.TabIndex = 2;
            this.checkShow.Text = "Show";
            this.checkShow.UseVisualStyleBackColor = true;
            this.checkShow.CheckedChanged += new System.EventHandler(this.checkShow_CheckedChanged);
            // 
            // checkCond
            // 
            this.checkCond.AutoSize = true;
            this.checkCond.Location = new System.Drawing.Point(85, 36);
            this.checkCond.Name = "checkCond";
            this.checkCond.Size = new System.Drawing.Size(117, 17);
            this.checkCond.TabIndex = 3;
            this.checkCond.Text = "Show on condition:";
            this.checkCond.UseVisualStyleBackColor = true;
            this.checkCond.CheckedChanged += new System.EventHandler(this.checkCond_CheckedChanged);
            // 
            // txtCond
            // 
            this.txtCond.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCond.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCond.Font = new System.Drawing.Font("Courier New", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCond.Location = new System.Drawing.Point(199, 34);
            this.txtCond.Multiline = false;
            this.txtCond.Name = "txtCond";
            this.txtCond.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.txtCond.Size = new System.Drawing.Size(196, 21);
            this.txtCond.TabIndex = 9;
            this.txtCond.Text = "";
            this.txtCond.EnabledChanged += new System.EventHandler(this.txtCond_EnabledChanged);
            this.txtCond.TextChanged += new System.EventHandler(this.txtCond_TextChanged);
            // 
            // lblCondValue
            // 
            this.lblCondValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCondValue.Location = new System.Drawing.Point(395, 37);
            this.lblCondValue.Name = "lblCondValue";
            this.lblCondValue.Size = new System.Drawing.Size(32, 16);
            this.lblCondValue.TabIndex = 10;
            this.lblCondValue.Text = "False";
            this.lblCondValue.Visible = false;
            // 
            // groupScript
            // 
            this.groupScript.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupScript.Controls.Add(this.lblCondValue);
            this.groupScript.Controls.Add(this.txtCond);
            this.groupScript.Controls.Add(this.checkCond);
            this.groupScript.Controls.Add(this.checkShow);
            this.groupScript.Controls.Add(this.lblScript);
            this.groupScript.Controls.Add(this.script);
            this.groupScript.Location = new System.Drawing.Point(3, 3);
            this.groupScript.Name = "groupScript";
            this.groupScript.Size = new System.Drawing.Size(433, 316);
            this.groupScript.TabIndex = 7;
            this.groupScript.TabStop = false;
            this.groupScript.Text = "Script";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(12, 122);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.AutoScroll = true;
            this.splitContainer1.Panel1.Controls.Add(this.groupOptions);
            this.splitContainer1.Panel1MinSize = 100;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.groupScript);
            this.splitContainer1.Panel2MinSize = 100;
            this.splitContainer1.Size = new System.Drawing.Size(743, 319);
            this.splitContainer1.SplitterDistance = 300;
            this.splitContainer1.TabIndex = 15;
            // 
            // groupOptions
            // 
            this.groupOptions.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupOptions.Controls.Add(this.options);
            this.groupOptions.Location = new System.Drawing.Point(3, 3);
            this.groupOptions.Name = "groupOptions";
            this.groupOptions.Size = new System.Drawing.Size(294, 316);
            this.groupOptions.TabIndex = 6;
            this.groupOptions.TabStop = false;
            this.groupOptions.Text = "Dialog options";
            // 
            // options
            // 
            this.options.AllowDrop = true;
            this.options.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.options.ContextMenuStrip = this.dlgMenu;
            this.options.DrawMode = System.Windows.Forms.TreeViewDrawMode.OwnerDrawText;
            this.options.HideSelection = false;
            this.options.LabelEdit = true;
            this.options.Location = new System.Drawing.Point(6, 19);
            this.options.Name = "options";
            this.options.ShowRootLines = false;
            this.options.Size = new System.Drawing.Size(282, 291);
            this.options.TabIndex = 6;
            this.options.MouseClick += new System.Windows.Forms.MouseEventHandler(this.options_MouseClick);
            this.options.DrawNode += new System.Windows.Forms.DrawTreeNodeEventHandler(this.options_DrawNode);
            this.options.AfterLabelEdit += new System.Windows.Forms.NodeLabelEditEventHandler(this.options_AfterLabelEdit);
            this.options.DragDrop += new System.Windows.Forms.DragEventHandler(this.options_DragDrop);
            this.options.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.options_AfterSelect);
            this.options.MouseMove += new System.Windows.Forms.MouseEventHandler(this.options_MouseMove);
            this.options.BeforeSelect += new System.Windows.Forms.TreeViewCancelEventHandler(this.options_BeforeSelect);
            this.options.KeyDown += new System.Windows.Forms.KeyEventHandler(this.options_KeyDown);
            this.options.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.options_ItemDrag);
            this.options.DragOver += new System.Windows.Forms.DragEventHandler(this.options_DragOver);
            // 
            // dlgExport
            // 
            this.dlgExport.DefaultExt = "txt";
            this.dlgExport.Filter = "Text files (*.txt)|*.txt";
            // 
            // btnRecent
            // 
            this.btnRecent.Name = "btnRecent";
            this.btnRecent.Size = new System.Drawing.Size(182, 22);
            this.btnRecent.Text = "Recent Files";
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(179, 6);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(767, 466);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.panelPreview);
            this.Controls.Add(this.statusline);
            this.Controls.Add(this.menuStrip1);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(775, 500);
            this.Name = "Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Dialog Designer";
            this.Load += new System.EventHandler(this.Main_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Main_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Main_KeyDown);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.dlgMenu.ResumeLayout(false);
            this.panelPreview.ResumeLayout(false);
            this.dlgMove.ResumeLayout(false);
            this.dlgScript.ResumeLayout(false);
            this.groupScript.ResumeLayout(false);
            this.groupScript.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.groupOptions.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem btnFile;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusline;
        private System.Windows.Forms.ToolStripStatusLabel lblStatus;
        private System.Windows.Forms.ContextMenuStrip dlgMenu;
        private System.Windows.Forms.ToolStripMenuItem btnAdd;
        private System.Windows.Forms.ToolStripMenuItem btnDelete;
        private System.Windows.Forms.ToolStripMenuItem btnNew;
        private System.Windows.Forms.ToolStripMenuItem btnOpen;
        private System.Windows.Forms.ToolStripMenuItem btnSave;
        private System.Windows.Forms.ToolStripMenuItem btnSaveAs;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.OpenFileDialog dlgOpen;
        private System.Windows.Forms.SaveFileDialog dlgSave;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem variablesToolStripMenuItem;
        private System.Windows.Forms.Panel panelPreview;
        private System.Windows.Forms.Button btnPreview;
        private System.Windows.Forms.Label lblPreview;
        private System.Windows.Forms.Timer timerPreview;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.ToolStripMenuItem btnAddBefore;
        private System.Windows.Forms.ToolStripMenuItem btnAddAfter;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem btnDeleteTree;
        private System.Windows.Forms.ContextMenuStrip dlgMove;
        private System.Windows.Forms.ToolStripMenuItem btnMoveBefore;
        private System.Windows.Forms.ToolStripMenuItem btnMoveAfter;
        private System.Windows.Forms.ToolStripMenuItem btnMoveChild;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.RichTextBox script;
        private System.Windows.Forms.Label lblScript;
        private System.Windows.Forms.CheckBox checkShow;
        private System.Windows.Forms.CheckBox checkCond;
        private System.Windows.Forms.RichTextBox txtCond;
        private System.Windows.Forms.Label lblCondValue;
        private System.Windows.Forms.GroupBox groupScript;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TreeView options;
        private System.Windows.Forms.GroupBox groupOptions;
        private System.Windows.Forms.Label lblSpeaker;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.ContextMenuStrip dlgScript;
        private System.Windows.Forms.ToolStripMenuItem copyFirstLineToTitle;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpTopicsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem btnExportAGS;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.SaveFileDialog dlgExport;
        private System.Windows.Forms.ToolStripMenuItem btnRecent;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
    }
}

