﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Diagnostics;

namespace DialogDesigner
{
    public partial class ExportSettings : Form
    {
        private const string REGEX_URL = "http://www.radsoftware.com.au/articles/regexlearnsyntax.aspx";
        private const string CATASTROPHIC_URL = "http://www.regular-expressions.info/catastrophic.html";
        private const string TOOLTIP_DEL = "Removes the selected pattern from the list.";

        public ExportSettings()
        {
            InitializeComponent();

            toolTip.SetToolTip(btnDelSearch, TOOLTIP_DEL);
            toolTip.SetToolTip(btnDelReplace, TOOLTIP_DEL);

            checkDialog.Checked = false;
            checkDialog_CheckedChanged(this, null);
            checkTurnOffSelectedOptions.Checked = Properties.Settings.Default.AutomaticallyTurnOffSelectedOption;                

            // Ladataan talletetut patternit
            if ( Properties.Settings.Default.SearchPatterns != null )
                foreach (string pattern in Properties.Settings.Default.SearchPatterns)
                    boxDialogSearch.Items.Add(pattern);

            if (Properties.Settings.Default.ReplacePatterns != null)
                foreach (string pattern in Properties.Settings.Default.ReplacePatterns)
                    boxDialogReplace.Items.Add(pattern);

            checkDialog.Checked = Properties.Settings.Default.UseCustomDialogExport;
            checkCapitalize.Checked = Properties.Settings.Default.DialogCapitalize;
            
            if (Properties.Settings.Default.UsedSearchPattern >= 0 && boxDialogSearch.Items.Count > 0)
                boxDialogSearch.SelectedIndex = Properties.Settings.Default.UsedSearchPattern;
            if (Properties.Settings.Default.UsedReplacePattern >= 0 && boxDialogReplace.Items.Count > 0)
                boxDialogReplace.SelectedIndex = Properties.Settings.Default.UsedReplacePattern;            
        }

        private void checkDialog_CheckedChanged(object sender, EventArgs e)
        {
            bool en = checkDialog.Checked;
            boxDialogReplace.Enabled = en;
            boxDialogSearch.Enabled = en;
            textDialogTest.Enabled = en;
            checkCapitalize.Enabled = en;

            btnDelSearch.Enabled = boxDialogSearch.Enabled && boxDialogSearch.SelectedIndex >= 0;
            btnDelReplace.Enabled = boxDialogReplace.Enabled && boxDialogReplace.SelectedIndex >= 0;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void UpdateDialogTest(object sender, EventArgs e)
        {
            if (textDialogTest.Text.Length == 0)
                return;

            string text = textDialogTest.Text;

            try
            {
                if (checkCapitalize.Checked)
                {
                    Match m = Regex.Match(text, boxDialogSearch.Text);
                    if (m.Success)
                    {
                        foreach (Capture c in m.Groups[ScriptCommand.GROUP_TO_UPPER].Captures)
                        {
                            text = text.Insert(c.Index, c.Value.Substring(0, 1).ToUpper());
                            text = text.Remove(c.Index + 1, 1);
                        }
                    }
                }

                textDialogOutput.Text = Regex.Replace(text,
                                                      boxDialogSearch.Text,
                                                      boxDialogReplace.Text);
            }
            catch (Exception)
            {
                textDialogOutput.Text = "** Invalid expression **";
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start(REGEX_URL);
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (checkDialog.Checked && (boxDialogSearch.Text.Length == 0 || boxDialogReplace.Text.Length == 0))
            {
                ErrorBox.Show("Specify both a search pattern and a replacement pattern!");
                return;
            }

            int searchIndex = boxDialogSearch.SelectedIndex;
            int replaceIndex = boxDialogReplace.SelectedIndex;

            // Lisätään uudet luetteloihin
            if (searchIndex < 0)
            {
                if (boxDialogSearch.Text.Length > 0 && checkDialog.Checked)
                    boxDialogSearch.Items.Add(boxDialogSearch.Text);
                searchIndex = boxDialogSearch.Items.Count - 1;
            }
            if (replaceIndex < 0)
            {
                if (boxDialogReplace.Text.Length > 0 && checkDialog.Checked)
                    boxDialogReplace.Items.Add(boxDialogReplace.Text);
                replaceIndex = boxDialogReplace.Items.Count - 1;
            }

            // Tallennetaan luettelot
            if (Properties.Settings.Default.SearchPatterns == null)
                Properties.Settings.Default.SearchPatterns =
                    new System.Collections.Specialized.StringCollection();
            if (Properties.Settings.Default.ReplacePatterns == null)
                Properties.Settings.Default.ReplacePatterns =
                    new System.Collections.Specialized.StringCollection();

            Properties.Settings.Default.SearchPatterns.Clear();
            Properties.Settings.Default.ReplacePatterns.Clear();

            foreach (string pattern in boxDialogSearch.Items)
                Properties.Settings.Default.SearchPatterns.Add(pattern);
            foreach (string pattern in boxDialogReplace.Items)
                Properties.Settings.Default.ReplacePatterns.Add(pattern);

            // Tallennetaan muut asetukset
            Properties.Settings.Default.UseCustomDialogExport = checkDialog.Checked;
            Properties.Settings.Default.UsedSearchPattern = searchIndex;
            Properties.Settings.Default.UsedReplacePattern = replaceIndex;
            Properties.Settings.Default.DialogCapitalize = checkCapitalize.Checked;
            Properties.Settings.Default.AutomaticallyTurnOffSelectedOption = checkTurnOffSelectedOptions.Checked;

            Close();
        }

        private void boxDialogReplace_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnDelReplace.Enabled = true;
        }

        private void boxDialogSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnDelSearch.Enabled = true;
        }

        private void boxDialogSearch_TextUpdate(object sender, EventArgs e)
        {
            btnDelSearch.Enabled = false;
        }

        private void boxDialogReplace_TextUpdate(object sender, EventArgs e)
        {
            btnDelReplace.Enabled = false;
        }

        private void btnDelSearch_Click(object sender, EventArgs e)
        {
            int index = boxDialogSearch.SelectedIndex;

            if (index < 0)
                return;

            boxDialogSearch.Items.RemoveAt(index);
            boxDialogSearch.Text = "";
            btnDelSearch.Enabled = false;
        }

        private void btnDelReplace_Click(object sender, EventArgs e)
        {
            int index = boxDialogReplace.SelectedIndex;

            if (index < 0)
                return;

            boxDialogReplace.Items.RemoveAt(index);
            boxDialogReplace.Text = "";
            btnDelReplace.Enabled = false;
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start(CATASTROPHIC_URL);
        }

        
    }
}
