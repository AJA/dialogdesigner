﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DialogDesigner
{
    public class ScriptStatus
    {
        public ScriptStatus()
        {
            Line = 0;
            DontHideOption = false;
            DontShowChildren = false;
            ShowConditionals = true;
        }

        public int Line { get; set; }
        public bool DontHideOption { get; set; }
        public bool DontShowChildren { get; set; }
        public bool ShowConditionals { get; set; }
    }

    public class ScriptWarningException : Exception
    {        
        public ScriptWarningException(string msg) : base(msg)
        {
        }
    }
}
