﻿using System;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Text;
using System.IO;

namespace Utils
{
    class RichTextBoxControl
    {
        const int WM_SETREDRAW = 0x000B;
        const int WM_USER = 0x400;
        const int EM_GETEVENTMASK = (WM_USER + 59);
        const int EM_SETEVENTMASK = (WM_USER + 69);

        [DllImport("user32", CharSet = CharSet.Auto)]
        private extern static IntPtr SendMessage(IntPtr hWnd, int msg, int wParam, IntPtr lParam);

        /// <summary>
        /// Estää tekstiruudun päivityksen
        /// </summary>
        /// <param name="box"></param>
        /// <returns></returns>
        public static IntPtr LockRichTextBox(RichTextBox box)
        {
            //Console.WriteLine("SendMessage (LockRichTextBox)");
            // Pysäytetään päivitys
            SendMessage(box.Handle, WM_SETREDRAW, 0, IntPtr.Zero);
            // Estetään tapahtumat
            return SendMessage(box.Handle, EM_GETEVENTMASK, 0, IntPtr.Zero);                
        }

        /// <summary>
        /// Sallii tekstiruudun päivityksen
        /// </summary>
        /// <param name="box"></param>
        /// <param name="eventMask"></param>
        public static void UnlockRichTextBox(RichTextBox box, IntPtr eventMask)
        {
            //Console.WriteLine("SendMessage (UnlockRichTextBox)");
            // Sallitaan tapahtumat
            SendMessage(box.Handle, EM_SETEVENTMASK, 0, eventMask);
            // Käynnistetään piirto
            SendMessage(box.Handle, WM_SETREDRAW, 1, IntPtr.Zero);
            box.Invalidate();
        }


        [StructLayout(LayoutKind.Sequential)]
        struct SCROLLINFO
        {
            public uint cbSize;
            public uint fMask;
            public int nMin;
            public int nMax;
            public uint nPage;
            public int nPos;
            public int nTrackPos;
        }

        private enum ScrollBarDirection
        {
            SB_HORZ = 0,
            SB_VERT = 1,
            SB_CTL = 2,
            SB_BOTH = 3
        }

        private enum ScrollInfoMask
        {
            SIF_RANGE = 0x1,
            SIF_PAGE = 0x2,
            SIF_POS = 0x4,
            SIF_DISABLENOSCROLL = 0x8,
            SIF_TRACKPOS = 0x10,
            SIF_ALL = SIF_RANGE + SIF_PAGE + SIF_POS + SIF_TRACKPOS
        }

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool GetScrollInfo(IntPtr hwnd, int fnBar, ref SCROLLINFO lpsi);

        [DllImport("user32.dll")]
        static extern int SetScrollInfo(IntPtr hwnd, int fnBar, [In] ref SCROLLINFO
           lpsi, bool fRedraw);

        public static int GetScroll(RichTextBox box)
        {
            //Console.WriteLine("GetScrollInfo (GetScroll)");
            SCROLLINFO si = new SCROLLINFO();
            si.cbSize = (uint)Marshal.SizeOf(si);
            si.fMask = (int)ScrollInfoMask.SIF_POS;
            GetScrollInfo(box.Handle, (int)ScrollBarDirection.SB_VERT, ref si);
            return si.nPos;
        }

        public static void SetScroll(RichTextBox box, int pos)
        {
            //Console.WriteLine("SetScrollInfo (SetScroll)");
            SCROLLINFO si = new SCROLLINFO();
            si.cbSize = (uint)Marshal.SizeOf(si);
            si.fMask = (int)ScrollInfoMask.SIF_POS;
            si.nPos = pos;
            SetScrollInfo(box.Handle, (int)ScrollBarDirection.SB_VERT, ref si, true);
        }
    }

    public static class StringControl
    {
        public static string TruncatePath(string path, int length)
        {
            if (path.Length > length)
            {

                string root = Path.GetPathRoot(path);
                string file = Path.GetFileName(path);
                int dirlength = length - root.Length - file.Length;
                string dir = Path.GetDirectoryName(path).Substring(root.Length);

                if (dirlength >= 0)
                {
                    int startIndex = dir.IndexOf(Path.DirectorySeparatorChar, dir.Length - dirlength - 1);
                    if (startIndex >= 0)
                    {
                        dir = dir.Substring(startIndex);
                    }
                }
                else
                    dir = "";
                dir = dir.Insert(0, "...");

                path = root + dir + "\\" + file;
            }

            return path;
        }
    }
}