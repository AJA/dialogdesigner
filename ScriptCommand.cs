﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.IO;

namespace DialogDesigner
{
    public static class ScriptCommand
    {
        public const string GROUP_TO_UPPER = "name";

        public enum Name
        {
            ERROR, Stop, Return, OptionOn, OptionOff, OptionOffForever, SiblingsOff,
            LeaveOn, DontShowChildren, OffForever, ShowConditionalsOnly, On, Off, If, Set,
            GotoDialog, GotoPrevious
        }

        private const string IGNORE_COMMENT = "(?=\\s*(//|$))";        

        private static SortedList<Name, string> patterns_;
        private static List<Name> references_;
        private static List<Name> custom_;
        private static List<Name> commands_;

        private static bool convertDlg_;
        private static bool dlgFirstToUpper_;
        private static string dlgSearchPattern_;
        private static string dlgReplacePattern_;
        private static bool turnOffSelectedOption_;

        private static bool publicVersion_;

        public static bool AutomaticallyTurnOffSelectedOption
        {
            get { return turnOffSelectedOption_; }
        }

        /// <summary>
        /// Alustaa luokan toimintakuntoon.
        /// </summary>
        public static void Init( bool publicVersion )
        {
            publicVersion_ = publicVersion;
            convertDlg_ = false;

            patterns_ = new SortedList<Name, string>();
            references_ = new List<Name>();
            custom_ = new List<Name>();
            commands_ = new List<Name>();

            // Listataan kaikki patternit
            patterns_.Add(Name.Stop, "^stop");
            patterns_.Add(Name.Return, "^return");
            patterns_.Add(Name.OptionOn, "^option-on (?<ref>[1-9][0-9]*)");
            patterns_.Add(Name.OptionOff, "^option-off (?<ref>[1-9][0-9]*)");
            patterns_.Add(Name.OptionOffForever, "^option-off-forever (?<ref>[1-9][0-9]*)");
            patterns_.Add(Name.SiblingsOff, "^#siblings-off");
            patterns_.Add(Name.LeaveOn, "^\\$leave-on");
            patterns_.Add(Name.DontShowChildren, "^\\$dont-show-children");
            patterns_.Add(Name.OffForever, "^#off-forever");
            patterns_.Add(Name.ShowConditionalsOnly, "^\\$show-conditionals-only");
            patterns_.Add(Name.On, "^#on( (?<ref>[1-9][0-9]*))+");
            patterns_.Add(Name.Off, "^#off( (?<ref>[1-9][0-9]*))+");
            //patterns_.Add(Name.If, "^#if\\s*\\((?<cond>[^{]*?)\\)\\s*{(\\s*(?!\\s*(return|stop|\\$.*))(?<cmd>[^;{}]*);)+\\s*}");
            //patterns_.Add(Name.If, "^#if\\s*\\((?<cond>((?<iscmd>is(?<istype>On|Off)\\((?<ref>[1-9][0-9]*)\\))|[^{])*?)\\)\\s*{(\\s*(?!\\s*(return|stop|\\$.*))(?<cmd>[^;{}]*);)+\\s*}");
            patterns_.Add(Name.If, "^#if\\s*\\((?<cond>((?<iscmd>is(?<istype>On|Off)\\((?<ref>[1-9][0-9]*)\\))|[^{])*?)\\)\\s*{(\\s*(?!\\s*(goto-dialog|\\$.*))(?<cmd>[^;{}]*);)+\\s*}");
            patterns_.Add(Name.Set, "^#set (?<var>[\\w\\(\\)\\[\\]]+)(?<op> ?= ?(?<val>-?[1-9][0-9]*|[0-9]|true|false)|\\+\\+|--)");
            patterns_.Add(Name.GotoDialog, "^goto-dialog [0-9]+");
            patterns_.Add(Name.GotoPrevious, "^goto-previous");

            // Listataan ne komennot, joissa on viite
            references_.Add(Name.OptionOn);
            references_.Add(Name.OptionOff);
            references_.Add(Name.OptionOffForever);
            references_.Add(Name.On);
            references_.Add(Name.Off);
            references_.Add(Name.If); // isOn/Off yhteydessä

            // Listataan ne komennot, jotka tarvitsee muuntaa AGS exporttia varten
            custom_.Add(Name.SiblingsOff);
            custom_.Add(Name.LeaveOn);
            custom_.Add(Name.DontShowChildren);
            custom_.Add(Name.OffForever);
            custom_.Add(Name.ShowConditionalsOnly);
            custom_.Add(Name.On);
            custom_.Add(Name.Off);
            custom_.Add(Name.If);
            custom_.Add(Name.Set);
            custom_.Add(Name.Return);
            custom_.Add(Name.Stop);
            custom_.Add(Name.GotoPrevious);
            
            // Listataan ne komennot, jotka pitävät sisällään muita komentoja
            commands_.Add(Name.If);
        }

        /// <summary>
        /// Asettaa käyttöön dialogirivien muunnoksen annetuilla regex-patterneilla
        /// </summary>
        /// <param name="on"></param>
        /// <param name="search"></param>
        /// <param name="replace"></param>
        public static void SetDialogConversion(bool on, StringCollection searchList, int searchIndex,
                                               StringCollection replaceList, int replaceIndex,
                                               bool firstToUpper, bool turnOffSelectedOption)
        {
            if (on && searchList != null && replaceList != null)
            {
                convertDlg_ = on;
                dlgSearchPattern_ = searchList[ searchIndex ];
                dlgReplacePattern_ = replaceList[ replaceIndex ];
                dlgFirstToUpper_ = firstToUpper;
            }

            turnOffSelectedOption_ = turnOffSelectedOption;
        }

        public static string GetHighlightRegex()
        {
            string regex = "(";
            foreach (KeyValuePair<Name, string> p in patterns_)
            {
                regex += p.Value + "|";
            }
            regex = regex.Remove(regex.Length - 1);
            return regex + ")" + IGNORE_COMMENT; //(?=\\s*(//|$))";
        }

        /// <summary>
        /// Tutkii, onko tekstissä komentoa, jossa on viite ja palauttaa merkatun version
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string MarkReferences(string text, string pre, string post)
        {
            foreach (KeyValuePair<Name, string> p in patterns_)
            {
                Match m = Regex.Match(text, p.Value);
                if (m.Success)
                {
                    if (commands_.Contains(p.Key))
                    {
                        // Sisältää muita komentoja
                        int i = 0;
                        foreach (Capture c in m.Groups["cmd"].Captures)
                        {
                            string s = MarkReferences(c.Value, pre, post);
                            text = text.Remove(c.Index + i, c.Length);
                            text = text.Insert(c.Index + i, s);
                            i += s.Length - c.Length;
                        }
                    }
                    if (references_.Contains(p.Key))
                    {
                        int i = 0;
                        // Referenssejä merkattavaksi
                        foreach (Capture c in m.Groups["ref"].Captures)
                        {
                            text = text.Insert(c.Index + i, pre);
                            i += pre.Length;
                            text = text.Insert(c.Index + c.Length + i, post);
                            i += post.Length;
                        }
                    }                    
                }
            }
            return text;
        }

        /// <summary>
        /// Palauttaa muunnettavan komennon vaikutukset ja kirjoitettavan tekstin
        /// </summary>
        /// <param name="option"></param>
        /// <param name="text"></param>
        /// <returns></returns>
        public static Name ToAGS(StreamWriter ags, OptionData option, ref string text, int line)
        {
            return ToAGS(ags, option, ref text, line, false);
        }

        private static Name ToAGS(StreamWriter ags, OptionData option, ref string text, int line, bool insideIf)
        {
            foreach (KeyValuePair<Name, string> p in patterns_)
            {
                if (custom_.Contains(p.Key))
                {
                    // Tarvitsee muuntaa jos löytyy
                    Match m = Regex.Match(text, p.Value + IGNORE_COMMENT);
                    if (m.Success)
                    {
                        // Muunnetaan ja haetaan vaikutukset
                        return ToAGS(ags, option, p.Key, m, line, ref text, insideIf);
                    }
                }
            }

            Name n = GetName(text);
            //if (n == Name.ERROR || (n != Name.Return && n != Name.Stop))
            if (n == Name.ERROR)
            {
                // Ei korvattavaa
                //if (!publicVersion_)
                if (convertDlg_)
                    DialogToAGS(ags, ref text);
            }

            text += ags.NewLine;
            return n;
        }


        private static void DialogToAGS(StreamWriter ags, ref string text)
        {
            //Match m = Regex.Match(text, "(?<name>^\\w+):(?<text>[^{]+?)({(?<mood>.+)})?$");
            if (dlgFirstToUpper_)
            {
                Match m = Regex.Match(text, dlgSearchPattern_);
                if (m.Success)
                {
                    foreach (Capture c in m.Groups[GROUP_TO_UPPER].Captures)
                    {
                        text = text.Insert(c.Index, c.Value.Substring(0, 1).ToUpper());
                        text = text.Remove(c.Index + 1, 1);
                    }
                }
            }
            
            text = Regex.Replace(text, dlgSearchPattern_, dlgReplacePattern_);

            /*
            if (m.Success)
            {
                string name = m.Groups["name"].Value.Trim();
                name = name.Insert(0, name.Substring(0, 1).ToUpper());
                name = name.Remove(1, 1);
                string speech = m.Groups["text"].Value.Trim().Replace("\"", "\\\"");
                text = "  say( c" + name + ", \"" + speech + "\"";

                if (m.Groups["mood"].Captures.Count > 0)
                    text += ", " + m.Groups["mood"].Value.Trim();

                text += " );";
            }*/
            //ags.WriteLine(text);
        }


        private static Name ToAGS(StreamWriter ags, OptionData option,
            Name name, Match m, int line, ref string text, bool insideIf)
        {
            switch (name)
            {
                case Name.If:
                    string condition = m.Groups["cond"].Value;
                    condition = ReplaceIsOnOff(option.Dialog, condition, true);

                    text = "  if (" + condition + ") {" + ags.NewLine;
                    //ags.WriteLine("  if (" + condition + ") {");

                    foreach (Capture c in m.Groups["cmd"].Captures)
                    {
                        // Jokainen komento tutkien
                        string cmd = c.Value;
                        ToAGS(ags, option, ref cmd, line, true);
                        text += cmd;
                    }

                    text += "  }" + ags.NewLine;
                    //ags.WriteLine("  }");
                    return Name.ERROR;

                case Name.Set:
                    text = "  " + m.Groups["var"].Value + m.Groups["op"].Value + ";" + ags.NewLine;
                    //ags.WriteLine("  " + m.Groups["var"].Value + m.Groups["op"].Value + ";");
                    return Name.ERROR;

                case Name.Return:
                    if (insideIf)
                    {
                        text = "    return RUN_DIALOG_RETURN;" + ags.NewLine;
                        return Name.ERROR;
                    }
                    
                    text = m.Value + ags.NewLine;
                    return name;                    

                case Name.Stop:
                    if (insideIf)
                    {
                        text = "    return RUN_DIALOG_STOP_DIALOG;" + ags.NewLine;
                        return Name.ERROR;
                    }

                    text = m.Value + ags.NewLine;
                    return name;

                case Name.GotoPrevious:
                    if (insideIf)
                    {
                        text = "    return RUN_DIALOG_GOTO_PREVIOUS;" + ags.NewLine;
                        return Name.ERROR;
                    }

                    text = m.Value + ags.NewLine;
                    return name;

                default:
                    Effects e = GetEffects(option, name, m, true);
                    text = "";
                    foreach (int j in e.Ons)
                        text += "option-on " + j + ags.NewLine;
                    foreach (int j in e.Offs)
                        text += "option-off " + j + ags.NewLine;
                    foreach (int j in e.OffForevers)
                        text += "option-off-forever " + j + ags.NewLine;
                    return name;
            }            
        }

        /// <summary>
        /// Palauttaa tekstissä olevan komennon nimen, jos löytyy
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static Name GetName(string text)
        {
            foreach (KeyValuePair<Name, string> p in patterns_)
            {
                Match m = Regex.Match(text, p.Value + IGNORE_COMMENT);
                if (m.Success)
                    return p.Key;
            }
            return Name.ERROR;
        }

        /// <summary>
        /// Tutkii, onko tekstissä jokin komento ja jos on, palauttaa sen vaikutukset.
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static Effects? GetEffects(OptionData option, string text, bool readOnly)
        {            
            foreach (KeyValuePair<Name, string> p in patterns_)
            {
                Match m = Regex.Match(text, p.Value + IGNORE_COMMENT);
                if (m.Success)
                    return GetEffects( option, p.Key, m, readOnly );
            }
            // Ei löytynyt
            return null;
        }

        /// <summary>
        /// Palauttaa vaihtoehdon skriptissä olleen komennon vaikutukset
        /// </summary>
        /// <param name="option"></param>
        /// <param name="name"></param>
        /// <param name="m"></param>
        /// <returns></returns>
        private static Effects GetEffects(OptionData option, Name name, Match m, bool readOnly)
        {
            Effects e = new Effects();
            e.Ons = new List<int>();
            e.Offs = new List<int>();
            e.OffForevers = new List<int>();
            e.ConditionalOns = new List<int>();

            int i;
            switch (name)
            {
                case Name.Stop:
                    e.Stop = true;
                    break;

                case Name.Return:
                    e.Return = true;
                    break;

                case Name.OptionOn:
                    i = Convert.ToInt32(m.Groups["ref"].Value);
                    if (i >= option.Dialog.Options.Count)
                        throw new Exception("Invalid option index: " + m.ToString());
                    e.Ons.Add(i);
                    break;

                case Name.OptionOff:
                    i = Convert.ToInt32(m.Groups["ref"].Value);
                    if (i >= option.Dialog.Options.Count)
                        throw new Exception("Invalid option index: " + m.ToString());
                    e.Offs.Add(i);
                    break;

                case Name.OptionOffForever:
                    i = Convert.ToInt32(m.Groups["ref"].Value);
                    if (i >= option.Dialog.Options.Count)
                        throw new Exception("Invalid option index: " + m.ToString());
                    e.OffForevers.Add(i);
                    break;

                case Name.SiblingsOff:
                    if (option.Parent == null)
                        throw new Exception("Can't turn off siblings when option has no parent: " + m.ToString());
                    foreach (OptionData sibling in option.Parent.Children)
                    {
                        if (!sibling.Equals(option))
                            e.Offs.Add(sibling.Index);
                    }
                    break;

                case Name.LeaveOn:
                    e.LeaveOn = true;
                    break;

                case Name.DontShowChildren:
                    e.DontShowChildren = true;
                    break;

                case Name.OffForever:
                    e.OffForevers.Add(option.Index);
                    e.LeaveOn = true;
                    break;

                case Name.ShowConditionalsOnly:
                    e.ShowConditionalsOnly = true;
                    break;

                case Name.On:
                    foreach (Capture c in m.Groups["ref"].Captures)
                    {
                        i = Convert.ToInt32(c.Value.ToString().Trim());
                        if (i >= option.Dialog.Options.Count)
                            throw new Exception("Invalid option index: " + m.ToString());
                        e.Ons.Add(i);
                    }
                    break;

                case Name.Off:
                    foreach (Capture c in m.Groups["ref"].Captures)
                    {
                        i = Convert.ToInt32(c.Value.ToString().Trim());
                        if (i >= option.Dialog.Options.Count)
                            throw new Exception("Invalid option index: " + m.ToString());
                        e.Offs.Add(i);
                    }
                    break;

                case Name.If:
                    string cond = m.Groups["cond"].Value;
                    //cond = ReplaceIsOnOff(option, m, false);

                    if (Conditional.Evaluate(cond, option.Dialog))
                    {
                        GetSubEffects(option, m, ref e, readOnly);
                    }
                    break;

                case Name.Set:
                    string varStr = m.Groups["var"].Value;
                    Variable var = null;
                    // Etsitään muuttuja
                    foreach (KeyValuePair<int, Variable> v in option.Dialog.Variables)
                    {
                        if (v.Value.Name == varStr)
                        {
                            var = v.Value;
                            break;
                        }
                    }
                    if (var == null)
                        throw new ScriptWarningException("Variable not defined: " + m.ToString());

                    if (!readOnly)
                    {
                        // Vaihdetaan arvoa
                        string opStr = m.Groups["op"].Value.Trim();
                        if (opStr[0] == '=')
                        {
                            if (m.Groups["val"].Value.Equals("true"))
                                var.Value = 1;
                            else if (m.Groups["val"].Value.Equals("false"))
                                var.Value = 0;
                            else
                                var.Value = Convert.ToInt32(m.Groups["val"].Value);
                        }
                        else if (opStr[0] == '-')
                            var.Value--;
                        else
                            var.Value++;
                    }
                    break;

                case Name.GotoDialog:
                    e.Return = true;
                    break;

                case Name.GotoPrevious:
                    e.Stop = true;
                    break;

                default:
                    break;
            }

            return e;
        }

        private static void GetSubEffects(OptionData option, Match m, ref Effects e, bool readOnly)
        {
            foreach (Capture c in m.Groups["cmd"].Captures)
            {
                // Käsittele komennot
                Effects? ifEff = GetEffects(option, c.Value, readOnly);

                if (ifEff == null)
                    throw new Exception("Invalid command: " + c.Value
                        + "\nInside command: " + m.ToString());

                e.Ons.AddRange(ifEff.Value.Ons);
                e.Offs.AddRange(ifEff.Value.Offs);
                e.OffForevers.AddRange(ifEff.Value.OffForevers);
                e.ConditionalOns.AddRange(ifEff.Value.ConditionalOns);

                if (ifEff.Value.Stop)
                    e.Stop = true;
                if (ifEff.Value.Return)
                    e.Return = true;

                if (ifEff.Value.DontShowChildren || ifEff.Value.LeaveOn
                    || ifEff.Value.ShowConditionalsOnly)
                    throw new Exception("Command not allowed inside #if command: "
                        + c.Value);
            }
        }

        /*private static string ReplaceIsOnOff(OptionData option, Match m, bool export)
        {
            string text = m.Groups["cond"].Value;
            int diff = m.Groups["cond"].Index;

            for (int i = 0; i < m.Groups["istype"].Captures.Count; i++)
            {                
                Capture c = m.Groups["istype"].Captures[i];
                Capture id = m.Groups["ref"].Captures[i];
                Capture cmd = m.Groups["iscmd"].Captures[i];
                string replacement;

                if (export)
                    replacement = "this.GetOptionState(" + id.Value + ") == eOption" + c.Value;
                else {
                    int opt = Convert.ToInt32( id.Value );
                    bool isOn = option.Dialog.Options[opt].Visible == OptionData.OptionState.On;

                    if ((c.Value == "On" && isOn) || (c.Value == "Off" && !isOn))
                        replacement = "1==1";
                    else
                        replacement = "0==1";
                }

                int len = text.Length;
                // Poistetaan alkuperäinen komento ja korvataan
                text = text.Remove(cmd.Index - diff, cmd.Length);
                text = text.Insert(cmd.Index - diff, replacement);
                diff += len - text.Length;
            }

            return text;
        }*/

        public static string ReplaceIsOnOff(Dialog dlg, string cond, bool export)
        {
            const string IS_ONOFF = "is(?<istype>On|Off)\\((?<ref>[1-9][0-9]*)\\)";
            int diff = 0;

            MatchCollection matches = Regex.Matches(cond, IS_ONOFF);
            foreach (Match m in matches)
            {
                Group c = m.Groups["istype"];
                Group id = m.Groups["ref"];
                string replacement;

                if ( export )
                {
                    string type = (c.Value == "On") ? "=" : "!";
                    replacement = "this.GetOptionState(" + id.Value + ") " + type + "= eOptionOn";
                }
                else
                {
                    int opt = Convert.ToInt32(id.Value);
                    bool isOn = dlg.Options[opt].Visible == OptionData.OptionState.On;

                    if ((c.Value == "On" && isOn) || (c.Value == "Off" && !isOn))
                        replacement = "1==1";
                    else
                        replacement = "0==1";
                }

                int len = cond.Length;
                // Poistetaan alkuperäinen komento ja korvataan
                cond = cond.Remove(m.Index - diff, m.Length);
                cond = cond.Insert(m.Index - diff, replacement);
                diff += len - cond.Length;
            }

            return cond;
        }
        
        /// <summary>
        /// Tietue kuvaa tietyn komennon vaikutuksia muihin valintoihin
        /// </summary>
        public struct Effects
        {
            public List<int> Ons;
            public List<int> Offs;
            public List<int> OffForevers;
            public List<int> ConditionalOns;
            public bool Return;
            public bool Stop;
            public bool DontShowChildren;
            public bool ShowConditionalsOnly;
            public bool LeaveOn;
        }

    }
}
