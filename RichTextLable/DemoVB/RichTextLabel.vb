' ==============================================================================
'
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
' ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
' THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
'
' � 2002 - 2004, LaMarvin. All Rights Reserved.
' ==============================================================================

Imports System.ComponentModel


'<summary>Displays RTF text in a read-only, label-like form.</summary>
Public Class RichTextLabel
  Inherits System.Windows.Forms.UserControl

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'UserControl overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Private WithEvents RichTextBox1 As System.Windows.Forms.RichTextBox
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.RichTextBox1 = New System.Windows.Forms.RichTextBox
    Me.SuspendLayout()
    '
    'RichTextBox1
    '
    Me.RichTextBox1.BackColor = System.Drawing.SystemColors.Control
    Me.RichTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
    Me.RichTextBox1.CausesValidation = False
    Me.RichTextBox1.Cursor = System.Windows.Forms.Cursors.Arrow
    Me.RichTextBox1.Dock = System.Windows.Forms.DockStyle.Fill
    Me.RichTextBox1.ForeColor = System.Drawing.SystemColors.ControlText
    Me.RichTextBox1.Location = New System.Drawing.Point(0, 0)
    Me.RichTextBox1.Name = "RichTextBox1"
    Me.RichTextBox1.ReadOnly = True
    Me.RichTextBox1.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None
    Me.RichTextBox1.Size = New System.Drawing.Size(150, 150)
    Me.RichTextBox1.TabIndex = 0
    Me.RichTextBox1.TabStop = False
    Me.RichTextBox1.Text = ""
    '
    'RichTextLabel
    '
    Me.Controls.Add(Me.RichTextBox1)
    Me.Name = "RichTextLabel"
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Public interface "
  <Category("Appearance")> _
  Public Property BorderStyle() As BorderStyle
    Get
      Return Me.RichTextBox1.BorderStyle
    End Get
    Set(ByVal Value As BorderStyle)
      Me.RichTextBox1.BorderStyle = Value
    End Set
  End Property


  ' The name of the embedded RTF file.
  Private _RtfResourceName As String

  ' This is the string form of the Editor attribute, which allows the design-time support
  ' to be completely separate from the component/control implementation. 
  <System.ComponentModel.Editor( _
    "LaMarvin.Windows.Forms.RichTextLabel.Design.RtfResourceNameEditor", _
    "System.Drawing.Design.UITypeEditor")> _
  Public Property RtfResourceName() As String
    Get
      Return Me._RtfResourceName
    End Get
    Set(ByVal Value As String)
      If Value Is Nothing Then
        Me.RichTextBox1.Rtf = ""
        Me._RtfResourceName = Nothing
        Return
      End If

      ' First try to load the resource; if it's invalid, we don't want to remember its name.
      Dim Assm As System.Reflection.Assembly = Me.GetType().Assembly
      Dim RtfTextStream As System.IO.Stream = Assm.GetManifestResourceStream(Value)
      If RtfTextStream Is Nothing Then
        ' Resource doesn't exist.
        Throw New ArgumentException("RTF resource '" & Value & "' doesn't exist.")
      End If

      ' Resource loaded - remember its name and load the RTF text into the control.
      Me._RtfResourceName = Value
      Me.RichTextBox1.LoadFile(RtfTextStream, RichTextBoxStreamType.RichText)
      RtfTextStream.Close()

      ' Replace the substitution strings in the RTF text.
      Me.DoRtfSubstitutions(Assm)
    End Set
  End Property

#End Region

#Region " Implementation "


  Private Sub DoRtfSubstitutions(ByVal assm As System.Reflection.Assembly)
    Try
      ' For each entry in the substitution map, replace the substitution 
      ' string if it exists.
      For Each E As DictionaryEntry In RichTextLabel.AttrMap
        If Me.RichTextBox1.Rtf.IndexOf(CStr(E.Key)) >= 0 Then
          Me.DoSingleRtfSubstitution(assm, CStr(E.Key), E)
        End If
      Next
    Catch ex As Exception
      Trace.Write(ex.ToString())
    End Try
  End Sub


  Private Sub DoSingleRtfSubstitution( _
    ByVal assm As System.Reflection.Assembly, _
    ByVal substitutionString As String, _
    ByVal e As DictionaryEntry)

    ' Retrieve the assembly attriute.
    Dim Attrs() As Object = assm.GetCustomAttributes(DirectCast(e.Value, AttrMapEntry).AttrType, True)
    If (Not Attrs Is Nothing) AndAlso (Attrs.Length > 0) Then
      ' The attribute exists - replace the substitution string with the given attribute's property value.
      Dim PropValue As String = CStr(DirectCast(e.Value, AttrMapEntry).AttrProp.GetValue(Attrs(0), Nothing))
      Me.RichTextBox1.Rtf = Me.RichTextBox1.Rtf.Replace(substitutionString, PropValue)
    End If
  End Sub


  ' The substitution strings are held in a static table to make it easy to extend it.
  ' The _AttrMap table contains AttrMapEntry objects keyed by a substitution string. 
  ' If the string is found in an RTF file, it will be replaced by the value of the property
  ' identified by the corresponding AttrMapEntry object.
  Private Shared _AttrMap As Hashtable
  Private Shared ReadOnly Property AttrMap() As Hashtable
    Get
      If RichTextLabel._AttrMap Is Nothing Then
        RichTextLabel._AttrMap = New Hashtable
        RichTextLabel._AttrMap.Add("[Product]", New AttrMapEntry(GetType(System.Reflection.AssemblyProductAttribute), "Product"))
        RichTextLabel._AttrMap.Add("[Title]", New AttrMapEntry(GetType(System.Reflection.AssemblyTitleAttribute), "Title"))
        RichTextLabel._AttrMap.Add("[Copyright]", New AttrMapEntry(GetType(System.Reflection.AssemblyCopyrightAttribute), "Copyright"))
        RichTextLabel._AttrMap.Add("[Company]", New AttrMapEntry(GetType(System.Reflection.AssemblyCompanyAttribute), "Company"))
        ' Here you can add additional entries for other assembly attributes.
      End If
      Return RichTextLabel._AttrMap
    End Get
  End Property


  ' Maps assembly's attribute type and the attribute property info of the property
  ' whose value that will replace a substitution string in the RTF file.
  Private Class AttrMapEntry
    Public AttrType As System.Type
    Public AttrProp As System.Reflection.PropertyInfo
    Public Sub New(ByVal attrType As System.Type, ByVal propName As String)
      Debug.Assert(Not attrType Is Nothing)
      Debug.Assert(Not propName Is Nothing)
      Me.AttrType = attrType
      Me.AttrProp = attrType.GetProperty(propName, Reflection.BindingFlags.Instance Or Reflection.BindingFlags.Public)
      Debug.Assert(Not Me.AttrProp Is Nothing)
    End Sub
  End Class


  ' RichTextBox raises this event when it recognizes a properly formatted URL in the text.
  ' We'll show the URL in the user's default browser (or whatever she has registered for the
  ' URL's protocol prefix).
  Private Sub RichTextBox1_LinkClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.LinkClickedEventArgs) Handles RichTextBox1.LinkClicked
    Try
      Dim StartInfo As New System.Diagnostics.ProcessStartInfo(e.LinkText)
      StartInfo.UseShellExecute = True
      System.Diagnostics.Process.Start(StartInfo)
    Catch ex As Exception
      Trace.WriteLine(ex.ToString())
      MsgBox(ex.Message, MsgBoxStyle.Exclamation)
    End Try
  End Sub

#End Region

  
End Class
