' ==============================================================================
'
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
' ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
' THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
'
' � 2002 - 2004, LaMarvin. All Rights Reserved.
' ==============================================================================

Public Class MainForm
  Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Private WithEvents OKButton As System.Windows.Forms.Button
  Private WithEvents RichTextLabel1 As RichTextLabelDemoVB.RichTextLabel
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.RichTextLabel1 = New RichTextLabelDemoVB.RichTextLabel
    Me.OKButton = New System.Windows.Forms.Button
    Me.SuspendLayout()
    '
    'RichTextLabel1
    '
    Me.RichTextLabel1.BorderStyle = System.Windows.Forms.BorderStyle.None
    Me.RichTextLabel1.Location = New System.Drawing.Point(8, 8)
    Me.RichTextLabel1.Name = "RichTextLabel1"
    Me.RichTextLabel1.RtfResourceName = "RichTextLabelDemoVB.Text1.rtf"
    Me.RichTextLabel1.Size = New System.Drawing.Size(300, 150)
    Me.RichTextLabel1.TabIndex = 1
    '
    'OKButton
    '
    Me.OKButton.DialogResult = System.Windows.Forms.DialogResult.OK
    Me.OKButton.Location = New System.Drawing.Point(120, 168)
    Me.OKButton.Name = "OKButton"
    Me.OKButton.TabIndex = 0
    Me.OKButton.Text = "OK"
    '
    'MainForm
    '
    Me.AcceptButton = Me.OKButton
    Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
    Me.CancelButton = Me.OKButton
    Me.ClientSize = New System.Drawing.Size(314, 207)
    Me.Controls.Add(Me.OKButton)
    Me.Controls.Add(Me.RichTextLabel1)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.MaximizeBox = False
    Me.Name = "MainForm"
    Me.Text = "RichTextLabel Control Demo (VB)"
    Me.ResumeLayout(False)

  End Sub

#End Region

  Private Sub OKButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OKButton.Click
    Me.Close()
  End Sub

End Class
