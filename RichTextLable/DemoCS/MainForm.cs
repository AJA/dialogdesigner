// ==============================================================================
//
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
//
// � 2002 - 2004, LaMarvin. All Rights Reserved.
// ==============================================================================

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

namespace RichTextLabelDemoCS
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class MainForm : System.Windows.Forms.Form
	{
    private RichTextLabelDemoCS.RichTextLabel richTextLabel1;
    private System.Windows.Forms.Button OKButton;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public MainForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
      this.richTextLabel1 = new RichTextLabelDemoCS.RichTextLabel();
      this.OKButton = new System.Windows.Forms.Button();
      this.SuspendLayout();
      // 
      // richTextLabel1
      // 
      this.richTextLabel1.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.richTextLabel1.Location = new System.Drawing.Point(16, 16);
      this.richTextLabel1.Name = "richTextLabel1";
      this.richTextLabel1.RtfResourceName = "RichTextLabelDemoCS.Text1.rtf";
      this.richTextLabel1.Size = new System.Drawing.Size(280, 150);
      this.richTextLabel1.TabIndex = 0;
      // 
      // OKButton
      // 
      this.OKButton.DialogResult = System.Windows.Forms.DialogResult.OK;
      this.OKButton.Location = new System.Drawing.Point(117, 184);
      this.OKButton.Name = "OKButton";
      this.OKButton.TabIndex = 1;
      this.OKButton.Text = "OK";
      this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
      // 
      // MainForm
      // 
      this.AcceptButton = this.OKButton;
      this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
      this.CancelButton = this.OKButton;
      this.ClientSize = new System.Drawing.Size(308, 231);
      this.Controls.Add(this.OKButton);
      this.Controls.Add(this.richTextLabel1);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
      this.MaximizeBox = false;
      this.Name = "MainForm";
      this.Text = "RichTextLabel Control Demo (C#)";
      this.ResumeLayout(false);

    }
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new MainForm());
		}

    private void OKButton_Click(object sender, System.EventArgs e)
    {
      this.Close();
    }
	}
}
