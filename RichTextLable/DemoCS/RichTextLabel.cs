// ==============================================================================
//
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
//
// � 2002 - 2004, LaMarvin. All Rights Reserved.
// ==============================================================================

using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using System.Diagnostics;

namespace RichTextLabelDemoCS
{
  /// <summary>
  /// Summary description for RichTextLabel.
  /// </summary>
  public class RichTextLabel : System.Windows.Forms.UserControl
  {
    private System.Windows.Forms.RichTextBox RichTextBox1;
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.Container components = null;

    public RichTextLabel()
    {
      // This call is required by the Windows.Forms Form Designer.
      InitializeComponent();

      // TODO: Add any initialization after the InitializeComponent call

    }

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    protected override void Dispose( bool disposing )
    {
      if( disposing )
      {
        if(components != null)
        {
          components.Dispose();
        }
      }
      base.Dispose( disposing );
    }

    #region Component Designer generated code
    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.RichTextBox1 = new System.Windows.Forms.RichTextBox();
      this.SuspendLayout();
      // 
      // RichTextBox1
      // 
      this.RichTextBox1.BackColor = System.Drawing.SystemColors.Control;
      this.RichTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.RichTextBox1.CausesValidation = false;
      this.RichTextBox1.Cursor = System.Windows.Forms.Cursors.Arrow;
      this.RichTextBox1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.RichTextBox1.ForeColor = System.Drawing.SystemColors.ControlText;
      this.RichTextBox1.Location = new System.Drawing.Point(0, 0);
      this.RichTextBox1.Name = "RichTextBox1";
      this.RichTextBox1.ReadOnly = true;
      this.RichTextBox1.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
      this.RichTextBox1.Size = new System.Drawing.Size(150, 150);
      this.RichTextBox1.TabIndex = 1;
      this.RichTextBox1.TabStop = false;
      this.RichTextBox1.Text = "";
      this.RichTextBox1.LinkClicked += new System.Windows.Forms.LinkClickedEventHandler(this.RichTextBox1_LinkClicked);
      // 
      // RichTextLabel
      // 
      this.Controls.Add(this.RichTextBox1);
      this.Name = "RichTextLabel";
      this.ResumeLayout(false);

    }
    #endregion

  
    #region Public interface

    [Category("Appearance")]
    public BorderStyle BorderStyle
    {
      get
      {
        return this.RichTextBox1.BorderStyle;
      }
      set
      {
        this.RichTextBox1.BorderStyle = value;
      }
    }

  
    // The name of the embedded RTF file.
    private string  _RtfResourceName;

    // This is the string form of the Editor attribute, which allows the design-time support
    // to be completely separate from the component/control implementation. 
    [System.ComponentModel.Editor( "LaMarvin.Windows.Forms.RichTextLabel.Design.RtfResourceNameEditor", "System.Drawing.Design.UITypeEditor")]
    public string RtfResourceName  
    {
      get 
      {
        return this._RtfResourceName;
      }
      set 
      {
        if ( value == null ) 
        {
          this.RichTextBox1.Rtf = "";
          this._RtfResourceName = null;
          return;
        }

        // First try to load the resource; if it's invalid, we don't want to remember its name.
        System.Reflection.Assembly Assm = this.GetType().Assembly;
        System.IO.Stream RtfTextStream = Assm.GetManifestResourceStream(value);
        if ( RtfTextStream == null )
        {
          // Resource doesn't exist.
          throw new ArgumentException("RTF resource '" + value + "' doesn't exist.");
        }

        // Resource loaded - remember its name and load the RTF text into the control.
        this._RtfResourceName = value;
        this.RichTextBox1.LoadFile(RtfTextStream, RichTextBoxStreamType.RichText);
        RtfTextStream.Close();

        // Replace the substitution strings in the RTF text.
        this.DoRtfSubstitutions(Assm);
      }
    }

    #endregion

    #region " Implementation "


    private void DoRtfSubstitutions( System.Reflection.Assembly assm) 
    {
      try 
      {
        // for ( each entry in the substitution map, replace the substitution 
        // string if it exists.
        foreach ( DictionaryEntry E in RichTextLabel.AttrMap)
        {
          if ( this.RichTextBox1.Rtf.IndexOf((string)(E.Key)) >= 0 )
            this.DoSingleRtfSubstitution(assm, (string)(E.Key), E);
        } //
      } 
      catch (Exception ex) 
      {
        Trace.Write(ex.ToString());
      }
    }


    private void DoSingleRtfSubstitution(  
      System.Reflection.Assembly assm,  
      string  substitutionString,  
      DictionaryEntry e)
    {

      // Retrieve the assembly attriute.
      object[] Attrs = assm.GetCustomAttributes(((AttrMapEntry)e.Value).AttrType, true);
      if ((Attrs != null) && (Attrs.Length > 0)) 
      {
        // The attribute exists - replace the substitution string with the given attribute//s property value.
        string  PropValue = (string)((AttrMapEntry)e.Value).AttrProp.GetValue(Attrs[0], null);
        this.RichTextBox1.Rtf = this.RichTextBox1.Rtf.Replace(substitutionString, PropValue);
      }
    }


    // The substitution strings are held in a static table to make it easy to extend it.
    // The _AttrMap table contains AttrMapEntry objects keyed by a substitution string. 
    // if ( the string is found in an RTF file, it will be replaced by the value of the property
    // identified by the corresponding AttrMapEntry object.
    private static Hashtable _AttrMap;

    private static Hashtable AttrMap  
    {
      get 
      {
        if ( RichTextLabel._AttrMap == null ) 
        {
          RichTextLabel._AttrMap = new Hashtable();
          RichTextLabel._AttrMap.Add("[Product]", new AttrMapEntry(typeof(System.Reflection.AssemblyProductAttribute), "Product"));
          RichTextLabel._AttrMap.Add("[Title]", new AttrMapEntry(typeof(System.Reflection.AssemblyTitleAttribute), "Title"));
          RichTextLabel._AttrMap.Add("[Copyright]", new AttrMapEntry(typeof(System.Reflection.AssemblyCopyrightAttribute), "Copyright"));
          RichTextLabel._AttrMap.Add("[Company]", new AttrMapEntry(typeof(System.Reflection.AssemblyCompanyAttribute), "Company"));
          // Here you can add additional entries for other assembly attributes.
        }
        return RichTextLabel._AttrMap;
      }
    }


    // Maps assembly//s attribute type and the attribute property info of the property
    // whose value that will replace a substitution string in the RTF file.
    private class AttrMapEntry 
    {
      public System.Type AttrType;
      public System.Reflection.PropertyInfo AttrProp;
  
      public AttrMapEntry( System.Type attrType,  string  propName) 
      {
        Debug.Assert(attrType != null);
        Debug.Assert(propName != null);
        this.AttrType = attrType;
        this.AttrProp = attrType.GetProperty(propName, 
          System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public);
        Debug.Assert(this.AttrProp != null);
      }
    }


    // RichTextBox raises this event when it recognizes a properly formatted URL in the text.
    // We//ll show the URL in the user//s default browser (or whatever she has registered for the
    // URL//s protocol prefix).
    private void RichTextBox1_LinkClicked(object sender, System.Windows.Forms.LinkClickedEventArgs e)
    {
      try 
      {
        System.Diagnostics.ProcessStartInfo  StartInfo = new System.Diagnostics.ProcessStartInfo(e.LinkText);
        StartInfo.UseShellExecute = true;
        System.Diagnostics.Process.Start(StartInfo);
      } 
      catch (Exception ex) 
      {
        Trace.WriteLine(ex.ToString());
        MessageBox.Show(ex.Message, Application.ProductName, 
          MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
      }
    }
  
    #endregion

  }
}