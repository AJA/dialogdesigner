' ==============================================================================
'
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
' ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
' THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
' PARTICULAR PURPOSE.
'
' �2004 Palo Mraz, LaMarvin. All Rights Reserved.
' ==============================================================================

Imports System.Drawing.Design
Imports System.Windows.Forms
Imports System.Windows.Forms.Design
Imports System.ComponentModel
Imports System.ComponentModel.Design


'<summary>
'</summary>
Public Class RtfResourceNameEditor
  Inherits UITypeEditor

  Private _EditorService As IWindowsFormsEditorService
  Private _Value As String
  Private _NoneItemIndex As Integer

  '<summary>
  ' Alway returns the DropDown style.
  '</summary>
  Public Overloads Overrides Function GetEditStyle( _
    ByVal context As ITypeDescriptorContext) As UITypeEditorEditStyle

    Return UITypeEditorEditStyle.DropDown
  End Function


  '<summary>
  ' Displays our drop-down list control with names of the image resources from our assembly
  ' along with the special "Browse..." and "None" items.
  '</summary>
  Public Overloads Overrides Function EditValue( _
    ByVal context As ITypeDescriptorContext, _
    ByVal provider As System.IServiceProvider, _
    ByVal value As Object) As Object

    If context Is Nothing Then
      Return value
    End If

    ' Store the context information for use in the drop-down ListBox event handlers.
    Me._EditorService = DirectCast(context.GetService(GetType(IWindowsFormsEditorService)), IWindowsFormsEditorService)
    If Me._EditorService Is Nothing Then
      Return value
    End If

    Me._Value = DirectCast(value, String)

    Dim SortedNames As ArrayList = Me.GetRtfResourceNames(context.Instance.GetType().Assembly)
    SortedNames.Sort(New CaseInsensitiveComparer)

    Dim List As New EnterKeyListBox
    For Each S As String In SortedNames
      List.Items.Add(S)
      ' Select the name that is currently set.
      If String.Compare(S, Me._Value, True) = 0 Then
        List.SelectedIndex = List.Items.Count - 1
      End If
    Next

    ' Add our special none item.
    Me._NoneItemIndex = List.Items.Add("None")

    ' Wire the ListBox events and display the drop-down UI. The selection process
    ' is then handled inside the event handler methods.
    AddHandler List.Click, AddressOf Me.OnClickInList
    AddHandler List.EnterPressed, AddressOf Me.OnEnterPressedInList
    _EditorService.DropDownControl(List)

    Return Me._Value
  End Function


  Private Sub OnClickInList( _
    ByVal sender As Object, _
    ByVal e As EventArgs)

    Me.ProcessSelectedListItem(DirectCast(sender, ListBox))
  End Sub


  Private Sub OnEnterPressedInList(ByVal sender As Object, ByVal e As EventArgs)
    ' We have to accept anything that is selected in the list box.
    Me.ProcessSelectedListItem(DirectCast(sender, ListBox))
  End Sub


  '<summary>
  ' Handles the user selection in the drop-down list taking into account
  ' our special "None" item.
  '</summary>
  Private Sub ProcessSelectedListItem(ByVal list As ListBox)
    If list.SelectedIndex = Me._NoneItemIndex Then
      Me._Value = Nothing
    ElseIf list.SelectedIndex >= 0 Then
      Me._Value = DirectCast(list.SelectedItem, String)
    End If

    Me._EditorService.CloseDropDown()
  End Sub


  '<summary>
  ' Retrieves the names of all the RTF resources in the assembly.
  '</summary>
  Private Function GetRtfResourceNames(ByVal assm As System.Reflection.Assembly) As ArrayList
    Dim ResourceNames As New ArrayList
    For Each Name As String In assm.GetManifestResourceNames()
      If Me.IsResourceRtfResource(Name) Then
        ResourceNames.Add(Name)
      End If
    Next
    Return ResourceNames
  End Function


  '<summary>
  ' Checks to see if the resourceName if a RTF file resource.
  '</summary>
  Private Function IsResourceRtfResource(ByVal resourceName As String) As Boolean
    Debug.Assert(Not resourceName Is Nothing)

    ' Get the index of the last dot in the name.
    Dim LastDotIndex As Integer = resourceName.LastIndexOf("."c)
    If LastDotIndex < 0 Then
      Return False
    End If

    Dim Ext As String = resourceName.Substring(LastDotIndex).ToLower()
    Return (String.Compare(Ext, ".rtf") = 0)
  End Function


  '<summary>
  ' This ListBox descendant intercepts the ENTER key and generates the 
  ' distinguished EnterPressed event.
  '</summary>
  Private Class EnterKeyListBox
    Inherits ListBox

    Public Event EnterPressed As EventHandler

    Protected Overrides Function ProcessDialogKey(ByVal keyData As System.Windows.Forms.Keys) As Boolean
      Try
        ' Note: keyData is a bitmask, so the following IF statement is evaluated to true
        ' only if just the ENTER key is pressed, without any modifiers (Shift, Alt, Ctrl...).
        If keyData = System.Windows.Forms.Keys.Return Then
          RaiseEvent EnterPressed(Me, EventArgs.Empty)
          Return True ' True means we've processed the key
        Else
          Return MyBase.ProcessDialogKey(keyData)
        End If
      Catch ex As Exception
        Trace.WriteLine(ex.ToString())
      End Try
    End Function

  End Class

End Class

