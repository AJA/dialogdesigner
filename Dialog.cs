﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using System.ComponentModel;

namespace DialogDesigner
{
    public class Dialog
    {
        private const string VERSION_ = "1.1";

        private List<OptionData> options_;

        public class VarList : SortedList<int, Variable> { }
        private VarList variables_;
        private string filename_;

        /// <summary>
        /// Rakentaja uudelle dialogille
        /// </summary>
        public Dialog()
        {
            filename_ = "";
            options_ = new List<OptionData>();
            variables_ = new VarList();
            
            OptionData start = new OptionData(this, "Start");
            start.Index = 0;
            InsertOption(0, start);
            Modified = false;
        }

        /// <summary>
        /// Rakentaja lukee dialogin tiedostosta
        /// </summary>
        /// <param name="filename"></param>
        public Dialog(string filename)
        {
            filename_ = filename;            
            options_ = new List<OptionData>();
            variables_ = new VarList();

            FileStream fs = new FileStream(filename, FileMode.Open,
                FileAccess.Read, FileShare.Read);
            
            XmlDocument xml = new XmlDocument();
            
            xml.Load(fs);

            if (xml.LastChild.Attributes.GetNamedItem("version") == null)
            {
                // Virheellinen tiedosto
                fs.Close();
                throw new Exception("Invalid dialog file!");
            }

            string version = xml.LastChild.Attributes.GetNamedItem("version").Value;

            if (version.CompareTo(VERSION_) > 0)
            {
                // Uudempi tiedostoversio
                fs.Close();
                throw new Exception("This dialog file was created using a newer version of Dialog Designer and cannot be opened!");
            }

            try
            {
                XmlNode options = xml.DocumentElement.FirstChild;
                XmlNode variables = xml.DocumentElement.LastChild;
                
                ReadFromXml(options, null, version);
                ReadVariables(variables, version);
            }
            catch (XmlException e)
            {
                ErrorBox.Show(e.Message);
            }

            fs.Close();
            Modified = false;

            // Indeksit vielä kohdilleen
            for (int i = 0; i < options_.Count; i++)
            {
                options_[i].Index = i;
            }
        }

        /// <summary>
        /// Lisätään dialogivalinta indeksiin
        /// </summary>
        /// <param name="index"></param>
        /// <param name="option"></param>
        public void InsertOption(int index, OptionData option)
        {
            BeginFixingReferences();

            // Lisätään
            options_.Insert(index, option);
            if (option.Index != 0)
            {
                // Jos ei ole uusi luomus
                FixReference(option.Index, index);
            }
            option.Index = index;

            // Lisätään lapset
            List<OptionData> options = new List<OptionData>();
            options.AddRange(option.Children);
            index++;

            for (int i = 0; i < options.Count; i++)
            {
                options_.Insert(index, options[i]);
                FixReference(options[i].Index, index);
                options[i].Index = index;
                options.InsertRange(i + 1, options[i].Children);
                index++;
            }

            // Uudelleennumeroidaan seuraavat
            for (int i = index; i < options_.Count; i++)
            {
                FixReference(options_[i].Index, i);
                options_[i].Index = i;
            }

            EndFixingReferences();
            Modified = true;
        }

        /// <summary>
        /// Poistetaan dialogivalinta indeksistä
        /// </summary>
        /// <param name="index"></param>
        public void RemoveOption(int index)
        {
            BeginFixingReferences();
            // Poistetaan
            options_.RemoveAt(index);
            FixReference(index, 0);

            // Uudelleennumeroidaan seuraavat
            for (int i = index; i < options_.Count; i++)
            {
                FixReference(options_[i].Index, i);
                options_[i].Index = i;
            }
            EndFixingReferences();
            Modified = true;
        }

        /// <summary>
        /// Siirretään dialogivalinta uuteen indeksiin
        /// </summary>
        /// <param name="option"></param>
        /// <param name="index"></param>
        public void MoveOption(OptionData option, int index, bool moveChildren)
        {
            BeginFixingReferences();

            int lkm = 1;
            if (moveChildren)
            {
                lkm += option.ChildCount;
            }
            else
            {
                // Parent-muunnokset lapsille
                while (option.Children.Count > 0)
                {
                    option.Children[0].Parent = option.Parent;
                }
            }

            List<OptionData> ops = options_.GetRange(option.Index, lkm);
            options_.RemoveRange(option.Index, lkm);
            options_.InsertRange(index, ops);

            // Uudelleennumeroidaan kaikki
            for (int i = 0; i < options_.Count; i++)
            {
                FixReference(options_[i].Index, i);
                options_[i].Index = i;
            }

            EndFixingReferences();
            Modified = true;
        }

        /// <summary>
        /// Piilottaa kaikki valinnat
        /// </summary>
        public void HideOptions()
        {
            foreach (OptionData option in options_)
            {
                option.Visible = OptionData.OptionState.Off;
            }
        }

        /// <summary>
        /// Lukee dialogin xml-dokumentista rekursiivisesti
        /// </summary>
        /// <param name="xml"></param>
        private void ReadFromXml(XmlNode node, OptionData parent, string version)
        {
            if (!node.Name.Equals("options", StringComparison.OrdinalIgnoreCase))
            {
                throw new XmlException("Wrong tag found: " + node.Name + "! Expected: options.");
            }

            // Käsitellään optionit
            foreach(XmlNode option in node.ChildNodes)
            {
                if (!option.Name.Equals("option", StringComparison.OrdinalIgnoreCase))
                {
                    throw new XmlException("Wrong tag found: " + node.Name + "! Expected: option.");
                }

                // Luetaan attribuutit
                string text = option.Attributes.GetNamedItem("text").Value;
                bool show = option.Attributes.GetNamedItem("show").Value.Equals("True");
                bool say = false;

                if (version.CompareTo("1.1") >= 0)
                {
                    // Lisätty say-attribuutti tiedostoversiossa 1.1
                    say = option.Attributes.GetNamedItem("say").Value.Equals("True");
                }

                XmlNode cond = option.Attributes.GetNamedItem("condition");
                string condition;
                if (cond == null)
                {
                    condition = null;
                }
                else
                {                    
                    condition = cond.Value;
                }
                
                string script = null;
                XmlNode options = null;

                // Luetaan skripti ja options
                foreach (XmlNode child in option.ChildNodes)
                {
                    if (child.Name.Equals("script", StringComparison.OrdinalIgnoreCase))
                    {
                        XmlNode scriptData = child.FirstChild;
                        script = scriptData.Value;
                    }
                    else if (child.Name.Equals("options", StringComparison.OrdinalIgnoreCase))
                    {
                        options = child;
                    }
                    else
                    {
                        throw new XmlException("Wrong tag found: " + child.Name + "! Expected: script or options.");
                    }
                }

                if (script == null)
                {
                    throw new XmlException("Script tag not found!\n\n" + option.InnerXml);
                }

                // Luodaan optiondata
                OptionData opt = new OptionData(this, text);
                opt.Show = show;
                opt.Say = say;
                opt.Condition = condition;
                opt.Script = script.Split('\n');
                opt.Parent = parent;
                options_.Add(opt);

                if (options != null)
                {
                    ReadFromXml(options, opt, version);
                }
            }
        }

        /// <summary>
        /// Lukee muuttujat xml-tiedostosta
        /// </summary>
        /// <param name="node"></param>
        private void ReadVariables(XmlNode node, string version)
        {
            if (node == null)
            {
                // Ei muuttujia! Vanha tiedostoversio?
                return;
            }

            if (!node.Name.Equals("variables", StringComparison.OrdinalIgnoreCase))
            {
                throw new XmlException("Wrong tag found: " + node.Name + "! Expected: variables.");
            }

            foreach (XmlNode var in node.ChildNodes)
            {
                // Luetaan attribuutit
                string name = var.Attributes.GetNamedItem("name").Value;
                Variable.VarType type = Variable.VarType.Integer;

                if (version.CompareTo("1.1") >= 0)
                {
                    // Muuttujatyypit tiedostoversiossa 1.1
                    string typeStr = var.Attributes.GetNamedItem("type").Value;
                    type = (Variable.VarType)Enum.Parse(typeof(Variable.VarType), typeStr);
                }

                int defValue = Convert.ToInt32(var.Attributes.GetNamedItem("default").Value);
                string description = var.Attributes.GetNamedItem("description").Value;
                variables_.Add(variables_.Count,
                    new Variable(name, type, defValue, defValue, description)); 
            }
        }

        /// <summary>
        /// Luo rekursiivisesti option-elementin sisältöineen
        /// </summary>
        /// <param name="xml"></param>
        /// <param name="option"></param>
        /// <returns></returns>
        private XmlElement createOptionElement(XmlDocument xml, OptionData option)
        {
            XmlElement element = xml.CreateElement("option");

            XmlAttribute text = xml.CreateAttribute("text");
            text.Value = option.Text;
            element.SetAttributeNode(text);

            XmlAttribute show = xml.CreateAttribute("show");
            show.Value = option.Show.ToString();
            element.SetAttributeNode(show);

            XmlAttribute say = xml.CreateAttribute("say");
            say.Value = option.Say.ToString();
            element.SetAttributeNode(say);

            if (option.Condition != null)
            {
                XmlAttribute condition = xml.CreateAttribute("condition");
                condition.Value = option.Condition;
                element.SetAttributeNode(condition);
            }            

            XmlElement script = xml.CreateElement("script");
            XmlCDataSection scriptData =
                xml.CreateCDataSection(string.Join("\n", option.Script));
            script.AppendChild(scriptData);
            element.AppendChild(script);

            if (option.Children.Count > 0)
            {
                XmlElement options = xml.CreateElement("options");                

                foreach (OptionData child in option.Children)
                {
                    options.AppendChild(createOptionElement(xml, child));
                }

                element.AppendChild(options);
            }

            return element;
        }

        /// <summary>
        /// Tallentaa dialogin tiedostoon
        /// </summary>
        /// <param name="filename"></param>
        public void SaveToFile(string filename)
        {
            if (options_.Count < 1)
            {
                throw new Exception("SaveToFile: Zero elements in dialog!");
            }

            filename_ = filename;

            // Luodaan sisältö
            XmlDocument xml = new XmlDocument();
            XmlDeclaration dec = xml.CreateXmlDeclaration("1.0", "UTF-8", null);
            XmlElement options = xml.CreateElement("options");
            options.AppendChild(createOptionElement(xml, options_[0]));

            XmlElement variables = xml.CreateElement("variables");
            foreach (Variable v in variables_.Values)
            {
                XmlElement var = xml.CreateElement("variable");
                variables.AppendChild(var);

                XmlAttribute name = xml.CreateAttribute("name");
                name.Value = v.Name;
                var.SetAttributeNode(name);

                XmlAttribute type = xml.CreateAttribute("type");
                type.Value = v.Type.ToString();
                var.SetAttributeNode(type);

                XmlAttribute def = xml.CreateAttribute("default");
                def.Value = v.DefaultValue.ToString();
                var.SetAttributeNode(def);

                XmlAttribute desc = xml.CreateAttribute("description");
                desc.Value = v.Description;
                var.SetAttributeNode(desc);                
            }

            XmlElement root = xml.CreateElement("dialog");
            XmlAttribute version = xml.CreateAttribute("version");
            version.Value = VERSION_;
            root.SetAttributeNode(version);
            root.AppendChild(options);
            root.AppendChild(variables);

            xml.AppendChild(dec);
            xml.AppendChild(root);

            FileStream fs = new FileStream(filename, FileMode.Create,
                                FileAccess.Write, FileShare.None);

            xml.Save(fs);
            fs.Close();

            Modified = false;
        }

        /// <summary>
        /// Päivittää puunäkymän vastaamaan dialogia
        /// </summary>
        /// <param name="tree"></param>
        public void SynchronizeWithTreeview(TreeView tree)
        {
            tree.Nodes.Clear();
            tree.BeginUpdate();            

            generateTree(tree.Nodes, options_[0]);

            tree.EndUpdate();
            tree.ExpandAll();
            tree.SelectedNode = options_[0].Node;
        }

        /// <summary>
        /// Generoidaan puu rekursiivisesti
        /// </summary>
        /// <param name="nodes"></param>
        /// <param name="option"></param>
        private void generateTree(TreeNodeCollection nodes, OptionData option)
        {
            // Lisätään valinta itse
            option.Node = nodes.Add( option.Text );
            option.Node.Tag = option;

            // Lisätään valinnan lapset
            for (int i = 0; i < option.Children.Count; i++)
            {
                OptionData child = option.Children[i];
                generateTree(option.Node.Nodes, child);
            }
        }

        // Modified-property
        public bool Modified { get; set; }

        // Filename-property
        public string Filename
        {
            get { return filename_; }
        }
    
        // Start-property
        public OptionData Start
        {
            get { return options_[0]; }
        }

        // Options-property
        public List<OptionData> Options
        {
            get { return options_; }
        }

        // Variables-property
        public VarList Variables
        {
            get { return variables_; }
        }

        /// <summary>
        /// Aloitetaan viitteiden korjaus
        /// </summary>
        public void BeginFixingReferences()
        {
            foreach (OptionData option in options_)
            {
                option.BeginFixingReferences();
            }
        }

        /// <summary>
        /// Lopetetaan viitteiden korjaus
        /// </summary>
        public void EndFixingReferences()
        {
            foreach (OptionData option in options_)
            {
                option.EndFixingReferences();
            }
        }

        /// <summary>
        /// Korjataan yksittäinen viite
        /// </summary>
        /// <param name="oldIndex"></param>
        /// <param name="newIndex"></param>
        public void FixReference(int oldIndex, int newIndex)
        {
            foreach (OptionData option in options_)
            {                
                option.FixReference(oldIndex, newIndex);
            }
        }

        public void ExportToAGS(StreamWriter ags, bool showComment)
        {
            if ( showComment )
            {
                // Otsakekommentti lisättävistä dialogivalinnoista ja asetuksista
                ags.WriteLine("// Create the following dialog options manually in AGS:");
                ags.WriteLine("//");
                ags.WriteLine("// #  Show Say Option text");
                ags.WriteLine("// --|----|---|-----------");
                foreach (OptionData option in options_)
                {
                    if (option.Index == 0)
                        continue;

                    string show = " ";
                    if (option.Parent.Index == 0 && option.Show)
                        show = "x";
                    string say = " ";
                    if (option.Say)
                        say = "x";
                    string i = option.Index.ToString();
                    if (i.Length < 2)
                        i = " " + i;

                    ags.WriteLine(string.Format("// {0} [{1}]  [{2}] {3}", i, show, say, option.Text));
                }
                ags.WriteLine("//");
                ags.WriteLine("// Dialog script begins ----------");
                ags.WriteLine();
                ags.WriteLine();
            }

            // Muunnetaan skriptit ja kirjoitetaan tiedostoon
            foreach (OptionData option in options_)
            {
                option.ExportToAGS(ags);
                ags.WriteLine();
                ags.WriteLine();
                ags.WriteLine();
            }
        }
    }
}
