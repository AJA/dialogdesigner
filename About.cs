﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using System.IO;
using System.Diagnostics;

namespace DialogDesigner
{
    public partial class About : Form
    {
        public About()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start( "mailto:akiahonen@gmail.com" );
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://www.codeproject.com/KB/recipes/TinyPG.aspx");
        }

        private void About_Load(object sender, EventArgs e)
        {
            label1.Text = Application.ProductName + " - Version "
                + Assembly.GetExecutingAssembly().GetName().Version.Major + "."
                + Assembly.GetExecutingAssembly().GetName().Version.Minor + "."
                + Assembly.GetExecutingAssembly().GetName().Version.Build;

            labelCopyright.Text = FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).LegalCopyright;

            toolTip.SetToolTip(linkLabel1, linkLabel1.Tag.ToString());
            toolTip.SetToolTip(linkLabel2, linkLabel2.Tag.ToString());
            button1.Select();
        }
    }
}
