﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;
using Utils;

namespace DialogDesigner
{
    public partial class Main : Form
    {
        public const bool PUBLIC_VERSION = true;

        private const string TITLE = "Dialog Designer";
        private const string NEW_OPTION = "New dialog option";

        private const int SCRIPT_STOP = -1;
        private const int SCRIPT_RETURN = 0;
        private const int SCRIPT_NEXTLINE = 1;
        private const int SCRIPT_ERROR = -2;

        private const int TAB_SIZE = 2;
        private const int UNDO_LEVELS = 21;

        private readonly Color COLOR_ONS = Color.LightGreen;
        private readonly Color COLOR_OFFS = Color.FromArgb(255, 190, 190);
        private readonly Color COLOR_NEUTRALS = Color.White;

        private const int MRU_MAX_ITEMS = 5;
        private const int MRU_MENU_MAX_LENGTH = 50;

        private bool previewing_;
        private OptionData running_;
        private ScriptStatus scriptStatus_;        
        private bool reset_;
        private bool modifiedAfterPreview_;

        private bool lockModify_;
        private Dialog dlg_;
        private List<TreeNode> nodeList_;

        private TreeNode dragOver_;
        private SyntaxHighlighter hilite_;
        private VarForm varForm_;

        private string prevCondText_;
        private List<int> ons_, offs_;

        private UndoBuffer undoBuffer_;
        
        private delegate void DelegateOpenFile(string path);
        private DelegateOpenFile dragDropOpenFileDelegate_;

        public Main( string filename )
        {
            previewing_ = false;
            InitializeComponent();

            splitContainer1.Panel1MinSize = 100;
            splitContainer1.Panel2MinSize = 300;

            Text = TITLE;
            hilite_ = new SyntaxHighlighter(script);
            varForm_ = null;

            dlg_ = null;
            groupScript.Enabled = false;
            groupOptions.Enabled = false;
            panelPreview.Visible = false;

            undoBuffer_ = new UndoBuffer(script, UNDO_LEVELS);

            lockModify_ = false;

            // Ylläpidettävä indeksilista
            nodeList_ = new List<TreeNode>();
            dragOver_ = null;

            running_ = null;
            reset_ = true;
            modifiedAfterPreview_ = false;

            prevCondText_ = null;

            ScriptCommand.Init( PUBLIC_VERSION );
            script.ScrollBars = RichTextBoxScrollBars.ForcedBoth;

            // Luodaan koodikorostusten tyylit            
            // Otsikko
            //hilite_.AddStyle(new SyntaxStyle("(@(S|[1-9][0-9]*).*$)", 1,
            //    FontStyle.Underline, Color.Black));
            // Toteutetut avainsanat
            hilite_.AddStyle(new SyntaxStyle(
                ScriptCommand.GetHighlightRegex(), 1, FontStyle.Regular, Color.Blue));
            // Toteuttamattomat avainsanat
            hilite_.AddStyle(new SyntaxStyle("^("
                + "add-inv [0-9]+"
                + "|give-score [0-9]+"
                + "|goto-dialog [0-9]+"
                + "|goto-previous"
                + "|lose-inv [0-9]+"
                + "|new-room [0-9]+"
                + "|play-sound [0-9]+"
                + "|run-script [0-9]+"
                + "|set-globalint [0-9]+ [0-9]+"
                + "|set-speech-view \\s+ [0-9]+"
                + ")", 1, FontStyle.Regular, Color.DimGray));
            // Dialogi
            hilite_.AddStyle(new SyntaxStyle("^\\w+:(.+$)", 1, FontStyle.Regular, Color.Maroon));
            // Puhuja
            hilite_.AddStyle(new SyntaxStyle("(^\\w+:)", 1, FontStyle.Regular, Color.Black));
            // Sisennetyt
            hilite_.AddStyle(new SyntaxStyle("(^\\s+.*$)", 1, FontStyle.Italic, Color.Black));
            // Kommentti
            hilite_.AddStyle(new SyntaxStyle("(//.*$)", 1, FontStyle.Regular, Color.DarkGreen));
            // Huomio
            hilite_.AddStyle(new SyntaxStyle("//[^\\*]*(\\*\\*\\*.*$)", 1,
                FontStyle.Regular, Color.Red));

            dragDropOpenFileDelegate_ = new DelegateOpenFile(dragDropOpenDialog);

            if (filename != null)
            {
                // Avataan komentoriviltä tiedosto
                openDialog(filename);
            }
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void dlgMenu_Opening(object sender, CancelEventArgs e)
        {
            if (options.SelectedNode == null || options.SelectedNode.Parent == null)
            {
                btnAddAfter.Enabled = false;
                btnAddBefore.Enabled = false;
                btnDelete.Enabled = false;
                btnDeleteTree.Enabled = false;
            }
            else
            {
                btnAddAfter.Enabled = true;
                btnAddBefore.Enabled = true;
                btnDelete.Enabled = true;
                btnDeleteTree.Enabled = true;
            }
        }

        /// <summary>
        /// Päivittää nodeListan vastaamaan options-puun nodeja
        /// </summary>
        private void updateNodeList()
        {
            nodeList_.Clear();

            List<TreeNode> list = new List<TreeNode>();
            // Juurinode listaan
            list.Add( options.Nodes[0] );            

            // Haetaan nodet järjestyksessä puusta
            for (int n = 0; n < list.Count; n++)
            {
                TreeNode node = list[n];
                nodeList_.Add(node);

                for (int i = 0; i < node.Nodes.Count; i++)
                {
                    list.Insert(n + i + 1, node.Nodes[i]);
                }
            }
        }

        private void printNodeList()
        {
            string text = "";
            foreach (TreeNode node in nodeList_)
            {
                text += node.Text + "\n";
            }
            MessageBox.Show(text);
        }

        private void btnAddAfter_Click(object sender, EventArgs e)
        {
            TreeNode node = new TreeNode();
            node.Text = NEW_OPTION;
            TreeNode parent = options.SelectedNode.Parent;
            int index = options.SelectedNode.Index;

            addNode(node, parent, index + 1, true);
        }

        private void btnAddBefore_Click(object sender, EventArgs e)
        {
            TreeNode node = new TreeNode();
            node.Text = NEW_OPTION;
            TreeNode parent = options.SelectedNode.Parent;
            int index = options.SelectedNode.Index;

            addNode(node, parent, index, true);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            TreeNode node = new TreeNode();
            node.Text = NEW_OPTION;
            TreeNode parent = options.SelectedNode;

            addNode(node, parent, parent.Nodes.Count, true);
        }

        private void addNode(TreeNode node, TreeNode parent, int index, bool createOption)
        {
            saveScript();
            options.BeginUpdate();
            parent.Nodes.Insert(index, node);
            updateNodeList();

            if (createOption)
            {
                // Luodaan valinta
                int i = nodeList_.IndexOf(node);
                OptionData opt = new OptionData(dlg_, node.Text);
                dlg_.InsertOption(i, opt);
                opt.Parent = (OptionData)parent.Tag;
                node.Tag = opt;
                opt.Node = node;
            }

            // Valitaan vielä uusi ja aloitetaan nimen muokkaus
            loadScript();
            options.SelectedNode = node;
            options.EndUpdate();            

            if (createOption)
            {
                node.BeginEdit();
            }

            if (!reset_)
                modifiedAfterPreview_ = true;
        }

        private void deleteNode(TreeNode node, bool deleteChildren, bool deleteOption)
        {
            options.BeginUpdate();
            OptionData option = (OptionData)node.Tag;
            
            if (!deleteChildren)
            {
                // Jätetään lapset paikoilleen             
                while (option.Children.Count > 0)
                {
                    OptionData child = option.Children[0];
                    child.Parent = option.Parent;
                }

                int index = node.Index;
                foreach (TreeNode child in node.Nodes)
                {
                    child.Remove();
                    node.Parent.Nodes.Insert(index, child);
                    index++;
                }                
                node.Nodes.Clear();
            }

            if (deleteOption)
            {
                option.Remove(deleteChildren, false);
            }

            // Poistetaan valittu                        
            node.Remove();

            options.ExpandAll();
            options.EndUpdate();

            updateNodeList();

            if (!reset_)
                modifiedAfterPreview_ = true;
        }

        private void moveNode(TreeNode node, TreeNode parent, int index, bool moveChildren)
        {
            OptionData option = (OptionData)node.Tag;

            // Poistetaan vanha node ja tarvittaessa jätetään lapset paikoilleen
            deleteNode(node, moveChildren, false);
            //option.Remove(true, true, false);
            // Lisätään node lapsineen uuteen paikkaan
            addNode(node, parent, index, false);

            parent.Expand();
            node.ExpandAll();

            int newIndex = nodeList_.IndexOf(node);
            dlg_.MoveOption(option, newIndex, moveChildren);
            //dlg_.InsertOption(newIndex, option);
            option.Parent = (OptionData)parent.Tag;
            options.SelectedNode = node;
            options_AfterSelect(null, null);

            if (!reset_)
                modifiedAfterPreview_ = true;

            options.Refresh();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (options.SelectedNode != null && options.SelectedNode.Parent != null)
            {                
                deleteNode(options.SelectedNode, false, true);
            }
        }

        private void btnDeleteTree_Click(object sender, EventArgs e)
        {
            if (options.SelectedNode != null && options.SelectedNode.Parent != null)
            {
                deleteNode(options.SelectedNode, true, true);
            }
        }

        private void options_ItemDrag(object sender, ItemDragEventArgs e)
        {
            if (((TreeNode)e.Item).Parent != null)
            {
                saveScript();
                DoDragDrop(e.Item, DragDropEffects.Move);
            }
        }

        private void options_DragDrop(object sender, DragEventArgs e)
        {
            dragOver_.BackColor = options.BackColor;
            dragOver_.ForeColor = options.ForeColor;
            dragOver_ = null;
	
            if(e.Data.GetDataPresent("System.Windows.Forms.TreeNode", false))
			{
				Point pt = ((TreeView)sender).PointToClient(new Point(e.X, e.Y));
				TreeNode DestinationNode = ((TreeView)sender).GetNodeAt(pt);
                TreeNode NewNode = (TreeNode)e.Data.GetData("System.Windows.Forms.TreeNode");

                if (DestinationNode.TreeView == NewNode.TreeView
                    && !DestinationNode.Equals(NewNode))
                {
                    // Siirto sallittu, näytetään vaihtoehdot                    
                    DestinationNode.TreeView.Tag =
                        new KeyValuePair<bool, TreeNode>((e.KeyState & 4) == 4, NewNode);
                    DestinationNode.TreeView.SelectedNode = DestinationNode;

                    // Estetään before/after siirrot Start-nodeen dropattaessa
                    btnMoveBefore.Enabled = (DestinationNode.Parent != null);
                    btnMoveAfter.Enabled = btnMoveBefore.Enabled;

                    dlgMove.Show(e.X, e.Y);
                }
                else
                {
                    setNodeOnOffColor(NewNode);
                }
			}
        }

        private void btnMoveBefore_Click(object sender, EventArgs e)
        {
            KeyValuePair<bool, TreeNode> data = (KeyValuePair<bool, TreeNode>)options.Tag;
            TreeNode node = data.Value;
            bool moveChildren = !data.Key;
            TreeNode Destination = options.SelectedNode;
            options.Tag = null;
            int index = Destination.Index;

            // Jos siirretään alemmaksi samalla tasolla, jätetään yksi ylemmäs
            if (Destination.Parent == node.Parent && node.Index < Destination.Index)
                index--;

            moveNode(node, Destination.Parent, index, moveChildren);
        }

        private void btnMoveAfter_Click(object sender, EventArgs e)
        {
            KeyValuePair<bool,TreeNode> data = (KeyValuePair<bool,TreeNode>)options.Tag;
            TreeNode node = data.Value;
            bool moveChildren = !data.Key;
            TreeNode Destination = options.SelectedNode;
            options.Tag = null;
            int index = Destination.Index + 1;

            // Jos siirretään alemmaksi samalla tasolla, jätetään yksi ylemmäs
            if (Destination.Parent == node.Parent && node.Index < Destination.Index)
                index--;                

            moveNode(node, Destination.Parent, index, moveChildren);
        }

        private void btnMoveChild_Click(object sender, EventArgs e)
        {
            KeyValuePair<bool, TreeNode> data = (KeyValuePair<bool, TreeNode>)options.Tag;
            TreeNode node = data.Value;
            bool moveChildren = !data.Key;
            TreeNode Destination = options.SelectedNode;
            options.Tag = null;

            moveNode(node, Destination, Destination.Nodes.Count, moveChildren);
        }

        private bool isParentsChild(TreeNode parent, TreeNode child)
        {
            if (parent.Nodes.Contains(child))
            {
                return true;
            }

            bool found = false;
            foreach (TreeNode node in parent.Nodes)
            {
                found = isParentsChild(node, child);
                if (found)
                    break;
            }
            return found;
        }

        private void options_DragOver(object sender, DragEventArgs e)
        {           
            Point pt = ((TreeView)sender).PointToClient(new Point(e.X, e.Y));
            TreeNode DestinationNode = ((TreeView)sender).GetNodeAt(pt);

            e.Effect = DragDropEffects.None;
            if (e.Data.GetDataPresent("System.Windows.Forms.TreeNode", false))
            {                
                TreeNode node = (TreeNode)e.Data.GetData("System.Windows.Forms.TreeNode");
                if ((e.KeyState & 4) != 4 && isParentsChild(node, DestinationNode))
                {
                    // Yritetään siirtää lapsensa lapseksi, ei käy! Paitsi shiftin kera.
                    return;
                }
                else
                {
                    if (dragOver_ != null && !dragOver_.Equals(DestinationNode))
                    {
                        setNodeOnOffColor(dragOver_);
                    }
                    DestinationNode.BackColor = Color.FromArgb(49, 106, 197);
                    DestinationNode.ForeColor = Color.White;
                    dragOver_ = DestinationNode;

                    e.Effect = DragDropEffects.Move;
                }
            }            
        }

        private void options_MouseClick(object sender, MouseEventArgs e)
        {
            options.SelectedNode = options.GetNodeAt(e.X, e.Y);
        }

        private void options_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F2 && options.SelectedNode != null)
            {
                options.SelectedNode.BeginEdit();
            }
        }

        private void setNodeOnOffColor(TreeNode node)
        {
            node.ForeColor = options.ForeColor;
            node.BackColor = options.BackColor;
            int i = ((OptionData)node.Tag).Index;

            if (ons_ == null || offs_ == null)
                return;

            if (ons_.Contains(i))
                node.BackColor = COLOR_ONS;
            else if (offs_.Contains(i))
            {
                node.BackColor = COLOR_OFFS;
                //node.ForeColor = Color.White;
            }
            else if (node.Equals(options.SelectedNode))
                node.BackColor = COLOR_NEUTRALS;
        }

        private void options_AfterSelect(object sender, TreeViewEventArgs e)
        {
            loadScript();
            updateNodeList();
            
            ons_ = new List<int>();
            offs_ = new List<int>();
            ScriptEffects se = ((OptionData)options.SelectedNode.Tag).GetScriptEffects(true, true, null);
            ons_.AddRange(se.Ons);
            ons_.AddRange(se.ConditionalOns);
            offs_.AddRange(se.Offs);
            offs_.AddRange(se.OffForevers);

            for (int i = 0; i < nodeList_.Count; i++)
            {
                setNodeOnOffColor(nodeList_[i]);                
            }            
        }

        private void options_AfterLabelEdit(object sender, NodeLabelEditEventArgs e)
        {
            if (e.Label != null)
            {
                // Talletetaan skripti takaisin
                saveScript();

                // Päivitetään nimi                       
                ((OptionData)e.Node.Tag).Text = e.Label;
                lblScript.Text = "[" + ((OptionData)e.Node.Tag).Index + "] " + e.Label;

                // Skriptin sisältö
                //loadScript();
                dlg_.Modified = true;

                if (!reset_)
                    modifiedAfterPreview_ = true;
            }
        }

        private void options_BeforeSelect(object sender, TreeViewCancelEventArgs e)
        {
            if (options.SelectedNode != null)
            {
                saveScript();
            }
        }

        private void openDialog(string filename)
        {
            try
            {
                dlg_ = new Dialog(filename);
            }
            catch (Exception err)
            {
                ErrorBox.Show(err.Message);
                return;
            }
            lockModify_ = true;
            dlg_.SynchronizeWithTreeview(options);
            groupOptions.Enabled = true;
            groupScript.Enabled = true;
            panelPreview.Visible = true;
            options.Select();
            lockModify_ = false;
            reset_ = true;
            modifiedAfterPreview_ = false;

            if (varForm_ != null && varForm_.Visible)
            {
                varForm_.Close();
                variablesToolStripMenuItem_Click(null, null);
            }

            addToMRU(filename);
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            if (dlg_ != null && dlg_.Modified)
            {
                // Kysytään vahvistus, halutaanko tallentaa vanha
                DialogResult result = MessageBox.Show(
                    "The dialog has changed.\n\nDo you want to save the changes?",
                    Application.ProductName,
                    MessageBoxButtons.YesNoCancel,
                    MessageBoxIcon.Exclamation
                );

                if (result == DialogResult.Yes)
                {
                    btnSave_Click(sender, e);
                }
                else if (result == DialogResult.Cancel)
                {
                    return;
                }
            }

            if (dlgOpen.ShowDialog() == DialogResult.OK)
            {
                openDialog(dlgOpen.FileName);
            }
        }

        private void btnSaveAs_Click(object sender, EventArgs e)
        {
            if (dlgSave.ShowDialog() == DialogResult.OK)
            {
                saveScript();
                dlg_.SaveToFile(dlgSave.FileName);
                addToMRU(dlgSave.FileName);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (dlg_.Filename == "")
            {
                // Ei ole tallennettu vielä
                btnSaveAs_Click(sender, e);
            }
            else
            {
                saveScript();
                dlg_.SaveToFile(dlg_.Filename);
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            if (dlg_ != null && dlg_.Modified)
            {
                // Kysytään vahvistus, halutaanko tallentaa vanha
                DialogResult result = MessageBox.Show(
                    "The dialog has changed.\n\nDo you want to save the changes?",
                    Application.ProductName,
                    MessageBoxButtons.YesNoCancel,
                    MessageBoxIcon.Exclamation
                );

                if (result == DialogResult.Yes)
                {
                    btnSave_Click(sender, e);
                }
                else if (result == DialogResult.Cancel)
                {
                    return;
                }
            }

            // Korvataan vanha dialogi uudella
            dlg_ = new Dialog();
            dlg_.SynchronizeWithTreeview(options);
            groupOptions.Enabled = true;
            groupScript.Enabled = true;
            panelPreview.Visible = true;
            options.Select();
            reset_ = true;
            modifiedAfterPreview_ = false;

            if (varForm_ != null && varForm_.Visible)
            {
                varForm_.Close();
                variablesToolStripMenuItem_Click(null, null);
            }
        }

        private void addToMRU(string filename)
        {
            if (Properties.Settings.Default.MRU == null)
                Properties.Settings.Default.MRU = new StringCollection();
            StringCollection MRU = Properties.Settings.Default.MRU;

            int i = MRU.IndexOf(filename);
            if (i >= 0)
            {
                // Poistetaan vanha
                MRU.RemoveAt(i);
            }

            if (MRU.Count >= MRU_MAX_ITEMS)
            {
                // Poistetaan vanhin
                MRU.RemoveAt(MRU.Count - 1);
            }

            MRU.Insert(0, filename);
            updateMRUMenu();
        }

        public void onOpenRecent(Object sender, EventArgs e)
        {
            ToolStripMenuItem item = (ToolStripMenuItem)sender;
            openDialog((string)item.Tag);
        }

        private void updateMRUMenu()
        {
            if (Properties.Settings.Default.MRU != null)
            {
                btnRecent.DropDownItems.Clear();
                for ( int i = 0; i < Properties.Settings.Default.MRU.Count; i++ )
                {
                    string file = Properties.Settings.Default.MRU[i];
                    ToolStripMenuItem item = new ToolStripMenuItem();
                    item.Click += new EventHandler(onOpenRecent);
                    item.Tag = file;
                    item.Text = (i + 1) + " " + StringControl.TruncatePath(file, MRU_MENU_MAX_LENGTH);                    
                    btnRecent.DropDownItems.Add( item );
                }
            }
        }

        private void btnFile_DropDownOpening(object sender, EventArgs e)
        {
            btnSave.Enabled = dlg_ != null && dlg_.Modified;
            btnSaveAs.Enabled = dlg_ != null;
            btnExportText.Enabled = dlg_ != null;
            btnExportProject.Enabled = dlg_ != null;

            // **** //
            if (Properties.Settings.Default.MRU == null ||
                    Properties.Settings.Default.MRU.Count == 0)
                btnRecent.Enabled = false;
            else
                btnRecent.Enabled = true;
        }

        private void script_TextChanged(object sender, EventArgs e)
        {
            // Tapahtuma estetty
            if (script.Tag != null && (bool)script.Tag)
                return;

            if (!lockModify_)
            {
                undoBuffer_.SetModified();

                dlg_.Modified = true;
                if (!reset_)
                    modifiedAfterPreview_ = true;

                // Korostukset #########

                //int scroll = RichTextBoxControl.GetScroll(script);

                // Rivin alku
                int end = 0;
                int start = script.GetFirstCharIndexOfCurrentLine();

                end = script.Text.IndexOf('\n', script.SelectionStart);
                //MessageBox.Show("START: " + start + "\nEND: " + end);
                if (end < 0)
                    end = script.Text.Length;

                int selStart = script.SelectionStart;
                int selLength = script.SelectionLength;

                hilite_.HighlightSection(start, end - start);

                if (start != 0)
                {
                    // Ei olla ekalla rivillä, korostetaan edellinen rivi uudestaan
                    int rivi = script.GetLineFromCharIndex(start - 1);
                    int prev = script.GetFirstCharIndexFromLine(rivi);
                    hilite_.HighlightSection(prev, script.Lines[rivi].Length);
                }
                // Osoitin takaisin paikoilleen
                //script.Select(selStart, selLength);

                //RichTextBoxControl.SetScroll(script, scroll);
            }            
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            string newText = "";
            if (dlg_ == null)
            {
                newText = TITLE;
            }
            else if (dlg_.Filename == "")
            {
                newText = TITLE + " - untitled";
            }
            else
            {
                newText = TITLE + " - " + System.IO.Path.GetFileName(dlg_.Filename);
            }

            if (dlg_ != null && dlg_.Modified)
            {
                newText += "*";
            }

            Text = newText;

            if (modifiedAfterPreview_ && !reset_)
            {
                modifiedAfterPreview_ = false;
                reset_ = true;
                //btnReset_Click(sender, e);
                doReset();
            }

            if (reset_ || previewing_)
                btnReset.Enabled = false;
            else
                btnReset.Enabled = true;
        }

        private void Main_KeyDown(object sender, KeyEventArgs e)
        {
            // GLOBAALIT PIKANÄPPÄIMET
            if (e.Control && e.KeyCode == Keys.S)
            {
                btnSave_Click(sender, e);
            }
            else if (e.Control && e.KeyCode == Keys.O)
            {
                btnOpen_Click(sender, e);
            }
            else if (e.Control && e.KeyCode == Keys.N)
            {
                btnNew_Click(sender, e);
            }
            else if (e.KeyCode == Keys.F1)
            {
                helpTopicsToolStripMenuItem_Click(sender, e);
            }

            // Preview skippaus
            if (previewing_ && e.KeyCode == Keys.OemPeriod)
            {
                timerPreview_Tick(sender, null);
            }
        }

        private void Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (dlg_ != null && dlg_.Modified)
            {
                // Kysytään vahvistus, halutaanko tallentaa vanha
                DialogResult result = MessageBox.Show(
                    "The dialog has changed.\n\nDo you want to save the changes?",
                    Application.ProductName,
                    MessageBoxButtons.YesNoCancel,
                    MessageBoxIcon.Exclamation
                );

                if (result == DialogResult.Yes)
                {
                    btnSave_Click(sender, e);
                }
                else if (result == DialogResult.Cancel)
                {
                    e.Cancel = true;
                    return;
                }
            }

            // Tallennetaan ikkunan sijaintitieto
            Properties.Settings.Default.MainState = this.WindowState;
            if (this.WindowState == FormWindowState.Normal)
            {
                Properties.Settings.Default.MainSize = this.Size;
                Properties.Settings.Default.MainLoc = this.Location;
            }
            else
            {
                Properties.Settings.Default.MainSize = this.RestoreBounds.Size;
                Properties.Settings.Default.MainLoc = this.RestoreBounds.Location;
            }
            Properties.Settings.Default.MainSplitter = this.splitContainer1.SplitterDistance;

            if (varForm_ != null && varForm_.Visible)
                Properties.Settings.Default.VarVisible = true;
            else
                Properties.Settings.Default.VarVisible = false;

            Properties.Settings.Default.Save();
        }

        private void checkShow_CheckedChanged(object sender, EventArgs e)
        {
            if (checkShow.Checked)
            {
                checkCond.Enabled = false;
            }
            else
            {
                checkCond.Enabled = true;
            }

            checkCond_CheckedChanged(sender, e);

            if (!lockModify_)
            {
                dlg_.Modified = true;
                if (!reset_)
                    modifiedAfterPreview_ = true;
            }
        }

        private void loadScript()
        {
            lockModify_ = true;

            if (options.SelectedNode == null)
            {
                lblScript.Text = "";
                checkShow.Checked = false;
                checkCond.Checked = false;
                checkSay.Checked = false;
                script.Clear();
                txtCond.Text = "";
                groupScript.Enabled = false;
                return;
            }

            OptionData option = (OptionData)options.SelectedNode.Tag;
            string index = option.Index.ToString();
            if (option.Index == 0)
                index = "S";
            lblScript.Text = "[" + index + "] " + options.SelectedNode.Text;

            checkShow.Checked = option.Show;
            checkShow.Enabled = true;
            checkCond.Enabled = !option.Show;
            checkSay.Checked = option.Say;
            checkSay.Enabled = true;
            //lblCondValue.Text = "";

            checkCond.Checked = option.Condition != null;
            if (checkCond.Checked)
                txtCond.Text = option.Condition;
            else
                txtCond.Text = "";
            txtCond.Enabled = checkCond.Checked && checkCond.Enabled;
            prevCondText_ = txtCond.Text;

            if (options.SelectedNode.Parent == null)
            {
                // Startin show-asetusta ei voi määrittää
                checkShow.Enabled = false;
                checkShow.Checked = false;

                checkSay.Enabled = false;
                checkSay.Checked = false;

                checkCond.Enabled = false;
                txtCond.Enabled = false;
            }

            // Skriptin sisältö
            try
            {
                IntPtr mask = RichTextBoxControl.LockRichTextBox(script);
                script.Select(0, 0);
                script.Lines = option.Script;
                RichTextBoxControl.UnlockRichTextBox(script, mask);
                hilite_.HighlightAll();
                lockModify_ = false;
                script.Select(0, 0);

                undoBuffer_.Clear();
            }
            catch (Exception e)
            {
                ErrorBox.Show("LoadScript: " + e.Message);
                Application.ExitThread();
            }
        }

        private void saveScript()
        {
            lockModify_ = true;
            OptionData option = (OptionData)options.SelectedNode.Tag;

            // Talletetaan skripti takaisin
            option.Script = script.Lines;
            option.Show = checkShow.Checked;
            option.Say = checkSay.Checked;

            if (checkCond.Checked)
            {
                option.Condition = txtCond.Text;
            }
            else
            {
                option.Condition = null;
            }

            lockModify_ = false;
        }

        private void variablesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (varForm_ == null || !varForm_.Visible)
            {
                varForm_ = new VarForm(dlg_);
                varForm_.Tag = this;
                varForm_.Show(this);
            }
        }

        private void btnPreview_Click(object sender, EventArgs e)
        {
            if (!previewing_)
            {
                saveScript();
                groupOptions.Enabled = false;
                groupScript.Enabled = false;
                btnReset.Enabled = false;

                btnPreview.Text = "Stop";
                timerPreview.Enabled = true;
                previewing_ = true;

                menuStrip1.Enabled = false;

                if (varForm_ != null && varForm_.Visible)
                    varForm_.Enabled = false;

                startPreview();                
            }
            else // Pysäytetään esikatselu
            {
                menuStrip1.Enabled = true;

                groupOptions.Enabled = true;
                groupScript.Enabled = true;
                btnReset.Enabled = true;

                lblPreview.Text = "";
                btnPreview.Text = "Preview";
                timerPreview.Enabled = false;
                previewing_ = false;                

                if (varForm_ != null && varForm_.Visible)
                {
                    varForm_.Enabled = true;
                    varForm_.LoadVariables(dlg_);
                }

                // Päivitetään ons/offs
                options_AfterSelect(sender, null);
                evaluateCondition();
                highlightCondition();
            }
        }

        private void startPreview()
        {
            if (reset_)
            {
                doReset();
                reset_ = false;
            }
            runScript(dlg_.Start);
        }

        private int showOptions()
        {
            lblPreview.Text = "";
            List<OptionData> visible = new List<OptionData>();
            
            // Käydään kaikki valinnat läpi
            foreach (OptionData option in dlg_.Options)
            {
                if (option.Visible == OptionData.OptionState.On)
                {
                    visible.Add(option);
                }
            }

            if (visible.Count == 1)
            {
                // Toistetaan ainoa automaattisesti
                return visible[0].Index;
            }
            else if (visible.Count == 0)
            {
                MessageBox.Show("No visible options to display.");
                btnPreview_Click(null, null);
                return -1;
            }

            Choise dialog = new Choise();
            int valinta = dialog.ShowDialog(visible);
            if (valinta < 0)
            {
                btnPreview_Click(null, null);
                return -1;
            }
            return visible[valinta].Index;
        }

        private void runScript(OptionData option)
        {
            running_ = option;
            scriptStatus_ = new ScriptStatus();

            if (option.Say)
            {
                lblSpeaker.Text = "player";
                lblPreview.Text = option.Text;
                timerPreview.Interval = speechDelay(option.Text);
            }
            else
                timerPreview.Interval = 1;

            timerPreview.Start();
        }

        private bool endScript(int result)
        {
            if (result != SCRIPT_ERROR)
            {
                // Otetaan skriptin vaikutukset käyttöön
                try
                {
                    List<string> warnings = new List<string>();
                    ScriptEffects e = running_.GetScriptEffects(false, false, warnings);
            
                    if (warnings.Count > 0)
                    {
                        WarningBox.Show(
                            "Warnings found in option " + running_.Index + " (" + running_.Text + "):\n\n" +
                            string.Join("\n", warnings.ToArray()));
                    }

                    /*
                    foreach (int i in e.Ons)
                        if (dlg_.Options[i].Visible != OptionData.OptionState.OffForever)
                            dlg_.Options[i].Visible = OptionData.OptionState.On;
                    foreach (int i in e.ConditionalOns)
                        if (dlg_.Options[i].Visible != OptionData.OptionState.OffForever)
                            dlg_.Options[i].Visible = OptionData.OptionState.On;
                    foreach (int i in e.Offs)
                        if (dlg_.Options[i].Visible != OptionData.OptionState.OffForever)
                            dlg_.Options[i].Visible = OptionData.OptionState.Off;
                    foreach (int i in e.OffForevers)
                        dlg_.Options[i].Visible = OptionData.OptionState.OffForever;
                     */
                }
                catch (Exception ex)
                {
                    ErrorBox.Show(ex.Message);
                    return false;
                }
            }
            else
                ErrorBox.Show("Preview failed.");

            return true;

            /*
            if (!scriptStatus_.DontHideOption &&
                running_.Visible != OptionData.OptionState.OffForever)
            {
                running_.Visible = OptionData.OptionState.Off;
            }

            if (!scriptStatus_.DontShowChildren && running_.Parent != null)
            {
                // Näytetään lapset jos kyseessä ei ole START
                foreach (OptionData option in running_.Children)
                {
                    if (option.Visible != OptionData.OptionState.OffForever)
                    {
                        if (option.Show)
                        {
                            option.Visible = OptionData.OptionState.On;
                        }
                    }
                }
            }
            if (scriptStatus_.ShowConditionals)
            {
                // Näytetään lapset joiden ehto pätee
                foreach (OptionData option in running_.Children)
                {

                    if (option.Visible != OptionData.OptionState.OffForever)
                    {
                        try
                        {
                            if (!option.Show && option.Condition != null &&
                                !option.ShownOnCondition &&
                                Conditional.Evaluate(option.Condition, dlg_.Variables))
                            {
                                option.Visible = OptionData.OptionState.On;
                                option.ShownOnCondition = true;
                            }
                        }
                        catch (Exception e)
                        {
                            ErrorBox.Show(e.Message);
                        }
                    }
                }
            }*/
        }

        private void timerPreview_Tick(object sender, EventArgs e)
        {
            // Skripti etenee askeleen
            timerPreview.Stop();

            int delay = SCRIPT_RETURN;
            string line;

            if (scriptStatus_.Line < running_.Script.Length)
            {
                // Skriptissä vielä rivejä
                line = running_.Script[scriptStatus_.Line];
                delay = runScriptLine(line);
            }
            // Jos skriptin loppu tulikin vastaan, oletetaan paluu valintoihin

            if (delay == SCRIPT_STOP || delay == SCRIPT_ERROR)
            {
                // Suoritus päättyi
                endScript(delay);
                btnPreview_Click(sender, e);
            }            
            else if (delay == SCRIPT_RETURN)
            {
                // Palataan valintoihin ja otetaan uusi aihe
                if (!endScript(delay))
                {
                    btnPreview_Click(sender, e);
                    return;
                }

                int opt = showOptions();
                if (opt >= 0)
                    runScript(dlg_.Options[opt]);
            }
            else
            {
                // Skripti kesken
                timerPreview.Interval = delay;
                scriptStatus_.Line++;
                timerPreview.Start();
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            reset_ = true;
            doReset();
            saveScript();
            options_AfterSelect(sender, null);
        }

        private void doReset()
        {
            dlg_.HideOptions();

            // Asetetaan pohjakerros näkyviksi
            foreach (TreeNode node in options.Nodes[0].Nodes)
            {
                OptionData option = (OptionData)node.Tag;
                if (option.Show)
                {
                    option.Visible = OptionData.OptionState.On;
                }
            }
        }

        /// <summary>
        /// Laskee puherivin viiveen
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        private int speechDelay(string line)
        {
            // int loops = ((StrLen(text) / game.text_speed) + 1) * GetGameSpeed();
            return (line.Length / 15 + 1) * 40 * 25;
        }

        /// <summary>
        /// Suorittaa yhden skriptirivin
        /// </summary>
        /// <param name="line"></param>
        /// <returns>Viive seuraavaan riviin</returns>
        private int runScriptLine(string line)
        {
            const string PATTERN_COMMENT = "//.*$";
            const string PATTERN_OPTION = "[1-9][0-9]*";
            const string PATTERN_HEADER = "^@(S|" + PATTERN_OPTION + ")";
            // Skipattavat skriptikäskyt
            const string PATTERN_SKIP = "^("
                + "add-inv [0-9]+"
                + "|give-score [0-9]+"
                + "|goto-dialog [0-9]+"
                + "|goto-previous"
                + "|lose-inv [0-9]+"
                + "|new-room [0-9]+"
                + "|play-sound [0-9]+"
                + "|run-script [0-9]+"
                + "|set-globalint [0-9]+ [0-9]+"
                + "|set-speech-view \\s+ [0-9]+"
                + ")";
            const string PATTERN_INDENTED = "^\\s";

            Match re = null;

            // Karsitaan kommentti
            re = Regex.Match(line, PATTERN_COMMENT);
            if (re.Success)
            {                
                line = line.Substring(0, re.Index);
            }

            if (line.Length > 0)
            {
                // Sisältöä on

                re = Regex.Match(line, PATTERN_INDENTED);
                if (re.Success)
                {
                    // Sisennettyä koodia
                    //MessageBox.Show("Skipping code: " + line.Trim());
                    return SCRIPT_NEXTLINE;
                }                

                line = line.Trim();

                re = Regex.Match(line, PATTERN_HEADER);
                if (re.Success)
                {
                    // Otsikkorivi! VIRHE!
                    ErrorBox.Show("Don't add option starting points to the script!");
                    return SCRIPT_ERROR;
                }

                // Suoritettavat komennot skipataan, paitsi return ja stop

                try
                {
                    ScriptCommand.Effects? e = ScriptCommand.GetEffects(running_, line, true);
                    if (e != null)
                    {
                        if (e.Value.Return)
                            return SCRIPT_RETURN;
                        else if (e.Value.Stop)
                            return SCRIPT_STOP;
                        else
                            return SCRIPT_NEXTLINE;
                    }
                }
                catch (ScriptWarningException)
                {
                    return SCRIPT_NEXTLINE;
                }
                catch (Exception ex)
                {
                    ErrorBox.Show("Option " + running_.Index + ", line " + (scriptStatus_.Line + 1) + ":\n"
                        + ex.Message);
                    return SCRIPT_ERROR;
                }

                /*ScriptCommand.Name n = ScriptCommand.GetName(line);

                if (n == ScriptCommand.Name.Return || n == ScriptCommand.Name.GotoDialog)
                    return SCRIPT_RETURN;
                else if (n == ScriptCommand.Name.Stop || n == ScriptCommand.Name.GotoPrevious)
                    return SCRIPT_STOP;
                else if (n != ScriptCommand.Name.ERROR)
                    return SCRIPT_NEXTLINE;*/

                /*re = Regex.Match(line, ScriptCommand.GetHighlightRegex());
                if (re.Success)
                {
                    // Skriptikomento
                    return runCommand(re.Value);
                }*/

                re = Regex.Match(line, PATTERN_SKIP);
                if (re.Success)
                {
                    // Skriptikomento, jota tämä ohjelma ei toteuta
                    //MessageBox.Show("Skipping command: " + line);
                    return SCRIPT_NEXTLINE;
                }

                // Kyseessä dialogi, joten näytetään se ruudulla ja lasketaan viive
                int katkaisu = line.IndexOf(':');
                if (katkaisu < 0)  // Ei ole dialogia!?
                    lblPreview.Text = line;
                else
                {
                    lblSpeaker.Text = line.Substring(0, katkaisu + 1);
                    string text = line.Substring(katkaisu + 1).Replace("[[", "__(${HAKASULKU}$)__");
                    text = text.Replace("[", "\n");
                    text = text.Replace("__(${HAKASULKU}$)__", "[");
                    lblPreview.Text = text;
                }

                return speechDelay(line);
            }
            // Tyhjä rivi, suoraan seuraavaan
            return SCRIPT_NEXTLINE;
        }

        
        
        private void script_KeyUp(object sender, KeyEventArgs e)
        {
            /*if (e.Control && e.KeyCode == Keys.V)
            {
                int selStart = script.SelectionStart;
                int selLength = script.SelectionLength;
                // Liitetään tekstiä, päivitetään korostukset
                hilite_.HighlightAll();
                script.Select(selStart, selLength);
            }*/
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            options.Tag = null;
        }

        private void options_DrawNode(object sender, DrawTreeNodeEventArgs e)
        {
            if (e.Node.IsEditing)
            {
                // Muokattaessa solmua ei piirretä!
                return;
            }

            // Piirretään tausta        
            Brush bgBrush;
            if ( e.Node.BackColor == COLOR_OFFS )
                bgBrush = new HatchBrush(HatchStyle.DarkDownwardDiagonal,
                    e.Node.BackColor, options.BackColor);
            else
                bgBrush = new SolidBrush(e.Node.BackColor);
            
            e.Graphics.FillRectangle(bgBrush, e.Node.Bounds);

            // Piirretään teksti
            Font nodeFont = options.Font;
            Size size = TextRenderer.MeasureText(e.Node.Text, nodeFont);
            Rectangle rc = new Rectangle(e.Node.Bounds.Location, size);            
            TextRenderer.DrawText(e.Graphics, e.Node.Text, nodeFont, rc.Location, e.Node.ForeColor);
            
            // Jos valittu, piirretään valintarinkula
            if ((e.State & TreeNodeStates.Selected) != 0)
            {
                using (Pen focusPen = new Pen(Color.Black))
                {
                    focusPen.DashStyle = System.Drawing.Drawing2D.DashStyle.Dot;
                    Rectangle focusBounds = e.Node.Bounds;
                    focusBounds.Size = new Size(focusBounds.Width - 1,
                    focusBounds.Height - 1);
                    e.Graphics.DrawRectangle(focusPen, focusBounds);
                }
            }
        }

        private void checkCond_CheckedChanged(object sender, EventArgs e)
        {
            if (checkCond.Enabled && checkCond.Checked)
                txtCond.Enabled = true;
            else
                txtCond.Enabled = false;
            
            if (!lockModify_)
            {
                dlg_.Modified = true;
                if (!reset_)
                    modifiedAfterPreview_ = true;
            }
        }

        public void txtCond_TextChanged(object sender, EventArgs e)
        {
            if (!txtCond.Enabled)
                return;

            if (prevCondText_ == txtCond.Text &&
                sender.GetType() != Type.GetType("DialogDesigner.VarForm"))
                return;
            prevCondText_ = txtCond.Text;

            highlightCondition();
            evaluateCondition();

            if (!lockModify_ && sender.GetType() != Type.GetType("DialogDesigner.VarForm"))
            {
                if (!reset_)
                    modifiedAfterPreview_ = true;
                dlg_.Modified = true;
            }
        }

        private void script_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && (e.KeyCode == Keys.Y || (e.Shift && e.KeyCode == Keys.Z)))
            {
                // REDO
                undoBuffer_.Redo();
                e.SuppressKeyPress = true;
            }
            else if ((e.Control && e.KeyCode == Keys.Z) || (e.Alt && e.KeyCode == Keys.Back))
            {
                // UNDO
                undoBuffer_.Undo();
                e.SuppressKeyPress = true;
            }
            else if (e.Control)
            {
                if (e.KeyCode == Keys.I)
                {
                    // Ctrl+I tekee oletuksena tabin, disabloidaan
                    e.SuppressKeyPress = true;
                }
                else if (e.KeyCode == Keys.V)
                {
                    // PASTE
                    btnPaste_Click(sender, null);
                    e.SuppressKeyPress = true;
                    e.Handled = true;
                }                
                else if (e.KeyCode == Keys.E || e.KeyCode == Keys.R || e.KeyCode == Keys.L
                    || e.KeyCode == Keys.D1 || e.KeyCode == Keys.D2 || e.KeyCode == Keys.D5)
                {
                    // Tasaus keskelle, oikealle ja vasemmalle disabloitu
                    // Rivivälinmuutokset disabloitu
                    e.SuppressKeyPress = true;
                }
            }

            if (e.KeyCode == Keys.Tab)
            {
                try
                {
                    undoBuffer_.AddSnapshot();

                    IntPtr eventMask = RichTextBoxControl.LockRichTextBox(script);
                    if (script.SelectionLength > 0)
                    {
                        // Käsitellään useampia rivejä
                        int selStart = script.SelectionStart;
                        int selEnd = selStart + script.SelectionLength;
                        int line = script.GetLineFromCharIndex(selStart);
                        int startLine = line;
                        int end = script.GetLineFromCharIndex(selEnd);

                        // Ei oteta seuraavaa riviä mukaan vaikka se valinnassa vahingossa tulisi
                        if (script.Text[selEnd - 1] == '\n')
                            end--;

                        bool first = true;

                        while (line <= end)
                        {
                            script.SelectionStart = script.GetFirstCharIndexFromLine(line);
                            if (e.Shift)
                            {
                                // Sisennystä pois
                                script.SelectionLength = TAB_SIZE;
                                string text = script.SelectedText.TrimStart(' ');
                                script.SelectedText = text;
                                
                                selEnd -= TAB_SIZE - text.Length;
                                if (first)
                                    selStart -= TAB_SIZE - text.Length;
                            }
                            else
                            {
                                script.SelectionLength = 0;
                                script.SelectedText = new string(' ', TAB_SIZE);
                                selEnd += TAB_SIZE;
                                if (first)
                                    selStart += TAB_SIZE;
                            }
                            line++;
                            if (first)
                                first = false;
                        }
                        hilite_.HighlightAll();

                        if (startLine != script.GetLineFromCharIndex(selStart))
                            selStart = script.GetFirstCharIndexFromLine(startLine);

                        script.Select(selStart, selEnd - selStart);
                    }
                    else
                    {
                        if (e.Shift)
                        {
                            // Poistetaan max TAB_SIZE verran välilyöntejä kursorisijainnin edestä
                            int index = script.SelectionStart;

                            // Määritetään kuinka monta välilyöntiä voidaan poistaa edeltä
                            if (index < TAB_SIZE)
                            {
                                script.SelectionStart = 0;
                                script.SelectionLength = index;
                            }
                            else
                            {
                                script.SelectionStart -= TAB_SIZE;
                                script.SelectionLength = TAB_SIZE;
                            }

                            string text = script.SelectedText;
                            int i;
                            for (i = text.Length - 1; i >= 0; i--)
                            {
                                if (text[i] != ' ')
                                    break;
                            }
                            i++;
                            script.SelectedText = text.Substring(0, i);
                        }
                        else
                        {
                            // Lisätään tab välilyönteinä
                            script.SelectedText = new string(' ', TAB_SIZE);
                        }
                    }
                    RichTextBoxControl.UnlockRichTextBox(script, eventMask);
                    e.SuppressKeyPress = true;
                    e.Handled = true;
                }
                catch (Exception er)
                {
                    ErrorBox.Show("Script_KeyDown: " + er.Message);
                    Close();
                }
            }
        }

        private void highlightCondition()
        {
            int selStart = txtCond.SelectionStart;
            int selLength = txtCond.SelectionLength;

            // Luodaan regex muuttujista
            //(x|xerox|poop|pm|X|pm.X\[xerox\])($|[\(\)\s=\<\>&\|])
            string variables = "(^|[\\(\\)\\s=\\<\\>&\\|!])(";
            bool first = true;
            foreach (KeyValuePair<int, Variable> v in dlg_.Variables)
            {
                string regex = Regex.Escape(v.Value.Name);

                if (first)
                {
                    variables += regex;
                    first = false;
                }
                else
                {
                    variables += "|" + regex;
                }
            }
            variables += ")($|[\\(\\)\\s=\\<\\>&\\|])";

            SyntaxHighlighter condHilite = new SyntaxHighlighter(txtCond);
            condHilite.AddStyle(new SyntaxStyle("is(On|Off)\\([1-9][0-9]*\\)", 0, Color.DarkCyan));
            condHilite.AddStyle(new SyntaxStyle("(-[1-9][0-9]*|[0-9]+)", 1, Color.Brown));
            condHilite.AddStyle(new SyntaxStyle(variables, 2, Color.Purple));            
            condHilite.HighlightAll();

            txtCond.Select(selStart, selLength);
        }

        private void evaluateCondition()
        {
            if (txtCond.Text.Trim().Length > 0)
            {
                try
                {
                    lblCondValue.Text =
                        Conditional.Evaluate(txtCond.Text, dlg_).ToString();
                }
                catch (ScriptWarningException)
                {
                    lblCondValue.Text = "!";
                }
            }
            else
                lblCondValue.Text = "";
        }

        private void txtCond_EnabledChanged(object sender, EventArgs e)
        {
            if (txtCond.Enabled)
            {
                //highlightCondition();
                lblCondValue.Visible = true;
                //lblCondValue.Text = "";
            }
            else
            {
                // Poistetaan korostus
                //txtCond.Text = txtCond.Text;
                lblCondValue.Visible = false;
                //lblCondValue.Text = "";
            }
        }

        private void lblPreview_TextChanged(object sender, EventArgs e)
        {
            if (lblPreview.Text == "")
                lblSpeaker.Text = "";
        }

        private void panelPreview_MouseDown(object sender, MouseEventArgs e)
        {
            if (previewing_)
            {
                timerPreview_Tick(sender, null);
            }
        }

        private void options_MouseMove(object sender, MouseEventArgs e)
        {
            TreeNode node = options.GetNodeAt(e.X, e.Y);

            if (node != null && node.Bounds.Contains(e.X, e.Y))
            {
                // Muutetaan tooltippiä, jos teksti muuttuisi
                string index = ((OptionData)node.Tag).Index.ToString();
                if (((OptionData)node.Tag).Index == 0)
                    index = "S";

                string tip = string.Format("[{0}] {1}", index, node.Text);
                if (tip != toolTip.GetToolTip(options))
                {
                    toolTip.SetToolTip(options, tip);
                }
            }
            else
            {
                toolTip.SetToolTip(options, "");
            }

        }

        private void copyFirstLineToTitle_Click(object sender, EventArgs e)
        {
            int selstart = script.SelectionStart;
            int sellen = script.SelectionLength;

            saveScript();
            for (int i = 0; i < script.Lines.Length; i++)
            {
                // Poistetaan kommentti
                string line = Regex.Replace(script.Lines[i], "//.*$", "");

                Match m = Regex.Match(line, "^(?:[^:]*:(?<line>.+$)|[^\"]*\"(?<line>.+)\")");
                if (m.Success)
                {
                    // Ensimmäinen dialogirivi
                    ((OptionData)options.SelectedNode.Tag).Text = m.Groups["line"].ToString().Trim();
                    options.SelectedNode.Text = m.Groups[1].ToString().Trim();
                    dlg_.Modified = true;
                    break;
                }
            }
            loadScript();

            script.Select(selstart, sellen);
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            About about = new About();
            about.ShowDialog(this);
        }

        private void helpTopicsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start( Application.StartupPath + "\\help.chm");
        }

        private void Main_Load(object sender, EventArgs e)
        {
            // Asetetaan tallennetut ikkunan asetukset
            this.Size = Properties.Settings.Default.MainSize;
            this.Location = Properties.Settings.Default.MainLoc;
            this.WindowState = Properties.Settings.Default.MainState;            
            try
            {
                splitContainer1.SplitterDistance = Properties.Settings.Default.MainSplitter;
            }
            catch (Exception)
            {
                splitContainer1.SplitterDistance = 300;
            }

            if (Properties.Settings.Default.VarVisible)
                variablesToolStripMenuItem_Click(null, null);

            updateMRUMenu();
        }

        private void btnExportAGS_Click(object sender, EventArgs e)
        {
            if (dlgExport.ShowDialog() == DialogResult.OK)
            {
                StreamWriter w = new StreamWriter(dlgExport.FileName);

                saveScript();
                //try
                //{
                ScriptCommand.SetDialogConversion(
                    Properties.Settings.Default.UseCustomDialogExport,
                    Properties.Settings.Default.SearchPatterns, Properties.Settings.Default.UsedSearchPattern,
                    Properties.Settings.Default.ReplacePatterns, Properties.Settings.Default.UsedReplacePattern,
                    Properties.Settings.Default.DialogCapitalize, Properties.Settings.Default.AutomaticallyTurnOffSelectedOption);
                dlg_.ExportToAGS(w, true);
                w.Close();
                MessageBox.Show("Dialog exported successfully!", Application.ProductName);
                /*}
                catch (Exception err)
                {                    
                    w.Close();
                    File.Delete(dlgExport.FileName);
                    ErrorBox.Show("Export failed!\n\n" + err.Message);
                }   */             
                
            }
        }

        private void btnReset_EnabledChanged(object sender, EventArgs e)
        {
            btnReset.Visible = btnReset.Enabled;
        }

        private void dlgScript_Opening(object sender, CancelEventArgs e)
        {
            if (script.SelectionLength > 0)
                btnCut.Enabled = true;
            else
                btnCut.Enabled = false;
            btnCopy.Enabled = btnCut.Enabled;

            if (Clipboard.ContainsText())
                btnPaste.Enabled = true;
            else
                btnPaste.Enabled = false;

            btnUndo.Enabled = undoBuffer_.CanUndo;
            btnRedo.Enabled = undoBuffer_.CanRedo;
        }

        private void btnPaste_Click(object sender, EventArgs e)
        {
            undoBuffer_.AddSnapshot();

            int start = script.SelectionStart;
            string text = Clipboard.GetText().Replace("\t", new string(' ', TAB_SIZE));
            script.SelectedText = text;
            hilite_.HighlightAll();
            script.Select(start + text.Length, 0);
        }

        private void btnCut_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(script.SelectedText);
            script.SelectedText = "";
        }

        private void btnCopy_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(script.SelectedText);
        }

        private void btnExportProject_Click(object sender, EventArgs e)
        {
            ExportProject export = new ExportProject(dlg_);
            export.ShowDialog();
        }

        private void btnExportSettings_Click(object sender, EventArgs e)
        {
            ExportSettings form = new ExportSettings();
            form.ShowDialog();
        }

        private void checkSay_CheckedChanged(object sender, EventArgs e)
        {
            if (!lockModify_)
            {
                dlg_.Modified = true;
            }
        }

        private void script_SelectionChanged(object sender, EventArgs e)
        {
            if (script.SelectionLength == 0)
            {
                int index = script.SelectionStart;
                int line = script.GetLineFromCharIndex(index) + 1;
                Point pt = script.GetPositionFromCharIndex(index);
                pt.X = 0;
                int col = index - script.GetCharIndexFromPosition(pt) + 1;
                if (line < script.Lines.Length && script.Lines[line - 1].Length == 0 && index == script.Text.Length)
                    col--;
                lblScriptLine.Text = String.Format("Line {0}  Col {1}", line, col);
            }
            else
                lblScriptLine.Text = "";
        }

        private void script_KeyPress(object sender, KeyPressEventArgs e)
        {
            undoBuffer_.SetModified();
        }

        private void btnUndo_Click(object sender, EventArgs e)
        {
            undoBuffer_.Undo();
        }

        private void btnRedo_Click(object sender, EventArgs e)
        {
            undoBuffer_.Redo();
        }

        private void Main_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                Array files = (Array)e.Data.GetData(DataFormats.FileDrop);

                if (files != null && files.Length == 1)
                {
                    e.Effect = DragDropEffects.Copy;
                    return;
                }
            }

            e.Effect = DragDropEffects.None;
        }

        private void Main_DragDrop(object sender, DragEventArgs e)
        {
            try
            {
                Array a = (Array)e.Data.GetData(DataFormats.FileDrop);

                if (a != null && a.Length == 1)
                {
                    string path = a.GetValue(0).ToString();

                    BeginInvoke(dragDropOpenFileDelegate_, new Object[] { path });
                    Activate();
                }
            }
            catch (Exception ex)
            {
            }
        }

        private void dragDropOpenDialog(string path)
        {
            if (dlg_ != null && dlg_.Modified)
            {
                // Kysytään vahvistus, halutaanko tallentaa vanha
                DialogResult result = MessageBox.Show(
                    "The dialog has changed.\n\nDo you want to save the changes?",
                    Application.ProductName,
                    MessageBoxButtons.YesNoCancel,
                    MessageBoxIcon.Exclamation
                );

                if (result == DialogResult.Yes)
                {
                    btnSave_Click(null, null);
                }
                else if (result == DialogResult.Cancel)
                {
                    return;
                }
            }

            openDialog(path);
        }

    }
}
