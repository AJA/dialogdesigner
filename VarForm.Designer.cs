﻿namespace DialogDesigner
{
    partial class VarForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gridVars = new System.Windows.Forms.DataGridView();
            this.menuVars = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.btnNew = new System.Windows.Forms.ToolStripMenuItem();
            this.btnDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.btnSave = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.IndexColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VarColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TypeColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.ValueColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DescriptionColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gridVars)).BeginInit();
            this.menuVars.SuspendLayout();
            this.SuspendLayout();
            // 
            // gridVars
            // 
            this.gridVars.AllowUserToAddRows = false;
            this.gridVars.AllowUserToDeleteRows = false;
            this.gridVars.AllowUserToResizeRows = false;
            this.gridVars.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gridVars.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            this.gridVars.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridVars.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IndexColumn,
            this.VarColumn,
            this.TypeColumn,
            this.ValueColumn,
            this.DescriptionColumn});
            this.gridVars.ContextMenuStrip = this.menuVars;
            this.gridVars.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.gridVars.Location = new System.Drawing.Point(0, 0);
            this.gridVars.MultiSelect = false;
            this.gridVars.Name = "gridVars";
            this.gridVars.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.gridVars.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridVars.Size = new System.Drawing.Size(393, 178);
            this.gridVars.TabIndex = 25;
            this.gridVars.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.gridVars_CellMouseClick);
            this.gridVars.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.gridVars_CellBeginEdit);
            this.gridVars.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridVars_CellDoubleClick);
            this.gridVars.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridVars_CellEndEdit);
            this.gridVars.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gridVars_KeyDown);
            // 
            // menuVars
            // 
            this.menuVars.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnNew,
            this.btnDelete});
            this.menuVars.Name = "menuVars";
            this.menuVars.Size = new System.Drawing.Size(117, 48);
            this.menuVars.Opening += new System.ComponentModel.CancelEventHandler(this.menuVars_Opening);
            // 
            // btnNew
            // 
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(116, 22);
            this.btnNew.Text = "New";
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(116, 22);
            this.btnDelete.Text = "Delete";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSave.Location = new System.Drawing.Point(12, 183);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(130, 23);
            this.btnSave.TabIndex = 26;
            this.btnSave.Text = "Save as default values";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button1.Location = new System.Drawing.Point(148, 183);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(130, 23);
            this.button1.TabIndex = 27;
            this.button1.Text = "Restore default values";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // IndexColumn
            // 
            this.IndexColumn.HeaderText = "Index";
            this.IndexColumn.Name = "IndexColumn";
            this.IndexColumn.ReadOnly = true;
            this.IndexColumn.Visible = false;
            // 
            // VarColumn
            // 
            this.VarColumn.HeaderText = "Name";
            this.VarColumn.Name = "VarColumn";
            // 
            // TypeColumn
            // 
            this.TypeColumn.HeaderText = "Type";
            this.TypeColumn.Name = "TypeColumn";
            // 
            // ValueColumn
            // 
            this.ValueColumn.FillWeight = 50F;
            this.ValueColumn.HeaderText = "Value";
            this.ValueColumn.MaxInputLength = 10;
            this.ValueColumn.Name = "ValueColumn";
            this.ValueColumn.Width = 50;
            // 
            // DescriptionColumn
            // 
            this.DescriptionColumn.FillWeight = 200F;
            this.DescriptionColumn.HeaderText = "Description";
            this.DescriptionColumn.Name = "DescriptionColumn";
            this.DescriptionColumn.Width = 200;
            // 
            // VarForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(393, 212);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.gridVars);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(401, 238);
            this.Name = "VarForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Variables";
            this.Deactivate += new System.EventHandler(this.VarForm_Deactivate);
            this.Load += new System.EventHandler(this.VarForm_Load);
            this.Activated += new System.EventHandler(this.VarForm_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.VarForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.gridVars)).EndInit();
            this.menuVars.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView gridVars;
        private System.Windows.Forms.ContextMenuStrip menuVars;
        private System.Windows.Forms.ToolStripMenuItem btnNew;
        private System.Windows.Forms.ToolStripMenuItem btnDelete;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridViewTextBoxColumn IndexColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn VarColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn TypeColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ValueColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn DescriptionColumn;


    }
}