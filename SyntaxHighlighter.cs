﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Runtime.InteropServices;
using Utils;

namespace DialogDesigner
{
    public class SyntaxHighlighter
    {
        // TOTEUTUS ALKAA -----------------------------------------
        private List<SyntaxStyle> styles_;
        private RichTextBox textBox_;
        private bool locked_;
        private bool longLock_;
        private IntPtr eventMask_;

        /// <summary>
        /// Rakentaja linkittää käytettävään tekstilaatikkoon
        /// </summary>
        /// <param name="textBox"></param>
        public SyntaxHighlighter(RichTextBox textBox)
        {
            textBox_ = textBox;
            styles_ = new List<SyntaxStyle>();
            locked_ = false;
            longLock_ = false;
            eventMask_ = IntPtr.Zero;
        }

        /// <summary>
        /// Ottaa käyttöön uuden tyylin
        /// </summary>
        /// <param name="style"></param>
        public void AddStyle(SyntaxStyle style)
        {
            styles_.Add(style);
        }

        /// <summary>
        /// Estää tekstilaatikon päivityksen
        /// </summary>
        private void lockUpdate()
        {
            if (!locked_)
            {
                try
                {
                    eventMask_ = RichTextBoxControl.LockRichTextBox(textBox_);
                }
                catch { }
                locked_ = true;
            }
        }

        /// <summary>
        /// Sallii tekstilaatikon päivityksen
        /// </summary>
        private void unlockUpdate()
        {
            if (locked_ && !longLock_)
            {
                try
                {
                    RichTextBoxControl.UnlockRichTextBox(textBox_, eventMask_);
                }
                catch { }
                locked_ = false;
            }
        }

        /// <summary>
        /// Korostaa koko tekstilaatikon sisällön
        /// </summary>
        public void HighlightAll()
        {
            if (textBox_.Text.Length == 0)
                return;
            try
            {
                int pos = 0;
                longLock_ = true;
                lockUpdate();

                // Poistetaan korostukset koko tekstistä
                textBox_.SelectAll();
                textBox_.SelectionBackColor = textBox_.BackColor;
                textBox_.SelectionColor = textBox_.ForeColor;
                textBox_.SelectionFont = textBox_.Font;

                foreach (string line in textBox_.Lines)
                {
                    HighlightSection(pos, line.Length);
                    pos += line.Length + 1;
                }
            }
            finally
            {
                longLock_ = false;
                unlockUpdate();
            }
        }

        /// <summary>
        /// Korostaa tekstilaatikon valinnan sisällön
        /// </summary>
        public void HighlightSection(int index, int length)
        {
            try
            {
                lockUpdate();
                textBox_.Tag = true;    // Estetään TextChanged-event

                int selstart = textBox_.SelectionStart;
                int sellen = textBox_.SelectionLength;
                int scrollpos = RichTextBoxControl.GetScroll(textBox_);

                textBox_.Select(index, length);
                string line = textBox_.SelectedText;

                // Oletukset
                textBox_.SelectionFont = textBox_.Font;
                textBox_.SelectionColor = textBox_.ForeColor;
                textBox_.SelectionBackColor = textBox_.BackColor;

                foreach (SyntaxStyle s in styles_)
                {
                    MatchCollection matches = Regex.Matches(line, s.Regex);

                    foreach (Match m in matches)
                    {
                        // Korostettavaa tekstiä löytyi
                        textBox_.Select(index + m.Groups[s.Group].Index,
                            m.Groups[s.Group].Length);

                        if (s.BackColor != null)
                            textBox_.SelectionBackColor = (Color)s.BackColor;
                        if (s.TextColor != null)
                            textBox_.SelectionColor = (Color)s.TextColor;
                        if (s.FontStyle != null)
                            textBox_.SelectionFont =
                                new Font(textBox_.Font, (FontStyle)s.FontStyle);
                        //Console.WriteLine("Matched: " + textBox_.SelectedText + "\nRegex: " + s.Regex);
                    }
                }
                //textBox_.Select(0, 0);
                textBox_.Tag = false;

                //Console.WriteLine("Scrollpos: " + scrollpos);

                textBox_.SelectionStart = selstart;
                textBox_.SelectionLength = sellen;
                RichTextBoxControl.SetScroll(textBox_, scrollpos);
                //textBox_.Refresh();
                //textBox_.ScrollToCaret();
            }
            finally
            {
                unlockUpdate();
            }
        }
    }

    public class SyntaxStyle
    {
        public String Regex { get; set; }
        public Color? TextColor { get; set; }
        public Color? BackColor { get; set; }
        public FontStyle? FontStyle { get; set; }
        public int Group { get; set; }

        /// <summary>
        /// Tyyli ilman fontin tyyliä ja taustan väriä
        /// </summary>
        /// <param name="regex"></param>
        /// <param name="textColor"></param>
        public SyntaxStyle(string regex, int group, Color textColor)
        {
            Regex = regex;
            FontStyle = null;
            TextColor = textColor;
            BackColor = null;
            Group = group;
        }

        /// <summary>
        /// Tyyli ilman tekstin ja taustan väriä
        /// </summary>
        /// <param name="regex"></param>
        /// <param name="font"></param>
        public SyntaxStyle(string regex, int group, FontStyle style)
        {
            Regex = regex;
            FontStyle = style;
            TextColor = null;
            BackColor = null;
            Group = group;
        }

        /// <summary>
        /// Tyyli ilman taustaväriä
        /// </summary>
        /// <param name="regex"></param>
        /// <param name="font"></param>
        /// <param name="textColor"></param>
        public SyntaxStyle(string regex, int group, FontStyle style, Color textColor)
        {
            Regex = regex;
            FontStyle = style;
            TextColor = textColor;
            BackColor = null;
            Group = group;
        }

        /// <summary>
        /// Täysin mukautettu tyyli
        /// </summary>
        /// <param name="regex"></param>
        /// <param name="font"></param>
        /// <param name="textColor"></param>
        /// <param name="backColor"></param>
        public SyntaxStyle(string regex, int group, FontStyle style,
            Color textColor, Color backColor)
        {
            Regex = regex;
            FontStyle = style;
            TextColor = textColor;
            BackColor = backColor;
            Group = group;
        }
    }
}
