﻿namespace DialogDesigner
{
    partial class Choise
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listChoises = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // listChoises
            // 
            this.listChoises.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.listChoises.BackColor = System.Drawing.Color.Black;
            this.listChoises.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listChoises.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.listChoises.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listChoises.ForeColor = System.Drawing.Color.White;
            this.listChoises.FormattingEnabled = true;
            this.listChoises.HorizontalExtent = 1000;
            this.listChoises.ItemHeight = 20;
            this.listChoises.Location = new System.Drawing.Point(12, 12);
            this.listChoises.Margin = new System.Windows.Forms.Padding(10);
            this.listChoises.Name = "listChoises";
            this.listChoises.Size = new System.Drawing.Size(560, 180);
            this.listChoises.TabIndex = 0;
            this.listChoises.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.listChoises_DrawItem);
            this.listChoises.MouseClick += new System.Windows.Forms.MouseEventHandler(this.listChoises_MouseClick);
            this.listChoises.MeasureItem += new System.Windows.Forms.MeasureItemEventHandler(this.listChoises_MeasureItem);
            this.listChoises.MouseMove += new System.Windows.Forms.MouseEventHandler(this.listBox1_MouseMove);
            this.listChoises.MouseLeave += new System.EventHandler(this.listChoises_MouseLeave);
            // 
            // Choise
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(584, 207);
            this.Controls.Add(this.listChoises);
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Choise";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Select option";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Choise_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Choise_KeyDown);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox listChoises;
    }
}