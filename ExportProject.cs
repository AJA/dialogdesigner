﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;
using System.IO;

namespace DialogDesigner
{
    public partial class ExportProject : Form
    {
        enum State
        {
            SelectProject,
            SetName,
            SelectDialog
        }

        private State state_;
        private XmlDocument projectXml_;
        private Dialog dialog_;

        private const string PATH_DIALOGS = "AGSEditorDocument/Game/Dialogs";

        private State FormState
        {
            get { return state_; }
            set
            {
                state_ = value;

                if (value == State.SelectProject)
                {
                    radioNew.Enabled = false;
                    radioReplace.Enabled = false;
                    textDialogName.Enabled = false;
                    listDialogs.Enabled = false;
                    btnBrowse.Select();
                    btnExport.Enabled = false;
                }
                else
                {
                    radioNew.Enabled = true;
                    radioReplace.Enabled = listDialogs.Items.Count > 0;
                    btnExport.Enabled = true;

                    if (value == State.SetName)
                    {
                        textDialogName.Enabled = true;
                        listDialogs.Enabled = false;
                        textDialogName.Select();
                    }
                    else
                    {
                        textDialogName.Enabled = false;
                        listDialogs.Enabled = true;
                        listDialogs.Select();
                    }
                }
            }
        }


        public ExportProject(Dialog dialog)
        {
            InitializeComponent();

            dialog_ = dialog;
            FormState = State.SelectProject;
        }


        private void btnBrowse_Click(object sender, EventArgs e)
        {
            if (dlgOpenProject.ShowDialog() == DialogResult.OK)
            {
                XmlDocument xml = new XmlDocument();

                try
                {
                    xml.Load(dlgOpenProject.FileName);
                }
                catch (Exception)
                {
                    ErrorBox.Show("Project file could not be opened!");
                    return;
                }

                try
                {
                    string version = xml.LastChild.Attributes.GetNamedItem("VersionIndex").Value;                

                    if (version == null || Convert.ToInt32(version) != 4)
                    {
                        DialogResult result = MessageBox.Show(
                            "The project file is newer or older than those tested with Dialog Designer.\n\n"
                            + "Are you sure you want to continue?",
                            "Confirmation",
                            MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);

                        if (result == DialogResult.No)
                            return;
                    }

                    projectXml_ = xml;
                    textProject.Text = dlgOpenProject.FileName;
                    radioNew.Checked = true;                

                    listDialogs.Items.Clear();

                    XmlNode dialogs = xml.SelectSingleNode(PATH_DIALOGS);

                    foreach (XmlNode dlg in dialogs.ChildNodes)
                    {
                        // Haetaan kunkiin dialogin nimi ja indeksi
                        string id = null;
                        string name = null;

                        foreach (XmlNode node in dlg.ChildNodes)
                        {
                            if (node.Name == "Name")
                                name = node.InnerText;
                            else if (node.Name == "ID")
                                id = node.InnerText;
                        }

                        // Lisätään luetteloon
                        if (id != null && name != null)
                            listDialogs.Items.Insert(Convert.ToInt32(id),
                                string.Format("[{0}] {1}", id, name));
                    }

                    if (listDialogs.Items.Count > 0)
                        listDialogs.SelectedIndex = 0;

                    FormState = State.SetName;
                }
                catch (Exception)
                {
                    ErrorBox.Show("Invalid project file!");
                    return;
                }
            }
        }

        private void radioNew_CheckedChanged(object sender, EventArgs e)
        {
            if (radioNew.Checked)
                FormState = State.SetName;
            else
                FormState = State.SelectDialog;
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            if (FormState == State.SetName && textDialogName.Text.Length == 0)
            {
                ErrorBox.Show("Dialog name can't be empty.");
                return;
            }
            else if (FormState == State.SelectDialog && listDialogs.SelectedIndex < 0)
            {
                ErrorBox.Show("Select dialog to be replaced.");
                return;
            }

            // Varmuuskopio
            File.Copy(textProject.Text, textProject.Text + ".bup", true);
            
            ScriptCommand.SetDialogConversion(
                    Properties.Settings.Default.UseCustomDialogExport,
                    Properties.Settings.Default.SearchPatterns, Properties.Settings.Default.UsedSearchPattern,
                    Properties.Settings.Default.ReplacePatterns, Properties.Settings.Default.UsedReplacePattern,
                    Properties.Settings.Default.DialogCapitalize, Properties.Settings.Default.AutomaticallyTurnOffSelectedOption);

            try
            {
                projectXml_.Load(dlgOpenProject.FileName);
            }
            catch (Exception)
            {
                ErrorBox.Show("Project file could not be opened!");
                return;
            }
            
            if (FormState == State.SetName)
            {
                Match m = Regex.Match(textDialogName.Text,
                                      "^[^a-zA-Z_]|[^a-zA-Z0-9_]");
                if (m.Success)
                {
                    ErrorBox.Show("Invalid dialog name!\n\n"
                                  + "Dialog names can only contain letters, numbers and underscores.\n"
                                  + "They also can't begin with a number." );
                    return;
                }

                XmlNode dialogs = projectXml_.SelectSingleNode(PATH_DIALOGS);
                XmlElement elDialog = projectXml_.CreateElement("Dialog");

                XmlElement elID = projectXml_.CreateElement("ID");
                elID.InnerText = Convert.ToString(listDialogs.Items.Count);
                elDialog.AppendChild(elID);

                XmlElement elName = projectXml_.CreateElement("Name");
                elName.InnerText = textDialogName.Text;
                elDialog.AppendChild(elName);

                XmlElement elParser = projectXml_.CreateElement("ShowTextParser");
                elParser.InnerText = "False";
                elDialog.AppendChild(elParser);

                XmlElement elScript = projectXml_.CreateElement("Script");
                ExportScript(elScript);
                elDialog.AppendChild(elScript);

                XmlElement elOptions = projectXml_.CreateElement("DialogOptions");
                ExportOptions(elOptions);
                elDialog.AppendChild(elOptions);

                dialogs.AppendChild(elDialog);
            }
            else if (FormState == State.SelectDialog)
            {
                XmlNodeList dialogs = projectXml_.SelectNodes( PATH_DIALOGS + "/Dialog/ID" );
                XmlNode dialog = null;

                // Etsitään korvattava dialogi
                foreach (XmlNode dlg in dialogs)
                {
                    if (dlg.InnerText == Convert.ToString(listDialogs.SelectedIndex))
                        dialog = dlg.ParentNode;
                }

                if (dialog == null)
                {
                    throw new Exception("Export failed!");
                }

                XmlNode script = dialog.SelectSingleNode("Script");
                XmlNode options = dialog.SelectSingleNode("DialogOptions");
                options.RemoveAll();

                ExportScript(script);
                ExportOptions(options);
            }

            // Tallennetaan lopputulos
            try
            {
                projectXml_.Save(textProject.Text);
            }
            catch (Exception ex)
            {
                ErrorBox.Show("Could not save project!\n\n" + ex.Message);
                return;
            }

            MessageBox.Show("Dialog exported successfully!", Application.ProductName);
            Close();
        }


        public void ExportScript(XmlNode scriptNode)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter ags = new StreamWriter(stream);
            dialog_.ExportToAGS(ags, false);
            ags.Flush();
            stream.Seek(0, SeekOrigin.Begin);

            StreamReader reader = new StreamReader(stream);
            string script = reader.ReadToEnd();
            ags.Close();

            scriptNode.RemoveAll();
            XmlCDataSection data = projectXml_.CreateCDataSection(script);
            scriptNode.AppendChild(data);
        }


        public void ExportOptions(XmlNode optionsNode)
        {
            foreach (OptionData op in dialog_.Options)
            {
                // Lisätään kaikki paitsi startti
                if (op.Index > 0)
                {                    
                    XmlElement elOption = projectXml_.CreateElement("DialogOption");

                    XmlElement elID = projectXml_.CreateElement("ID");
                    elID.InnerText = Convert.ToString(op.Index);
                    elOption.AppendChild(elID);

                    XmlElement elText = projectXml_.CreateElement("Text");
                    elText.InnerText = op.Text;
                    XmlAttribute atSpace = projectXml_.CreateAttribute("xml:space");
                    atSpace.Value = "preserve";
                    elText.SetAttributeNode(atSpace);
                    elOption.AppendChild(elText);

                    XmlElement elShow = projectXml_.CreateElement("Show");
                    elShow.InnerText = Convert.ToString(op.Parent.Index == 0 && op.Show);
                    elOption.AppendChild(elShow);

                    XmlElement elSay = projectXml_.CreateElement("Say");
                    elSay.InnerText = Convert.ToString(op.Say);
                    elOption.AppendChild(elSay);

                    optionsNode.AppendChild(elOption);
                }
            }
        }
    }
}
