﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace DialogDesigner
{
    public static class ErrorBox
    {
        public static void Show(string text)
        {
            MessageBox.Show(text, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }

    public static class WarningBox
    {
        public static void Show(string text)
        {
            MessageBox.Show(text, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }
    }
}
