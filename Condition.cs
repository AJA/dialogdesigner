﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace DialogDesigner
{
    public static class Conditional
    {

        /// <summary>
        /// Määrittää ehdon totuusarvon, heittää poikkeuksen jos muuttuja puuttui
        /// </summary>
        /// <param name="condition"></param>
        /// <param name="variables"></param>
        public static bool Evaluate(string condition, Dialog dlg)
        {
            // Määritetään isOn/Off-arvot
            condition = ScriptCommand.ReplaceIsOnOff(dlg, condition, false);

            // Korvataan muuttujat arvoillaan
            foreach (KeyValuePair<int, Variable> v in dlg.Variables)
            {
                MatchCollection matches = Regex.Matches( condition,
                    "(^|[\\(\\)\\s=\\<\\>&\\|!])(" + Regex.Escape(v.Value.Name)
                    + ")($|[\\(\\)\\s=\\<\\>&\\|])");

                // Kuinka paljon on poistettu alusta/edestä merkkejä
                int startShift = 0;

                foreach (Match m in matches)
                {
                    //Console.WriteLine("MATCH: " + m.ToString());
                    //Console.WriteLine("EVALUATING: " + condition);
                    //Console.WriteLine("LENGTH: {0}", condition.Length);

                    string start = condition.Substring(0, m.Groups[2].Index - startShift);
                    string end = condition.Substring(m.Groups[2].Index + m.Groups[2].Length - startShift);

                    string value = v.Value.Value.ToString();
                    if (v.Value.Type == Variable.VarType.Boolean)
                        value = "(" + value + "!=0)";

                    startShift += m.Groups[2].Length - value.Length;

                    condition = start + value + end;
                }

                /*condition = Regex.Replace(condition,
                    "(?<=^|[\\(\\)\\s=\\<\\>&\\|])(" + v.Value.Name
                    + ")(?=$|[\\(\\)\\s=\\<\\>&\\|])", v.Value.Value.ToString());*/
            }

            Match match = Regex.Match(condition, "[A-Za-z_\\.\\[\\]]");
            if (match.Success)
            {
                // Kaikille muuttujille ei ollut arvoa
                throw new ScriptWarningException("Undefined variables found!");
            }

            //Console.WriteLine(condition);

            // Parsitaan ehto
            Scanner scanner = new Scanner(); 
            Parser parser = new Parser(scanner); 
            ParseTree tree = parser.Parse( condition );

            if (tree.Errors.Count > 0)
            {
                throw new ScriptWarningException("Condition is illegal!");
            }

            return Convert.ToBoolean(tree.Eval(null));

        }
    }
}
