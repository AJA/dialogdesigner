﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DialogDesigner
{
    public partial class Choise : Form
    {
        private int prevSel_;
        private int select_;
        private bool altF4_;

        public Choise()
        {
            InitializeComponent();
            prevSel_ = -1;
            select_ = -1;
            altF4_ = false;
            DialogResult = 0;           
        }

        public int ShowDialog( List<OptionData> options )
        {
            listChoises.Items.AddRange(options.ToArray());
            listChoises.SelectedIndex = -1;

            ShowDialog();
            return select_;
        }

        private void listBox1_MouseMove(object sender, MouseEventArgs e)
        {
            int index = listChoises.IndexFromPoint(e.X, e.Y);
            if (prevSel_ != index)
            {
                // Valinta muuttunut!
                prevSel_ = index;
                listChoises.SelectedIndex = index;
                listChoises.Invalidate(true);
            }
        }

        private void Choise_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Alt && e.KeyCode == Keys.F4)
            {
                altF4_ = true;
            }
            else if (e.KeyCode == Keys.Enter && listChoises.SelectedIndex >= 0)
            {
                select_ = listChoises.SelectedIndex;
                Close();
            }
        }

        private void Choise_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (altF4_)
            {
                // Ei sallita sulkemista
                altF4_ = false;
                e.Cancel = true;
            }
        }

        private void listChoises_MouseClick(object sender, MouseEventArgs e)
        {
            if (listChoises.SelectedIndex >= 0)
            {
                select_ = listChoises.SelectedIndex;
                Close();
            }
        }
        /*
        public static string[] Wrap(string text, int maxLength, Font font, int subLineIndent)
        {
            string[] Words = text.Split(' ');
            int currentLineLength = 0;
            List<string> Lines = new List<string>();
            string currentLine = "";
            int spaceWidth = TextRenderer.MeasureText(" ", font).Width;
            int indent = subLineIndent * spaceWidth;

            foreach (string currentWord in Words)
            {
                if (currentWord.Length > 0)
                {
                    // Mitataan teksti
                    int length = TextRenderer.MeasureText( currentWord, font ).Width;

                    if (currentLineLength + spaceWidth + length > maxLength)
                    {
                        // Ei mahdu enää sana riville
                        Lines.Add(currentLine);
                        currentLine = currentWord;
                        currentLineLength = length + indent;
                    }
                    else
                    {
                        // Mahtuu vielä riville
                        currentLine += currentWord + " ";
                        currentLineLength += spaceWidth + length;
                    }
                }

            }
            if (currentLine != "")
                Lines.Add(currentLine);

            return Lines.ToArray();
        }
        */

        private void listChoises_DrawItem(object sender, DrawItemEventArgs e)
        {
            Brush color = Brushes.White;
            if ((e.State & DrawItemState.Selected) != 0)
            {
                color = Brushes.Orange;
            }

            string text = listChoises.Items[e.Index].ToString();

            // Muunnetaan valinta tarvittaessa useariviseksi
            //string[] lines = Wrap(text, 2*listChoises.Width - 100, e.Font, 0); //listChoises.Width, e.Font, 4);

            //int itemHeight = lines.Length * listChoises.ItemHeight;
            e.Graphics.FillRectangle(Brushes.Black, e.Bounds);            

            e.Graphics.DrawString("> " + text, e.Font, color, e.Bounds, StringFormat.GenericDefault);
            
        }

        private void listChoises_MouseLeave(object sender, EventArgs e)
        {
            listChoises.SelectedIndex = -1;
            prevSel_ = -1;
        }

        private void listChoises_MeasureItem(object sender, MeasureItemEventArgs e)
        {
            string text = listChoises.Items[e.Index].ToString();
            e.ItemWidth = listChoises.Width;

            // Muunnetaan valinta tarvittaessa useariviseksi
            SizeF size = e.Graphics.MeasureString("> " + text, listChoises.Font,
                listChoises.Width, StringFormat.GenericDefault);
            int h = Convert.ToInt32(size.Height);
            e.ItemHeight = h;

            /*string[] lines = Wrap(text, e.ItemWidth, listChoises.Font, 4);
            e.ItemHeight = lines.Length * listChoises.ItemHeight;*/
        }
    }
}
