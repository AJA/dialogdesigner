﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using Utils;

namespace DialogDesigner
{
    public class UndoBuffer
    {
        private RichTextBox box_;
        private int depth_;
        private List<Snapshot> buffer_;
        private int undoPos_;        
        private bool textModified_;
        private bool postUndo_;

        public bool Processing { get; set; }
        public bool CanUndo
        {
            get 
            {
                return (undoPos_ > 1 || textModified_);
            }
        }
        public bool CanRedo
        {
            get
            {
                return (undoPos_ < buffer_.Count);
            }
        }

        public UndoBuffer(RichTextBox textBox, int bufferDepth)
        {
            box_ = textBox;
            depth_ = bufferDepth;
            undoPos_ = 0;
            buffer_ = new List<Snapshot>();
            Processing = false;
            textModified_ = false;
            postUndo_ = false;
        }

        public void Clear()
        {
            //Console.WriteLine("CLEAR");
            buffer_.Clear();
            undoPos_ = 0;
        }

        public void SetModified()
        {
            if (!Processing)
            {
                // Tuhotaan nykytasoa myöhemmät
                if (undoPos_ < buffer_.Count)
                    buffer_.RemoveRange(undoPos_, buffer_.Count - undoPos_);

                if (!textModified_ && !postUndo_)
                    AddSnapshot();
                postUndo_ = false;

                textModified_ = true;
            }
        }

        public void AddSnapshot()
        {
            if (Processing)
                return;

            // Tuhotaan nykytasoa myöhemmät
            if (undoPos_ < buffer_.Count)
                buffer_.RemoveRange(undoPos_, buffer_.Count - undoPos_);

            // Unohdetaan vanhin jos täyttä
            if (buffer_.Count == depth_)
                buffer_.RemoveAt(0);
            else
                undoPos_++;

            //Console.WriteLine("Added snapshot #" + undoPos_ + ":\n" + box_.Text);

            buffer_.Add(new Snapshot(box_.Rtf, box_.Text, box_.SelectionStart, box_.SelectionLength));
        }

        public void Undo()
        {
            if (textModified_)
            {
                AddSnapshot();
                textModified_ = false;
            }

            if (undoPos_ > 1)
            {
                Processing = true;
                undoPos_--;
                //Console.WriteLine("UNDO " + undoPos_ + ": " + buffer_[undoPos_ - 1].Text);
                IntPtr mask = RichTextBoxControl.LockRichTextBox(box_);
                box_.Rtf = buffer_[undoPos_ - 1].Text;
                box_.Select(buffer_[undoPos_ - 1].SelStart, buffer_[undoPos_ - 1].SelLength);

                ScrollToSelection();

                RichTextBoxControl.UnlockRichTextBox(box_, mask);
                Processing = false;
                postUndo_ = true;
            }
        }

        public void Redo()
        {
            if (!textModified_ && undoPos_ < buffer_.Count)
            {
                Processing = true;
                undoPos_++;
                IntPtr mask = RichTextBoxControl.LockRichTextBox(box_);
                box_.Rtf = buffer_[undoPos_ - 1].Text;
                box_.Select(buffer_[undoPos_ - 1].SelStart, buffer_[undoPos_ - 1].SelLength);

                ScrollToSelection();

                RichTextBoxControl.UnlockRichTextBox(box_, mask);
                //Console.WriteLine("REDO: " + undoPos_ + ": " + buffer_[undoPos_ - 1].Text);
                Processing = false;
            }
        }

        private void ScrollToSelection()
        {
            box_.ScrollToCaret();

            /*int firstChar = box_.GetCharFromPosition(new Point(1, 1));
            int lastChar = box_.GetCharFromPosition(new Point(1, box_.Height - 1));

            if (box_.SelectionStart < firstChar || box_.SelectionStart > lastChar)
                box_.ScrollToCaret();*/
        }

        /// <summary>
        /// Undo-tasoa kuvaava rakenne
        /// </summary>
        private struct Snapshot
        {
            public Snapshot(string text, string db, int selStart, int selLength)
            {
                Text = text;
                Debug = db;
                SelStart = selStart;
                SelLength = selLength;
            }

            public string Text;
            public string Debug;
            public int SelStart;
            public int SelLength;
        }
    }
}
