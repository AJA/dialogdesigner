﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DialogDesigner
{
    public class Variable
    {
        public enum VarType { Integer, Boolean }

        public string Name { get; set; }
        public int DefaultValue { get; set; }
        public int Value { get; set; }        
        public string Description { get; set; }
        public VarType Type { get; set; }
        

        public Variable(string name, VarType type, int value, string description)
        {
            Name = name;
            Type = type;
            DefaultValue = 0;
            Value = value;
            Description = description;
        }

        public Variable(string name, VarType type, int defaultValue, int value, string description)
        {
            Name = name;
            Type = type;
            DefaultValue = defaultValue;
            Value = value;
            Description = description;
        }
    }
}
